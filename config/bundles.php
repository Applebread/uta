<?php

$bundles = [
    Symfony\Bundle\FrameworkBundle\FrameworkBundle::class => ['all' => true],
    Doctrine\Bundle\DoctrineBundle\DoctrineBundle::class => ['all' => true],
    Doctrine\Bundle\MigrationsBundle\DoctrineMigrationsBundle::class => ['all' => true],
    Symfony\Bundle\DebugBundle\DebugBundle::class => ['dev' => true],
    Symfony\Bundle\TwigBundle\TwigBundle::class => ['all' => true],
    Symfony\Bundle\WebProfilerBundle\WebProfilerBundle::class => ['dev' => true, 'test' => true],
    Twig\Extra\TwigExtraBundle\TwigExtraBundle::class => ['all' => true],
    Symfony\Bundle\SecurityBundle\SecurityBundle::class => ['all' => true],
    Symfony\Bundle\MonologBundle\MonologBundle::class => ['all' => true],
    Symfony\Bundle\MakerBundle\MakerBundle::class => ['dev' => true],
    Sensio\Bundle\FrameworkExtraBundle\SensioFrameworkExtraBundle::class => ['all' => true],
    App\Modules\UserBundle\UserBundle::class => ['all' => true],
    App\Modules\PilotBundle\PilotBundle::class => ['all' => true],
    App\Modules\ConfigurationBundle\ConfigurationBundle::class => ['all' => true],
    App\Modules\TimetableBundle\TimetableBundle::class => ['all' => true],
    App\Modules\AirportBundle\AirportBundle::class => ['all' => true],
    App\Modules\InfogateParserBundle\InfogateParserBundle::class => ['all' => true],
    App\Modules\FlightawareParserBundle\FlightawareParserBundle::class => ['all' => true],
    App\Modules\AdminPanelBundle\AdminPanelBundle::class => ['all' => true],
    App\Modules\TripBundle\TripBundle::class => ['all' => true],
    App\Modules\AiracUploaderBundle\AiracUploaderBundle::class => ['all' => true],
    App\Modules\AircraftBundle\AircraftBundle::class => ['all' => true],
    App\Modules\AviationEdgeParserBundle\AviationEdgeParserBundle::class => ['all' => true],
    App\Modules\UtairParserBundle\UtairParserBundle::class => ['all' => true],
    App\Modules\ExamBundle\ExamBundle::class => ['all' => true],
    App\Modules\CheckWxApiBundle\CheckWxApiBundle::class => ['all' => true],
    Nelmio\CorsBundle\NelmioCorsBundle::class => ['all' => true],
    Doctrine\Bundle\FixturesBundle\DoctrineFixturesBundle::class => ['dev' => true, 'test' => true],
    Lexik\Bundle\JWTAuthenticationBundle\LexikJWTAuthenticationBundle::class => ['all' => true],
    ApiPlatform\Core\Bridge\Symfony\Bundle\ApiPlatformBundle::class => ['all' => true],
];

if (in_array($this->getEnvironment(), ['dev', 'test'])) {
    $bundles[Nelmio\ApiDocBundle\NelmioApiDocBundle::class] = ['all' => true];
}

return $bundles;
