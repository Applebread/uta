<?php

namespace App\Dto;

use Iterator;
use JsonSerializable;

abstract class AbstractPaginatedCollection implements Iterator, JsonSerializable
{
    protected array $items;

    protected int $totalResults;

    public function next(): void
    {
        next($this->items);
    }

    public function key(): ?int
    {
        return key($this->items);
    }

    public function valid(): bool
    {
        return $this->key() !== null;
    }

    public function rewind(): void
    {
        reset($this->items);
    }

    public function jsonSerialize(): array
    {
        return get_object_vars($this);
    }
}
