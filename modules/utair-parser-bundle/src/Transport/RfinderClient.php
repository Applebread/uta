<?php

namespace App\Modules\UtairParserBundle\Transport;

use GuzzleHttp\Client;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\MessageFormatter;
use GuzzleHttp\Middleware;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;


class RfinderClient extends Client
{
    public function __construct(
        string $apiUrl,
        callable $defaultHandler = null,
        LoggerInterface $logger = null
    )
    {
        $stack = HandlerStack::create($defaultHandler ?: null);
        $stack->push(Middleware::log($logger ?? new NullLogger(), new MessageFormatter(MessageFormatter::DEBUG)));

        parent::__construct([
            'base_uri' => $apiUrl,
            'handler' => $stack
        ]);
    }
}
