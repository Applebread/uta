<?php

namespace App\Modules\UtairParserBundle\Transport;

use GuzzleHttp\Client;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\MessageFormatter;
use GuzzleHttp\Middleware;
use GuzzleHttp\RequestOptions;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;


class UtairClient extends Client
{
    public function __construct(
        string $apiUrl,
        string $apikey,
        callable $defaultHandler = null,
        LoggerInterface $logger = null
    )
    {
        $stack = HandlerStack::create($defaultHandler ?: null);
        $stack->push(Middleware::log($logger ?? new NullLogger(), new MessageFormatter(MessageFormatter::DEBUG)));

        parent::__construct([
            'base_uri' => $apiUrl,
            'handler' => $stack,
            RequestOptions::HEADERS => [
                'Authorization' => "Bearer $apikey"
            ]
        ]);
    }
}
