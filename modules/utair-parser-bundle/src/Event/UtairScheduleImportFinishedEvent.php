<?php

namespace App\Modules\UtairParserBundle\Event;

use App\Modules\UtairParserBundle\Value\UtairFlightsCollection;

class UtairScheduleImportFinishedEvent
{
    public function __construct(private readonly UtairFlightsCollection $collection, private readonly bool $legacy)
    {
    }

    public function getCollection(): UtairFlightsCollection
    {
        return $this->collection;
    }

    public function isLegacy(): bool
    {
        return $this->legacy;
    }
}
