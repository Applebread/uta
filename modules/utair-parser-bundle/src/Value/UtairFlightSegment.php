<?php

namespace App\Modules\UtairParserBundle\Value;

use DateTimeImmutable;

class UtairFlightSegment
{
    public function __construct(
        private readonly int               $flightNumber,
        private readonly string            $departure,
        private readonly string            $arrival,
        private readonly string            $departureTime,
        private readonly string            $arrivalTime,
        private readonly DateTimeImmutable $startPeriodDate,
        private readonly DateTimeImmutable $endPeriodDate,
        private readonly string            $legacyAircraftType,
        private readonly string            $aircraftType,
        private readonly array             $days = [],
    )
    {
    }

    public function getFlightNumber(): int
    {
        return $this->flightNumber;
    }

    public function getDeparture(): string
    {
        return $this->departure;
    }

    public function getLegacyAircraftType(): string
    {
        return $this->legacyAircraftType;
    }

    public function getAircraftType(): string
    {
        return $this->aircraftType;
    }

    public function getArrival(): string
    {
        return $this->arrival;
    }

    public function getDepartureTime(): string
    {
        return $this->departureTime;
    }

    public function getStartPeriodDate(): DateTimeImmutable
    {
        return $this->startPeriodDate;
    }

    public function getEndPeriodDate(): DateTimeImmutable
    {
        return $this->endPeriodDate;
    }

    public function getArrivalTime(): string
    {
        return $this->arrivalTime;
    }

    public function getDays(): array
    {
        return $this->days;
    }

    public function getUuid(): string
    {
        return implode(':', [
            $this->flightNumber,
            $this->departureTime,
            $this->arrivalTime,
            $this->departure,
            $this->arrival,
            $this->startPeriodDate->format('Y-m-d-h-i-s'),
            $this->endPeriodDate->format('Y-m-d-h-i-s'),
            $this->aircraftType,
            implode(',', $this->days),
        ]);
    }
}
