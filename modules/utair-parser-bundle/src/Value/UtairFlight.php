<?php

namespace App\Modules\UtairParserBundle\Value;

use DateTimeImmutable;

class UtairFlight
{
    public function __construct(
        private readonly DateTimeImmutable $startPeriodDate,
        private readonly DateTimeImmutable $endPeriodDate,
        private readonly array $days,
        private readonly array $segments = []
    )
    {
    }

    public function getStartPeriodDate(): DateTimeImmutable
    {
        return $this->startPeriodDate;
    }

    public function getEndPeriodDate(): DateTimeImmutable
    {
        return $this->endPeriodDate;
    }

    /**
     * @return int[]
     */
    public function getDays(): array
    {
        return $this->days;
    }

    /**
     * @return UtairFlightSegment[]
     */
    public function getSegments(): array
    {
        return $this->segments;
    }
}
