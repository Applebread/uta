<?php

declare(strict_types=1);

namespace App\Modules\UtairParserBundle\Value;

use Iterator;
use Traversable;

final class UtairFlightsCollection implements Iterator
{
    private array $flights;

    public function __construct(UtairFlight...$flights)
    {
        $this->flights = $flights;
    }

    public function add(UtairFlight $flight): void
    {
        $this->flights[] = $flight;
    }

    public function getIterator(): Traversable
    {
        foreach ($this->flights as $flight) {
            yield $flight;
        }
    }

    public function current(): UtairFlight
    {
        return current($this->flights);
    }

    public function next(): void
    {
        next($this->flights);
    }

    public function key(): int
    {
        return key($this->flights);
    }

    public function valid(): bool
    {
        return false !== current($this->flights);
    }

    public function rewind(): void
    {
        reset($this->flights);
    }
}
