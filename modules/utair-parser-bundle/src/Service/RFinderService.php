<?php

namespace App\Modules\UtairParserBundle\Service;

use App\Modules\UtairParserBundle\Transport\RfinderClient;
use DateTimeImmutable;
use DOMDocument;
use DOMXPath;
use GuzzleHttp\RequestOptions;

class RFinderService
{
    public function __construct(private readonly RfinderClient $client)
    {
    }

    /**
     * @return string[]
     */
    public function parseRoutes(string $departure, string $arrival): array
    {
        $mainPageResponse = $this->client->get('');
        $mainContent = (string) $mainPageResponse->getBody();

        $mainDoc = new DOMDocument();
        $mainDoc->loadHTML($mainContent, LIBXML_NOWARNING | LIBXML_NOERROR);
        $xpath = new DOMXPath($mainDoc);
        $input = $xpath->query('//input[@type="hidden" and @name="k"]/@value')->item(0);

        $response = $this->client->post('autoroute_rtx.php', [RequestOptions::FORM_PARAMS => [
            'id1' => $departure,
            'ic1' => '',
            'id2' => $arrival,
            'ic2' => '',
            'minalt' => 'FL120',
            'maxalt' => 'FL390',
            'lvl' => 'B',
            'dbid' => '2409',
            'usesid' => 'N',
            'usestar' => 'N',
            'rnav' => 'Y',
            'nats' => 'R',
            'k' => $input->nodeValue
        ]]);

        $responseString = (string) $response->getBody();

        $doc = new DOMDocument();
        $doc->loadHTML($responseString, LIBXML_NOWARNING | LIBXML_NOERROR);

        $routes = iterator_to_array($doc->getElementsByTagName('tt')->getIterator());

        if (empty($routes[1])) {
            return [];
        }

        return [$routes[1]->textContent];
    }
}
