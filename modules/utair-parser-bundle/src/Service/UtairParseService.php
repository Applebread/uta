<?php

namespace App\Modules\UtairParserBundle\Service;

use App\Modules\UtairParserBundle\Interfaces\UtairParseServiceInterface;
use App\Modules\UtairParserBundle\Transport\UtairClient;
use App\Modules\UtairParserBundle\Value\UtairFlight;
use App\Modules\UtairParserBundle\Value\UtairFlightsCollection;
use App\Modules\UtairParserBundle\Value\UtairFlightSegment;
use DateTimeImmutable;
use Doctrine\DBAL\Connection;

class UtairParseService implements UtairParseServiceInterface
{
    private const LEGACY_AIRCRAFT_TYPE_MAP = [
        '738' => 'B738',
        '762' => 'B762',
        '735' => 'B735',
        '734' => 'B734',
        'AT7' => 'AT75',
        'AN4' => 'AN24RV'
    ];

    private const AIRCRAFT_TYPE_MAP = [
        '738' => 'B738',
        '762' => 'B762',
        '735' => 'B735',
        '734' => 'B734',
        'AT7' => 'AT75',
        'AN4' => 'AN24'
    ];

    public function __construct(private readonly UtairClient $client, private readonly Connection $connection)
    {
    }

    public function parseSchedule(): UtairFlightsCollection
    {
        $collection = new UtairFlightsCollection();
        $today = (new DateTimeImmutable())->format('Y-m-d');
        $currentYear = (new DateTimeImmutable())->format('Y');
        $endOfTheYear = "$currentYear-12-31";
        $chunkSize = 100; // max 100
        return $this->parseParts($chunkSize, $today, $endOfTheYear, $collection);
    }

    private function fillCollection(UtairFlightsCollection $collection, array $data): UtairFlightsCollection
    {
        foreach ($data['objects'] as $object) {
            foreach ($object['flights'] as $flight) {
                $utairFlight = new UtairFlight(
                    new DateTimeImmutable($flight['start_period_date']),
                    new DateTimeImmutable($flight['end_period_date']),
                    $flight['days'],
                    array_map(function (array $segmentData) {
                        return new UtairFlightSegment(
                            (int)$segmentData['flight_number'],
                            $this->getICAOByIATA($segmentData['departure_airport_code']),
                            $this->getICAOByIATA($segmentData['arrival_airport_code']),
                            $segmentData['departure_time'],
                            $segmentData['arrival_time'],
                            new DateTimeImmutable($segmentData['period']['start_period_date']),
                            new DateTimeImmutable($segmentData['period']['end_period_date']),
                            self::getLegacyAircraftType($segmentData['airplane']),
                            self::getAircraftType($segmentData['airplane']),
                            self::normalizeDaysArray($segmentData['period']['days'])
                        );
                    }, array_filter($flight['segments'], function (array $segmentData) {
                        return self::getAircraftType($segmentData['airplane']) && self::getLegacyAircraftType($segmentData['airplane']) && $segmentData['landings'] === 0 && !$segmentData['operating_company'];
                    }))
                );

                $collection->add($utairFlight);
            }
        }

        return $collection;
    }

    private static function normalizeDaysArray(array $days): array
    {
        return array_map(function (int $day) use ($days) {
            return in_array($day, $days) ? (int) $day : 0;
        }, range(1, 7));
    }

    public function parseParts(int $chunkSize, string $today, string $endOfTheYear, UtairFlightsCollection $collection, ?string $cityCode = null): UtairFlightsCollection
    {
        $requestUrl = "schedule?skip=0&limit=$chunkSize".($cityCode ? '&departureCityCode='.$cityCode : '')."&departureDateFrom=$today&departureDateTo=$endOfTheYear&isDirect=1";
        $firstPartResponse = $this->client->get($requestUrl);
        $firstPartData = json_decode((string)$firstPartResponse->getBody(), true);
        $total = (int)$firstPartData['data']['total'];

        $collection = $this->fillCollection($collection, $firstPartData['data']);

        $parsed = $chunkSize;
        while ($parsed < $total) {
            $url = "schedule?skip=$parsed&limit=$chunkSize".($cityCode ? '&departureCityCode='.$cityCode : '')."&departureDateFrom=$today&departureDateTo=$endOfTheYear&isDirect=1";
            $response = $this->client->get($url);
            $responseData = json_decode((string)$response->getBody(), true);
            $collection = $this->fillCollection($collection, $responseData['data']);
            $parsed += $chunkSize;
        }
        return $collection;
    }

    private function getICAOByIATA(string $iata): ?string
    {
        $res = $this->connection->fetchAllAssociative("SELECT icao FROM airports_dictionary WHERE iata='$iata' LIMIT 1");

        return !empty($res[0]) ? $res[0]['icao'] : null;
    }

    private static function getLegacyAircraftType(string $type): ?string
    {
        return self::LEGACY_AIRCRAFT_TYPE_MAP[$type] ?? null;
    }

    private static function getAircraftType(string $type): ?string
    {
        return self::AIRCRAFT_TYPE_MAP[$type] ?? null;
    }
}
