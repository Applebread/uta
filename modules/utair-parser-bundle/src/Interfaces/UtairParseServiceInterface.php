<?php

namespace App\Modules\UtairParserBundle\Interfaces;

use App\Modules\UtairParserBundle\Value\UtairFlightsCollection;

interface UtairParseServiceInterface
{
    public function parseSchedule(): UtairFlightsCollection;
}
