<?php

namespace App\Modules\UtairParserBundle;

use Exception;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class UtairParserBundle extends Bundle
{
    public function getContainerExtension()
    {
        return null;
    }

    /**
     * @throws Exception
     */
    public function build(ContainerBuilder $container)
    {
        if ($container->isCompiled()) {
            @trigger_error('Container already compiled. Can not set parameters', E_USER_WARNING);

            return;
        }

        $loader = new YamlFileLoader($container, new FileLocator(__DIR__.'/Resources/config'));
        $loader->load('parameters.yaml');
        $loader->load('services.yaml');

        parent::build($container);
    }
}
