<?php

namespace App\Modules\UtairParserBundle\Command;

use App\Modules\UtairParserBundle\Event\UtairScheduleImportFinishedEvent;
use App\Modules\UtairParserBundle\Interfaces\UtairParseServiceInterface;
use Psr\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class ParseFlightsCommand extends Command
{
    public function __construct(
        private readonly UtairParseServiceInterface $utairParseService,
        private readonly EventDispatcherInterface   $eventDispatcher,
    )
    {
        parent::__construct('utair:import-import-schedule');
    }

    protected function configure(): void
    {
        $this->setDescription('Import UTAIR schedule');
        $this->setHelp('Example: bin/console utair:import-import-schedule');

        $this->addOption(
            'legacy',
            'l',
            null,
            'if legacy - it will create old format CSV file'
        );
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->eventDispatcher->dispatch(
            new UtairScheduleImportFinishedEvent($this->utairParseService->parseSchedule(), $input->getOption('legacy') ?? false)
        );

        return 0;
    }
}
