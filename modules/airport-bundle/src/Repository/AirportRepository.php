<?php

namespace App\Modules\AirportBundle\Repository;

use App\Modules\AirportBundle\Entity\Airport;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class AirportRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Airport::class);
    }

    public function findByIcao(string $icao): ?Airport
    {
        return $this->findOneBy(['icao' => $icao]);
    }

    public function clearAirports(): int
    {
        $qb = $this->createQueryBuilder('t');

        $qb->delete()->where('t.source=:source')->setParameter('source', Airport::SOURCE_AIRAC);

        return $qb->getQuery()->getSingleScalarResult() ?? 0;
    }

    public function findNearestByRange(int $range, Airport $airport): ?array
    {
        $area = $range / 111;

        return $this->createQueryBuilder('a')
            ->select()
            ->where('a.icao <> :airport 
            AND a.latitude BETWEEN :minLatRange AND :maxLatRange 
            AND a.longitude BETWEEN :minLongRange AND :maxLongRange
            ')
            ->setParameter('airport', $airport->getIcao())
            ->setParameter('minLatRange', $airport->getLatitude() - $area)
            ->setParameter('maxLatRange', $airport->getLatitude() + $area)
            ->setParameter('minLongRange', $airport->getLongitude() - $area)
            ->setParameter('maxLongRange', $airport->getLongitude() + $area)
            ->getQuery()->getResult();
    }

    public function save(Airport $airport): Airport
    {
        $this->_em->persist($airport);
        $this->_em->flush();

        return $airport;
    }
}
