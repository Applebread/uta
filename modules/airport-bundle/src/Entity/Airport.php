<?php

namespace App\Modules\AirportBundle\Entity;

use App\Modules\AirportBundle\Repository\AirportRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: AirportRepository::class)]
class Airport
{
    const SOURCE_AIRAC = 'airac';

    const SOURCE_MANUAL = 'manual';

    const SOURCE_EXTERNAL = 'external';

    #[ORM\Id]
    #[ORM\Column(type: 'string', length: 4, unique: true)]
    private string $icao;

    #[ORM\Column(type: 'string', length: 3)]
    private string $iata;

    #[ORM\Column(type: 'string')]
    private string $name;

    #[ORM\Column(type: 'decimal', precision: 20, scale: 16, nullable: true)]
    private ?string $latitude;

    #[ORM\Column(type: 'decimal', precision: 20, scale: 16, nullable: true)]
    private ?string $longitude;

    #[ORM\Column(type: 'integer')]
    private int $elevation;

    #[ORM\Column(type: 'string')]
    private string $city;

    #[ORM\Column(type: 'string')]
    private string $timezone;

    #[ORM\Column(type: 'string')]
    private string $source;

    public function __construct(
        string $icao,
        string $iata,
        string $name,
        ?string $latitude,
        ?string $longitude,
        int $elevation,
        string $city,
        string $timezone,
        ?string $source = self::SOURCE_MANUAL
    )
    {
        $this->icao = $icao;
        $this->iata = $iata;
        $this->name = $name;
        $this->latitude = $latitude;
        $this->longitude = $longitude;
        $this->elevation = $elevation;
        $this->city = $city;
        $this->timezone = $timezone;
        $this->source = $source;
    }

    public function getIcao(): string
    {
        return $this->icao;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getLatitude(): ?string
    {
        return $this->latitude;
    }

    public function getLongitude(): ?string
    {
        return $this->longitude;
    }

    public function getCity(): string
    {
        return $this->city;
    }

    public function getTimezone(): string
    {
        return $this->timezone;
    }

    public function setTimezone(string $timezone): void
    {
        $this->timezone = $timezone;
    }

    public function getSource(): string
    {
        return $this->source;
    }

    public function setIcao(string $icao): void
    {
        $this->icao = $icao;
    }

    public function setIata(string $iata): void
    {
        $this->iata = $iata;
    }

    public function setElevation(int $elevation): void
    {
        $this->elevation = $elevation;
    }

    /**
     * @param string|null $latitude
     */
    public function setLatitude(?string $latitude): void
    {
        $this->latitude = $latitude;
    }

    public function setLongitude(?string $longitude): void
    {
        $this->longitude = $longitude;
    }

    public function setCity(string $city): void
    {
        $this->city = $city;
    }
}
