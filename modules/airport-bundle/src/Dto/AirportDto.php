<?php

declare(strict_types=1);

namespace App\Modules\AirportBundle\Dto;

use App\Modules\AirportBundle\Entity\Airport;
use Symfony\Component\HttpFoundation\Request;

final class AirportDto
{
    private function __construct(
        public string  $icao,
        public string  $name,
        public ?string $latitude,
        public ?string $longitude,
        public string  $city,
        public string  $timezone,
        public string  $source,
        public ?string  $currentMetar  = null,
    )
    {
    }

    public static final function fromEntityAndMetarString(Airport $airport, ?string $metar = null): self
    {
        return new self(
            $airport->getIcao(),
            $airport->getName(),
            $airport->getLatitude(),
            $airport->getLongitude(),
            $airport->getCity(),
            $airport->getTimezone(),
            $airport->getSource(),
            $metar
        );
    }

    public static final function fromRequest(Request $request): self
    {
        $data = json_decode((string)$request->getContent(), true);

        return new self(
            $data['icao'],
            $data['name'],
            $data['latitude'],
            $data['longitude'],
            $data['city'],
            $data['timezone'],
            $data['source'] ?? ''
        );
    }
}
