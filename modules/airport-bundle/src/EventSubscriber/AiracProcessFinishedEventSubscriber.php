<?php

declare(strict_types=1);

namespace App\Modules\AirportBundle\EventSubscriber;

use App\Modules\AiracUploaderBundle\Event\AiracProcessFinishedEvent;
use App\Modules\AiracUploaderBundle\Value\AiracAirport;
use App\Modules\AirportBundle\Entity\Airport;
use App\Modules\AirportBundle\Repository\AirportRepository;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

final class AiracProcessFinishedEventSubscriber implements EventSubscriberInterface
{
    public function __construct(private AirportRepository $airportRepository)
    {
    }

    public static function getSubscribedEvents(): array
    {
        return [
            AiracProcessFinishedEvent::NAME => 'airacProcessFinished'
        ];
    }

    public function airacProcessFinished(AiracProcessFinishedEvent $event): void
    {
        foreach ($event->airportCollection() as $airacAirport) {
            if ($airport = $this->airportRepository->findByIcao($airacAirport->icao())) {
                $airport->setElevation($airacAirport->elevation());
                $airport->setLatitude((string) ($airacAirport->latitude() / 1000000));
                $airport->setLongitude((string) ($airacAirport->longitude() / 1000000));
                $airport->setName($airacAirport->name());
                $airport->setTimezone('');
            } else {
                $airport = new Airport(
                    $airacAirport->icao(),
                    '',
                    $airacAirport->name(),
                    (string) ($airacAirport->latitude() / 1000000),
                    (string) ($airacAirport->longitude() / 1000000),
                    $airacAirport->elevation(),
                    $airacAirport->name(),
                    '',
                    Airport::SOURCE_AIRAC
                );
            }

            $this->airportRepository->save($airport);
        }
    }
}
