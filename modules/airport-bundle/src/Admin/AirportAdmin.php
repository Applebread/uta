<?php

namespace App\Modules\AirportBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollectionInterface;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class AirportAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $form): void
    {
        $form
            ->add('name', TextType::class)
            ->add('icao', TextType::class)
            ->add('iata', TextType::class)
            ->add('city', TextType::class)
            ->add('longitude', TextType::class)
            ->add('latitude', TextType::class)
            ->add('elevation', NumberType::class)
            ->add('timezone', NumberType::class);
    }

    protected function configureDatagridFilters(DatagridMapper $datagrid): void
    {
        $datagrid
            ->add('id')
            ->add('name')
            ->add('icao')
            ->add('iata')
            ->add('city')
            ->add('latitude')
            ->add('longitude')
            ->add('elevation')
            ->add('timezone');
    }

    protected function configureActionButtons(array $buttonList, string $action, ?object $object = null): array
    {
        $list = parent::configureActionButtons($buttonList, $action, $object);

        $list['import_from_infogate'] = array(
            'template' =>  '@AirportBundle/infogate_btn.twig',
        );

        return $list;
    }

    protected function configureListFields(ListMapper $list): void
    {
        $list->addIdentifier('id')
            ->add('name')
            ->add('icao')
            ->add('iata')
            ->add('city')
            ->add('latitude')
            ->add('longitude')
            ->add('elevation')
            ->add('timezone')
            ->add(ListMapper::NAME_ACTIONS, null, [
                'actions' => [
                    'edit' => [],
                    'delete' => []
                ]
            ]);
    }

    protected function configureRoutes(RouteCollectionInterface $collection): void
    {
        $collection->add('importFromInfogate','import');
    }
}
