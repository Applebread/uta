<?php

declare(strict_types=1);

namespace App\Modules\AirportBundle\Service;

use App\Modules\AirportBundle\Dto\AirportDto;
use App\Modules\AirportBundle\Dto\AirportsListDto;
use App\Modules\AirportBundle\Interfaces\AirportListServiceInterface;
use App\Modules\AirportBundle\Repository\AirportRepository;

class AirportListService implements AirportListServiceInterface
{
    public function __construct(private AirportRepository $airportRepository)
    {
    }

    public function getAirportList(int $page, int $pageSize): AirportsListDto
    {
        $airports = $this->airportRepository->findBy([], ['icao' => 'ASC'], $pageSize, ($page - 1) * $pageSize);
        $totalCount = $this->airportRepository->count([]);

        $airportsList = [];

        foreach ($airports as $airport) {
            $airportsList[] = AirportDto::fromEntityAndMetarString($airport);
        }

        return new AirportsListDto($totalCount, ...$airportsList);
    }
}
