<?php

declare(strict_types=1);

namespace App\Modules\AirportBundle\Service;

use App\Modules\AirportBundle\Dto\AirportDto;
use App\Modules\AirportBundle\Entity\Airport;
use App\Modules\AirportBundle\Interfaces\AirportDetailsServiceInterface;
use App\Modules\AirportBundle\Repository\AirportRepository;

class AirportDetailsService implements AirportDetailsServiceInterface
{
    public function __construct(private AirportRepository $airportRepository)
    {
    }

    public function getAirportByIcao(string $icao): ?Airport
    {
        return $this->airportRepository->findOneBy(['icao' => $icao]);
    }

    public function getNearestByRange(int $rangeKilometers, Airport $airport): array
    {
        return $this->airportRepository->findNearestByRange($rangeKilometers, $airport);
    }

    public function updateFromRequest(Airport $airport, AirportDto $airportDto): Airport
    {
        $airport->setIcao($airportDto->icao);
        $airport->setCity($airportDto->city);
        $airport->setName($airportDto->name);
        $airport->setLatitude($airportDto->latitude);
        $airport->setLongitude($airportDto->longitude);
        $airport->setTimezone($airportDto->timezone);

        return $this->airportRepository->save($airport);
    }

    public function createFromRequest(AirportDto $airportDto): Airport
    {
        $airport = new Airport(
            $airportDto->icao,
            '',
            $airportDto->name,
            $airportDto->latitude,
            $airportDto->longitude,
            0,
            $airportDto->city,
            $airportDto->timezone,
            Airport::SOURCE_MANUAL
        );

        $this->airportRepository->save($airport);

        return $airport;
    }
}
