<?php

namespace App\Modules\AirportBundle\Controller;

use App\Modules\AirportBundle\Dto\AirportDto;
use App\Modules\AirportBundle\Entity\Airport;
use App\Modules\AirportBundle\Interfaces\AirportDetailsServiceInterface;
use App\Modules\AirportBundle\Interfaces\AirportListServiceInterface;
use App\Modules\AirportBundle\Repository\AirportRepository;
use App\Modules\InfogateParserBundle\Interfaces\InfogateAirportServiceInterface;
use OpenApi\Annotations as OA;
use OpenApi\Attributes\Parameter;
use OpenApi\Attributes\Tag;
use Nelmio\ApiDocBundle\Annotation as NA;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class AirportAdminController extends AbstractController
{
    public function __construct(
        private readonly InfogateAirportServiceInterface $infogateAirportService,
        private readonly AirportRepository $airportRepository,
        private readonly AirportListServiceInterface $airportListService,
        private readonly AirportDetailsServiceInterface $airportDetailsService
    )
    {
    }

    #[Route(path: '/api/admin/airports/infogate-import', name: 'airport_infogate_import', methods: ['GET'])]
    #[Tag('Airport')]
    #[IsGranted('ROLE_ADMIN')]
    public function importFromInfogateAction(): JsonResponse
    {
        $airports = $this->infogateAirportService->getAirportList();

        foreach ($airports->getIterator() as $infogateAirport) {
            $this->airportRepository->save(
                new Airport(
                    $infogateAirport->getIcaoCodeIntl(),
                    $infogateAirport->getIataCode(),
                    $infogateAirport->getName(),
                    null,
                    null,
                    0,
                    $infogateAirport->getCityName(),
                    $infogateAirport->getTimeZone(),
                    Airport::SOURCE_EXTERNAL
                )
            );
        }

        return new JsonResponse();
    }

    #[Route(path: '/api/airports/list', name: 'airports_list', methods: ['GET'])]
    #[Tag('Airport')]
    #[Parameter(name: 'page', in: 'query')]
    #[Parameter(name: 'page_size', in: 'query')]
    #[IsGranted('ROLE_ADMIN')]
    public function getAirportList(Request $request): JsonResponse
    {
        return new JsonResponse(
            $this->airportListService->getAirportList(
                (int) $request->get('page', 1),
                (int) $request->get('page_size', 15)
            )
        );
    }

    #[Route(path: '/api/airport/{airport}', name: 'airport', methods: ['GET'])]
    #[Tag('Airport')]
    #[Parameter(name: 'airport', in: 'query', ref: Airport::class)]
    #[IsGranted('ROLE_ADMIN')]
    public function getAirport(Airport $airport): JsonResponse
    {
        return new JsonResponse(AirportDto::fromEntityAndMetarString($airport));
    }

    /** @OA\RequestBody(@OA\JsonContent(ref=@NA\Model(type=AirportDto::class))) */
    #[Route(path: '/api/airport/{airport}', name: 'airport_update', methods: ['PUT'])]
    #[Tag('Airport')]
    #[Parameter(name: 'airport', in: 'query', ref: Airport::class)]
    #[IsGranted('ROLE_ADMIN')]
    public function updateAirport(Airport $airport, Request $request): JsonResponse
    {
        $airport = $this->airportDetailsService->updateFromRequest($airport, AirportDto::fromRequest($request));

        return new JsonResponse(AirportDto::fromEntityAndMetarString($airport));
    }

    /** @OA\RequestBody(@OA\JsonContent(ref=@NA\Model(type=AirportDto::class))) */
    #[Route(path: '/api/airport', name: 'airport_create', methods: ['POST'])]
    #[Tag('Airport')]
    #[IsGranted('ROLE_ADMIN')]
    public function createAirport(Request $request): JsonResponse
    {
        $airport = $this->airportDetailsService->createFromRequest(AirportDto::fromRequest($request));

        return new JsonResponse(AirportDto::fromEntityAndMetarString($airport));
    }
}
