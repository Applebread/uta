<?php

namespace App\Modules\AirportBundle\Controller;

use App\Modules\AirportBundle\Dto\AirportDto;
use App\Modules\AirportBundle\Entity\Airport;
use App\Modules\AirportBundle\Interfaces\AirportDetailsServiceInterface;
use App\Modules\CheckWxApiBundle\Interfaces\CheckWxServiceInterface;
use OpenApi\Attributes\Tag;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class AirportInfoController
{

    #[Route(path: '/api/airports/{icao}/alternates', name: 'airport_show_alternates', methods: ['GET'])]
    #[Tag('Airport')]
    #[IsGranted('ROLE_PILOT')]
    public function showAirportAlternates(string $icao, CheckWxServiceInterface $checkWxService, AirportDetailsServiceInterface $airportDetailsService): JsonResponse
    {
        $airport = $airportDetailsService->getAirportByIcao($icao);
        $alternates = $airportDetailsService->getNearestByRange(500, $airport);

        $alternates = array_slice($alternates, 0, 5);
        $metars = $checkWxService->getWeatherByIcaoList(array_map(function (Airport $airport) { return $airport->getIcao(); }, $alternates));

        return new JsonResponse(array_map(function (Airport $airport) use ($metars) {
            return AirportDto::fromEntityAndMetarString($airport, $metars[$airport->getIcao()] ?? null);
        }, $alternates));
    }
}
