<?php

declare(strict_types=1);

namespace App\Modules\TripBundle\Interfaces;

use App\Modules\TripBundle\Entity\Trip;

interface TripInfoProviderInterface
{
    public function provideActiveTripInfo(): ?Trip;
}
