<?php

namespace App\Modules\TripBundle\Interfaces;

use App\Modules\TripBundle\Entity\Trip;
use App\Modules\TripBundle\Value\TripRequest;

interface TripStarterInterface
{
    public function startAction(TripActionInterface $action, TripRequest $tripRequest): Trip;
}
