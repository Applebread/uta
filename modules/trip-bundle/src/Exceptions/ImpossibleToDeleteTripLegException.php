<?php

declare(strict_types=1);

namespace App\Modules\TripBundle\Exceptions;

use InvalidArgumentException;

final class ImpossibleToDeleteTripLegException extends InvalidArgumentException
{
    public static function throw(): void
    {
        throw new self("Impossible to delete single trip leg");
    }
}
