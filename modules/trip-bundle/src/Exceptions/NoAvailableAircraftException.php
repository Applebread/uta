<?php

declare(strict_types=1);

namespace App\Modules\TripBundle\Exceptions;

use InvalidArgumentException;

final class NoAvailableAircraftException extends InvalidArgumentException
{
    public static function throwFromType(string $aircraftTypeIcao): void
    {
        throw new self("Available aircraft with type '$aircraftTypeIcao' wasn't found");
    }
}
