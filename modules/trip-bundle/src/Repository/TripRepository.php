<?php

namespace App\Modules\TripBundle\Repository;

use App\Modules\PilotBundle\Entity\Pilot;
use App\Modules\TripBundle\Entity\Trip;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class TripRepository extends ServiceEntityRepository
{
    const ACTIVE_STATUSES = [Trip::STATUS_BOOKED, Trip::STATUS_PROGRESS];

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Trip::class);
    }

    public function save(Trip $trip): void
    {
        $this->_em->persist($trip);
        $this->_em->flush();
    }

    public function delete(Trip $trip): void
    {
        $this->_em->remove($trip);
        $this->_em->flush();
    }

    public function findActiveByCaptainOrFirstOfficer(Pilot $pilot): ?Trip
    {
        $qb = $this->createQueryBuilder('trip');
        $qb->where($qb->expr()->eq('trip.captain', ':captain'));
        $qb->orWhere($qb->expr()->eq('trip.firstOfficer', ':fo'));
        $qb->andWhere($qb->expr()->in('trip.status', self::ACTIVE_STATUSES));
        $qb->setParameter(':captain', $pilot);
        $qb->setParameter(':fo', $pilot);

        return $qb->getQuery()->getOneOrNullResult();
    }
}
