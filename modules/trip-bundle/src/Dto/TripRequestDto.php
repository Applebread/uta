<?php

namespace App\Modules\TripBundle\Dto;

use DateTimeImmutable;
use Symfony\Component\HttpFoundation\Request;

final class TripRequestDto
{
    const DATE_OF_FLIGHT_DATE_FORMAT = 'Y-m-d';

    /**
     * @var string|null
     */
    private ?string $aircraftType;

    /**
     * @var string|null
     */
    private ?string $dateOfFlight;

    /**
     * @var string|null
     */
    private ?string $from;

    /**
     * @var string|null
     */
    private ?string $to;

    /**
     * @var bool
     */
    private bool $availableForMe;

    private function __construct(
        ?string $aircraftType,
        ?string $dateOfFlight,
        ?string $from,
        ?string $to,
        ?bool $availableForMe = false
    )
    {
        $this->aircraftType = $aircraftType;
        $this->dateOfFlight = $dateOfFlight;
        $this->from = $from;
        $this->to = $to;
        $this->availableForMe = $availableForMe;
    }

    public static function fromRequest(Request $request): self
    {
        $data = json_decode($request->getContent(), true);

        return new self(
            $data['aircraftType'] ?? null,
            $data['dateOfFlight'] ?? null,
            $data['from'] ?? null,
            $data['to'] ?? null,
            $data['availableForMe'],
        );
    }

    public function getAircraftType(): ?string
    {
        return $this->aircraftType;
    }

    public function getDateOfFlight(): ?DateTimeImmutable
    {
        if (!$this->dateOfFlight) {
            return null;
        }

        return DateTimeImmutable::createFromFormat(self::DATE_OF_FLIGHT_DATE_FORMAT, $this->dateOfFlight);
    }

    public function getFrom(): ?string
    {
        return $this->from;
    }

    public function getTo(): ?string
    {
        return $this->to;
    }

    public function isAvailableForMe(): bool
    {
        return $this->availableForMe;
    }
}
