<?php

declare(strict_types=1);

namespace App\Modules\TripBundle\Dto;

use Symfony\Component\HttpFoundation\Request;

final class LegAlternatesDto
{
    private function __construct(/** @var string[] */public array $alternates)
    {
    }

    public static function fromRequest(Request $request): self
    {
        $requestData = json_decode($request->getContent(), true);
        return new self($requestData['alternates']);
    }

    public function alternates(): array
    {
        return $this->alternates;
    }
}
