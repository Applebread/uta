<?php

declare(strict_types=1);

namespace App\Modules\TripBundle\Dto;

use App\Modules\AircraftBundle\Dto\AircraftDto;
use App\Modules\TripBundle\Entity\Trip;
use App\Modules\TripBundle\Entity\TripLeg;

final class TripDto
{
    public readonly int $id;
    public readonly string $captain;
    public readonly ?string $firstOfficer;
    public readonly string $from;
    public readonly string $to;
    public readonly float $dutyTime;
    public readonly AircraftDto $aircraft;
    public readonly string $status;
    public readonly array  $legs;

    public static function fromEntity(Trip $trip): self
    {
        $me = new self();

        $dutyTime = 0;
        foreach ($trip->getLegs() as $leg) {
            $dutyTime += $leg->getAverageDutyTimeMinutes();
        }

        $me->id = $trip->id();
        $me->captain = $trip->captain()->getFullName();
        $me->firstOfficer = $trip->firstOfficer()?->getFullName();
        $me->from = $trip->from()->getIcao();
        $me->to = $trip->to()->getIcao();
        $me->status = $trip->getStatus();
        $me->dutyTime = $dutyTime;
        $me->aircraft = AircraftDto::fromEntity($trip->getAssignedAircraft());
        $me->legs = $trip->getLegs()->map(function (TripLeg $leg) {
            return TripLegDto::fromEntity($leg);
        })->toArray();
        return $me;
    }
}
