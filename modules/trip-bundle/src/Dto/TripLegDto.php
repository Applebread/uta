<?php

declare(strict_types=1);

namespace App\Modules\TripBundle\Dto;

use App\Modules\AirportBundle\Dto\AirportDto;
use App\Modules\TripBundle\Entity\TripLeg;

final class TripLegDto
{
    private function __construct(
        public readonly int        $id,
        public readonly string     $callsign,
        public readonly string     $status,
        public readonly AirportDto $from,
        public readonly AirportDto $to,
        public readonly float      $averageDutyTime,
        public readonly float      $averageDistance,
        public readonly ?string    $departureTime,
        /** @var AirportDto[] */
        public readonly array      $selectedAlternates
    )
    {
    }

    public static function fromEntity(TripLeg $tripLeg): self
    {
        return new self(
            $tripLeg->getId(),
            $tripLeg->getFlightNumber(),
            $tripLeg->getStatus(),
            AirportDto::fromEntityAndMetarString($tripLeg->getFrom()),
            AirportDto::fromEntityAndMetarString($tripLeg->getTo()),
            $tripLeg->getAverageDutyTimeMinutes(),
            $tripLeg->getDistance(),
            $tripLeg->departureDateTime()?->format('H:i:s'),
            $tripLeg->getSelectedAlternates()
        );
    }
}
