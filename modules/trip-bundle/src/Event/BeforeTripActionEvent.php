<?php

namespace App\Modules\TripBundle\Event;

use App\Modules\PilotBundle\Entity\Pilot;
use App\Modules\TripBundle\Entity\Trip;

class BeforeTripActionEvent
{
    public function __construct(private readonly Pilot $pilot, private readonly ?Trip $trip = null)
    {
    }

    public function trip(): ?Trip
    {
        return $this->trip;
    }

    public function pilot(): Pilot
    {
        return $this->pilot;
    }
}
