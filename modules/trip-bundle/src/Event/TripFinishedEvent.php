<?php

declare(strict_types=1);

namespace App\Modules\TripBundle\Event;

use App\Modules\AircraftBundle\Entity\Aircraft;
use App\Modules\TripBundle\Entity\Trip;

class TripFinishedEvent
{
    public function __construct(private readonly Trip $trip, private readonly ?Aircraft $assignedAircraft)
    {
    }

    public function trip(): Trip
    {
        return $this->trip;
    }

    public function assignedAircraft(): ?Aircraft
    {
        return $this->assignedAircraft;
    }
}
