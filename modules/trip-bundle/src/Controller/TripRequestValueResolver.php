<?php

declare(strict_types=1);

namespace App\Modules\TripBundle\Controller;

use App\Modules\PilotBundle\Entity\Pilot;
use App\Modules\PilotBundle\Interfaces\CurrentPilotInterface;
use App\Modules\TimetableBundle\Entity\Flight;
use App\Modules\TimetableBundle\Repository\FlightRepository;
use App\Modules\TripBundle\Dto\BookingRequestDto;
use App\Modules\TripBundle\Dto\FinishLegDto;
use App\Modules\TripBundle\Entity\Trip;
use App\Modules\TripBundle\Entity\TripLeg;
use App\Modules\TripBundle\Repository\TripRepository;
use App\Modules\TripBundle\Value\TripRequest;
use ReflectionClass;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ArgumentValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;

class TripRequestValueResolver implements ArgumentValueResolverInterface
{

    public function __construct(
        private readonly CurrentPilotInterface $currentPilotService,
        private readonly TripRepository $tripRepository,
        private readonly FlightRepository $flightRepository
    )
    {
    }

    public function supports(Request $request, ArgumentMetadata $argument): bool
    {
        return $argument->getType() !== null
            && class_exists($argument->getType())
            && in_array(TripRequest::class, class_parents($argument->getType()))
            && ($request->attributes->has('trip') || $request->attributes->has('flight'));
    }

    public function resolve(Request $request, ArgumentMetadata $argument): iterable|\Generator
    {
        /** @var TripRequest $resolvedArgumentType */
        $resolvedArgumentType = $argument->getType();

        $reflection = new ReflectionClass($resolvedArgumentType);

        $args = [];

        foreach ($reflection->getConstructor()->getParameters() as $parameter)
        {
            switch ($parameter->getType()->getName()) {
                case Pilot::class:
                    $args[] = $this->currentPilotService->getCurrentPilot();
                    break;
                case Trip::class:
                    $args[] = $this->tripRepository->find($request->attributes->get('trip'));
                    break;
                case TripLeg::class:
                    $args[] = $request->attributes->get('leg');
                    break;
                case Flight::class:
                    $args[] = $this->flightRepository->find($request->attributes->get('flight'));
                    break;
                case TripRepository::class:
                    $args[] = $this->tripRepository;
                    break;
                case FinishLegDto::class:
                    $args[] = FinishLegDto::fromRequest($request);
                    break;
                case BookingRequestDto::class:
                    $args[] = BookingRequestDto::fromRequest($request);
                    break;
                default:
                    continue 2;
            }
        }

        yield new $resolvedArgumentType(...$args);
    }
}
