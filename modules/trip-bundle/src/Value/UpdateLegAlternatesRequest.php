<?php

declare(strict_types=1);

namespace App\Modules\TripBundle\Value;

use App\Modules\PilotBundle\Entity\Pilot;
use App\Modules\TripBundle\Entity\Trip;
use App\Modules\TripBundle\Entity\TripLeg;
use App\Modules\TripBundle\Repository\TripRepository;

class UpdateLegAlternatesRequest extends TripRequest
{
    private array $alternates;

    public function __construct(
        Pilot $pilot, Trip $trip, TripRepository $repository, private readonly TripLeg $leg
    )
    {
        parent::__construct($pilot, $repository, $trip);
    }

    public function leg(): TripLeg
    {
        return $this->leg;
    }

    public function setAlternates(array $alternates): void
    {
        $this->alternates = $alternates;
    }

    public function getAlternates(): array
    {
        return $this->alternates;
    }
}
