<?php

declare(strict_types=1);

namespace App\Modules\TripBundle\Value;

use App\Modules\PilotBundle\Entity\Pilot;
use App\Modules\TripBundle\Entity\Trip;
use App\Modules\TripBundle\Repository\TripRepository;

class RemoveLastLegRequest extends TripRequest
{
    public function __construct(Pilot $pilot, Trip $trip, TripRepository $repository)
    {
        parent::__construct($pilot, $repository, $trip);
    }
}
