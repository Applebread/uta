<?php

declare(strict_types=1);

namespace App\Modules\TripBundle\Value;

use App\Modules\PilotBundle\Entity\Pilot;
use App\Modules\TripBundle\Dto\FinishLegDto;
use App\Modules\TripBundle\Entity\Trip;
use App\Modules\TripBundle\Repository\TripRepository;

class FinishLegRequest extends TripRequest
{
    public function __construct(Pilot $pilot, Trip $trip, TripRepository $repository, private readonly FinishLegDto $lastLegData)
    {
        parent::__construct($pilot, $repository, $trip);
    }

    public function lastLegData(): FinishLegDto
    {
        return $this->lastLegData;
    }
}
