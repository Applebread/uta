<?php

declare(strict_types=1);

namespace App\Modules\TripBundle\Entity;

use App\Modules\AircraftBundle\Entity\Aircraft;
use App\Modules\AirportBundle\Entity\Airport;
use App\Modules\PilotBundle\Entity\Pilot;
use App\Modules\TimetableBundle\Entity\Flight;
use App\Modules\TripBundle\Exceptions\ImpossibleToAddTripLegException;
use App\Modules\TripBundle\Exceptions\ImpossibleToDeleteTripLegException;
use App\Modules\TripBundle\Exceptions\TripLegsReadonlyException;
use DateTimeImmutable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use InvalidArgumentException;

#[ORM\Entity]
#[ORM\UniqueConstraint(columns: ['id', 'captain_id', 'status'])]
#[ORM\UniqueConstraint(columns: ['id', 'first_officer', 'status'])]
class Trip
{
    const STATUS_BOOKED = 'booked';
    const STATUS_PROGRESS = 'progress';
    const STATUS_FINISHED = 'finished';

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private int $id;

    #[ORM\ManyToOne(targetEntity: Pilot::class)]
    private readonly Pilot $captain;

    #[ORM\ManyToOne(targetEntity: Pilot::class)]
    #[ORM\JoinColumn(name: 'first_officer', referencedColumnName: 'id', nullable: true)]
    private ?Pilot $firstOfficer = null;

    #[ORM\OneToOne(targetEntity: Aircraft::class)]
    #[ORM\JoinColumn(name: 'aircraft_id', referencedColumnName: 'id', nullable: true)]
    private ?Aircraft $aircraft;

    #[ORM\Column(type: 'datetime_immutable')]
    private readonly DateTimeImmutable $bookingDateTime;

    #[ORM\Column(type: 'string')]
    private string $status;

    #[ORM\OneToMany(mappedBy: 'trip', targetEntity: TripLeg::class, cascade: ['persist', 'remove'], orphanRemoval: true)]
    #[ORM\OrderBy(['id' => 'ASC'])]
    private Collection $legs;

    #[ORM\Column(type: 'datetime_immutable')]
    private readonly DateTimeImmutable $etd;

    #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    private ?DateTimeImmutable $finished = null;

    public function __construct(
        Pilot $captain,
        Aircraft $aircraft,
        DateTimeImmutable $etd
    )
    {
        $this->bookingDateTime = new DateTimeImmutable('now');
        $this->status = self::STATUS_BOOKED;
        $this->captain = $captain;
        $this->aircraft = $aircraft;
        $this->etd = $etd;
        $this->legs = new ArrayCollection();
        $this->aircraft->assignToTrip($this);
    }

    public function id(): int
    {
        return $this->id;
    }

    public function addFirstOfficer(Pilot $firstOfficer): void
    {
        if ($this->status !== self::STATUS_BOOKED) {
            throw new InvalidArgumentException("Impossible to join this trip");
        }

        $this->firstOfficer = $firstOfficer;
    }

    public function from(): Airport
    {
        return $this->legs->first()->getFrom();
    }

    public function dutyHours(): float
    {
        return array_sum(array_map(function (TripLeg $leg) {
            return $leg->getFlightTime();
        }, $this->legs->toArray()));
    }

    public function to(): Airport
    {
        return $this->legs->last()->getTo();
    }

    public function startTrip(): void
    {
        if (!$this->isInStatus(self::STATUS_BOOKED)) {
            throw new InvalidArgumentException("Unable to start trip not in '".self::STATUS_BOOKED."' status");
        }

        if (!$this->legs->count()) {
            throw new InvalidArgumentException("No selected legs for trip $this->id");
        }

        $this->legs->first()->startLeg();
        $this->status = self::STATUS_PROGRESS;
    }

    public function currentLeg(): ?TripLeg
    {
        $startedLegs = $this->legs->filter(function (TripLeg $leg) {
            return $leg->isInStatus(TripLeg::STATUS_STARTED);
        });

        return count($startedLegs) ? $startedLegs->first() : null;
    }

    public function getFirstLeg(): TripLeg
    {
        return $this->legs->first();
    }

    public function getLegs(): Collection
    {
        return $this->legs;
    }

    public function finishCurrentLeg(string $arrivalGate, DateTimeImmutable $touchdownTime, DateTimeImmutable $arrivalTime, int $fuelOnBoard, int $touchdownFpm): void
    {
        if (!$currentLeg = $this->currentLeg()) {
            throw new InvalidArgumentException("No active leg");
        }

        $currentLeg->finishLeg($arrivalGate, $touchdownTime, $arrivalTime, $fuelOnBoard, $touchdownFpm);

        if ($currentLeg->getId() === $this->legs->last()->getId()) {
            $this->finishTrip();
            $this->aircraft->removeFromTrip();
            $this->clearAssignedAircraft();
        }
    }

    public function startNextLeg(): void
    {
        if ($this->currentLeg() || !$lastArrivedLeg = $this->lastArrivedLeg()) {
            throw new InvalidArgumentException("Finish previous leg first");
        }

        $nextLeg = $this->nextLeg($lastArrivedLeg);

        if (!$nextLeg || !$nextLeg->isInStatus(TripLeg::STATUS_CREATED)) {
            throw new InvalidArgumentException("Next leg wasn't found");
        }

        $nextLeg->startLeg();
    }

    public function getStatus(): string
    {
        return $this->status;
    }

    public static final function createFromFlight(Pilot $captain, Flight $flight, Aircraft $aircraft, DateTimeImmutable $etd): self
    {
        $trip = new self($captain, $aircraft, $etd);
        $trip->addTripLeg(TripLeg::createFromFlight($flight));

        return $trip;
    }

    public function isInStatus(...$statuses): bool
    {
        return in_array($this->status, $statuses);
    }

    public function getAssignedAircraft(): Aircraft
    {
        return $this->aircraft;
    }

    public function captain(): Pilot
    {
        return $this->captain;
    }

    public function firstOfficer(): ?Pilot
    {
        return $this->firstOfficer;
    }

    public function getLastLeg(): ?TripLeg
    {
        $lastLeg = $this->legs->last();

        return $lastLeg ?: null;
    }

    public function addTripLeg(TripLeg $leg): void
    {
        if (!$this->isInStatus(self::STATUS_BOOKED)) {
            TripLegsReadonlyException::throwException();
        }

        if ($this->getLastLeg() && $this->getLastLeg()->getTo()->getIcao() !== $leg->getFrom()->getIcao()) {
            ImpossibleToAddTripLegException::throwFromTripLeg($this->getLastLeg());
        }

        $leg->setTrip($this);
        $this->legs->add($leg);
    }

    public function removeLastTripLeg(): void
    {
        if (!$this->isInStatus(self::STATUS_BOOKED)) {
            TripLegsReadonlyException::throwException();
        }

        if ($this->getLegs()->count() <= 1) {
            ImpossibleToDeleteTripLegException::throw();
        }

        $this->legs->removeElement($this->legs->last());
    }

    public function nextLeg(?TripLeg $leg = null): ?TripLeg
    {
        $leg = $leg ?: $this->currentLeg();

        if (!$leg) {
            return null;
        }

        return !empty($this->legs[$this->legs->indexOf($leg) + 1]) ? $this->legs[$this->legs->indexOf($leg) + 1] : null;
    }

    private function finishTrip(): void
    {
        if ($this->currentLeg()) {
            throw new InvalidArgumentException("Finish current leg first");
        }

        $this->status = self::STATUS_FINISHED;
        $this->finished = new DateTimeImmutable("now");
    }

    private function clearAssignedAircraft(): void
    {
        $this->aircraft = null;
    }

    private function lastArrivedLeg(): ?TripLeg
    {
        $arrivedLegs = $this->legs->filter(function (TripLeg $leg) {
            return $leg->isInStatus(TripLeg::STATUS_ARRIVED);
        });

        return count($arrivedLegs) ? $arrivedLegs->last() : null;
    }
}
