<?php

declare(strict_types=1);

namespace App\Modules\TripBundle\Entity;

use App\Modules\AirportBundle\Entity\Airport;
use App\Modules\TimetableBundle\Entity\Flight;
use DateInterval;
use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
class TripLeg
{
    private const EARTH_RADIUS = 6371;


    const STATUS_CREATED = 'created';

    const STATUS_STARTED = 'started';

    const STATUS_ARRIVED = 'arrived';

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private int $id;

    #[ORM\Column(type: 'string')]
    private string $status = self::STATUS_CREATED;

    #[ORM\Column(type: 'datetime_immutable')]
    private DateTimeImmutable $created;

    #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    private ?DateTimeImmutable $departureDateTime = null;

    #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    private ?DateTimeImmutable $arrived = null;

    #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    private ?DateTimeImmutable $finished = null;

    #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    private ?DateTimeImmutable $touchdownTime = null;

    #[ORM\Column(type: 'integer', nullable: true)]
    private ?int $touchdownFpm = null;

    #[ORM\ManyToOne(targetEntity: Trip::class)]
    private Trip $trip;

    #[ORM\OneToOne(targetEntity: FlightTrack::class, cascade: ['persist', 'remove'])]
    private readonly FlightTrack $track;

    public function __construct(
        #[ORM\Column(type: 'string')]
        private readonly string $flightNumber,
        #[ORM\ManyToOne(targetEntity: Airport::class)]
        #[ORM\JoinColumn(name: 'from_id', referencedColumnName: 'icao')]
        private readonly Airport $from,
        #[ORM\ManyToOne(targetEntity: Airport::class)]
        #[ORM\JoinColumn(name: 'to_id', referencedColumnName: 'icao')]
        private readonly Airport $to,
        #[ORM\Column(type: 'simple_array', nullable: true)]
        private array $selectedAlternates = []
    )
    {
        $this->track = new FlightTrack();
        $this->created = new DateTimeImmutable('now');
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getFrom(): Airport
    {
        return $this->from;
    }

    public function getTo(): Airport
    {
        return $this->to;
    }

    public function startLeg(): void
    {
        $this->status = self::STATUS_STARTED;
        $this->departureDateTime = new DateTimeImmutable('now');
    }

    public function finishLeg(
        string $arrivalGate,
        DateTimeImmutable $touchdownTime,
        DateTimeImmutable $arrivalTime,
        int $fuelOnBoard,
        int $touchdownFpm
    ): void
    {
        $this->status = self::STATUS_ARRIVED;
        $this->finished = new DateTimeImmutable("now");
        $this->arrived = $arrivalTime;
        $this->touchdownTime = $touchdownTime;
        $this->touchdownFpm = $touchdownFpm;
        $this->trip->getAssignedAircraft()->moveToLocation($this->to, $arrivalGate);
        $this->trip->getAssignedAircraft()->refuelTo($fuelOnBoard);
        $this->trip->captain()->moveToLocation($this->to);
        $this->trip->firstOfficer()?->moveToLocation($this->to);
    }

    public function setTrip(Trip $trip): void
    {
        $this->trip = $trip;
    }

    public function getFlightTime(): DateInterval
    {
        return match ($this->status) {
            self::STATUS_STARTED => $this->departureDateTime->diff(new DateTimeImmutable('now')),
            self::STATUS_ARRIVED => $this->departureDateTime->diff($this->arrived),
            default => new DateInterval('PT0M'),
        };
    }

    public function isInStatus(string ...$statuses): bool
    {
        return in_array($this->status, $statuses);
    }

    public static function createFromFlight(Flight $flight, array $alternates = []): self
    {
        return new self(
            $flight->getFlightNumber(),
            $flight->getFrom(),
            $flight->getTo(),
            $alternates
        );
    }

    public function getFlightNumber(): string
    {
        return $this->flightNumber;
    }

    public function departureDateTime(): ?DateTimeImmutable
    {
        return $this->departureDateTime;
    }

    public function arrived(): ?DateTimeImmutable
    {
        return $this->arrived;
    }

    public function getStatus(): string
    {
        return $this->status;
    }

    public function getSelectedAlternates(): array
    {
        return $this->selectedAlternates;
    }

    public function setSelectedAlternates(array $selectedAlternates): void
    {
        $this->selectedAlternates = $selectedAlternates;
    }

    public function getDistance(): float
    {
        return (round(self::calculateDistanceBetweenAirports($this->from, $this->to)));
    }

    public function getAverageDutyTimeMinutes(): float
    {
        return round($this->getDistance() / $this->trip->getAssignedAircraft()->getType()->getAverageSpeed(), 1) * 60 + 30;
    }

    private static function calculateDistanceBetweenAirports(Airport $from, Airport $to): float
    {
        $lat1 = floatval($from->getLatitude());
        $lat2 = floatval($to->getLatitude());
        $lon1 = floatval($from->getLongitude());
        $lon2 = floatval($to->getLongitude());

        $dLat = deg2rad($lat2 - $lat1);
        $dLon = deg2rad($lon2 - $lon1);
        $a = sin($dLat / 2) * sin($dLat / 2) +
             cos(self::degreesToRads($lat1)) * cos(self::degreesToRads($lat2)) *
             sin($dLon / 2) * sin($dLon / 2);
        $c = 2 * atan2(sqrt($a), sqrt(1 - $a));

        return self::EARTH_RADIUS * $c;
    }

    private static function degreesToRads(float $deg): float
    {
        return $deg * (M_PI / 180);
    }
}
