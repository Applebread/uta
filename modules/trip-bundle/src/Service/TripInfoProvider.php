<?php

declare(strict_types=1);

namespace App\Modules\TripBundle\Service;

use App\Modules\PilotBundle\Interfaces\CurrentPilotInterface;
use App\Modules\TripBundle\Entity\Trip;
use App\Modules\TripBundle\Interfaces\TripInfoProviderInterface;
use App\Modules\TripBundle\Repository\TripRepository;

class TripInfoProvider implements TripInfoProviderInterface
{
    public function __construct(private readonly TripRepository $tripRepository, private readonly CurrentPilotInterface $currentPilot)
    {
    }

    public function provideActiveTripInfo(): ?Trip
    {
        return $this->tripRepository->findActiveByCaptainOrFirstOfficer($this->currentPilot->getCurrentPilot());
    }
}
