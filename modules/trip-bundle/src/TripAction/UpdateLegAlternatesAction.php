<?php

declare(strict_types=1);

namespace App\Modules\TripBundle\TripAction;

use App\Modules\TripBundle\Entity\Trip;
use App\Modules\TripBundle\Exceptions\InvalidTripStatusException;
use App\Modules\TripBundle\Interfaces\TripActionInterface;
use App\Modules\TripBundle\Repository\TripRepository;
use App\Modules\TripBundle\Value\TripRequest;
use App\Modules\TripBundle\Value\UpdateLegAlternatesRequest;

class UpdateLegAlternatesAction implements TripActionInterface
{
    public function __construct(private readonly TripRepository $tripRepository)
    {
    }

    /** @var UpdateLegAlternatesRequest $tripRequest */
    public function __invoke(TripRequest $tripRequest): Trip
    {
        if (!$tripRequest->trip()->isInStatus(Trip::STATUS_BOOKED)) {
            InvalidTripStatusException::throwFromTrip($tripRequest->trip());
        }

        $tripRequest->leg()->setSelectedAlternates($tripRequest->getAlternates());
        $this->tripRepository->save($tripRequest->trip());

        return $tripRequest->trip();
    }
}
