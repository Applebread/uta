<?php

declare(strict_types=1);

namespace App\Modules\TripBundle\TripAction;

use App\Modules\TripBundle\Entity\Trip;
use App\Modules\TripBundle\Interfaces\TripActionInterface;
use App\Modules\TripBundle\Value\AddLegRequest;
use App\Modules\TripBundle\Value\TripRequest;

class RemoveLastLegAction implements TripActionInterface
{
    /** @param AddLegRequest $tripRequest */
    public function __invoke(TripRequest $tripRequest): Trip
    {
        $tripRequest->trip()->removeLastTripLeg();
        $tripRequest->repository()->save($tripRequest->trip());

        return $tripRequest->trip();
    }
}
