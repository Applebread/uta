<?php

declare(strict_types=1);

namespace App\Modules\TripBundle\TripAction;

use App\Modules\TripBundle\Entity\Trip;
use App\Modules\TripBundle\Exceptions\InvalidEstimatedTimeDepartureException;
use App\Modules\TripBundle\Exceptions\InvalidPilotLocationException;
use App\Modules\TripBundle\Exceptions\InvalidTypeRatingException;
use App\Modules\TripBundle\Exceptions\NoAvailableAircraftException;
use App\Modules\TripBundle\Interfaces\AircraftSelectorInterface;
use App\Modules\TripBundle\Interfaces\TripActionInterface;
use App\Modules\TripBundle\Value\BookTripRequest;
use App\Modules\TripBundle\Value\TripRequest;
use DateTimeImmutable;

class BookTripAction implements TripActionInterface
{
    public function __construct(private readonly AircraftSelectorInterface $aircraftSelector)
    {
    }

    /** @param BookTripRequest $tripRequest */
    public function __invoke(TripRequest $tripRequest): Trip
    {
        $pilot = $tripRequest->pilot();
        $flight = $tripRequest->flight();

        if ($pilot->getLocation()->getIcao() !== $flight->getFrom()->getIcao()) {
            InvalidPilotLocationException::throwFromPilotAndLocation($pilot, $flight->getFrom());
        }

        if (!$pilot->hasRatingIcaoString($tripRequest->aircraftTypeIcao())) {
            InvalidTypeRatingException::throwFromTypeRatingIcao($tripRequest->aircraftTypeIcao());
        }

        if ($tripRequest->etd() < new DateTimeImmutable('now')) {
            InvalidEstimatedTimeDepartureException::throwFromEtd($tripRequest->etd());
        }

        if (!$availableAircraft = $this->aircraftSelector->getAvailableAircraftByTypeAndLocation($tripRequest->aircraftTypeIcao(), $flight->getFrom())) {
            NoAvailableAircraftException::throwFromType($tripRequest->aircraftTypeIcao());
        }

        $trip = Trip::createFromFlight($pilot, $flight, $availableAircraft, $tripRequest->etd());
        $tripRequest->repository()->save($trip);

        return $trip;
    }
}
