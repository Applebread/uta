<?php

declare(strict_types=1);

namespace App\Modules\TripBundle\TripAction;

use App\Modules\TripBundle\Entity\Trip;
use App\Modules\TripBundle\Exceptions\InvalidPilotLocationException;
use App\Modules\TripBundle\Exceptions\InvalidTripStatusException;
use App\Modules\TripBundle\Exceptions\InvalidTypeRatingException;
use App\Modules\TripBundle\Interfaces\TripActionInterface;
use App\Modules\TripBundle\Value\JoinTripRequest;
use App\Modules\TripBundle\Value\TripRequest;

class JoinTripAction implements TripActionInterface
{

    /** @param JoinTripRequest $tripRequest */
    public function __invoke(TripRequest $tripRequest): Trip
    {
        if (!$tripRequest->trip()->isInStatus(Trip::STATUS_BOOKED)) {
            InvalidTripStatusException::throwFromTrip($tripRequest->trip());
        }

        if ($tripRequest->pilot()->getLocation()->getIcao() !== $tripRequest->trip()->from()->getIcao()) {
            InvalidPilotLocationException::throwFromPilotAndLocation($tripRequest->pilot(), $tripRequest->trip()->from());
        }

        if (!$tripRequest->pilot()->hasRating($tripRequest->trip()->getAssignedAircraft()->getType()->getIcao())) {
            InvalidTypeRatingException::throwFromTrip($tripRequest->trip());
        }

        $tripRequest->trip()->addFirstOfficer($tripRequest->pilot());
        $tripRequest->repository()->save($tripRequest->trip());

        return $tripRequest->trip();
    }
}
