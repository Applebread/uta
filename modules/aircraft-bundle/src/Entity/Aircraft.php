<?php

declare(strict_types=1);

namespace App\Modules\AircraftBundle\Entity;

use App\Modules\AirportBundle\Entity\Airport;
use App\Modules\TripBundle\Entity\Trip;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
class Aircraft
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private int $id;

    #[ORM\ManyToOne(targetEntity: AircraftType::class, cascade: ['persist'])]
    private AircraftType $type;

    #[ORM\Column(type: 'string', length: 9, unique: true)]
    private string $registration;

    #[ORM\ManyToOne(targetEntity: Airport::class, cascade: ['persist'])]
    #[ORM\JoinColumn(name:'location_id', referencedColumnName: 'icao')]
    private Airport $location;

    #[ORM\OneToOne(inversedBy: 'aircraft', targetEntity: Trip::class)]
    #[ORM\JoinColumn(name: 'trip_id', referencedColumnName: 'id', nullable: true)]
    private ?Trip $trip;

    #[ORM\Column(type: 'string')]
    private string $gate;

    #[ORM\Column(type: 'integer')]
    private int $fob = 0;

    #[ORM\OneToOne(targetEntity: AircraftStats::class, cascade: ['persist', 'remove'])]
    private AircraftStats $aircraftStats;

    public function __construct(
        AircraftType $type,
        string $registration,
        Airport $location,
        string $gate,
        ?int $fob = 0,
        ?AircraftStats $aircraftStats = null
    )
    {
        $this->type = $type;
        $this->registration = $registration;
        $this->location = $location;
        $this->gate = $gate;
        $this->fob = $fob;
        $this->aircraftStats = $aircraftStats ?? new AircraftStats();
        $this->trip = null;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function moveToLocation(Airport $airport, string $gate, ?int $newFob = null): void
    {
        $this->location = $airport;
        $this->gate = $gate;

        if (!$newFob) {
            return;
        }

        $this->fob = max(0, $newFob);
    }

    public function getType(): AircraftType
    {
        return $this->type;
    }

    public function getRegistration(): string
    {
        return $this->registration;
    }

    public function getLocation(): Airport
    {
        return $this->location;
    }

    public function getGate(): string
    {
        return $this->gate;
    }

    public function getFob(): int
    {
        return $this->fob;
    }

    public function getAircraftStats(): AircraftStats
    {
        return $this->aircraftStats;
    }

    public function setType(AircraftType $type): void
    {
        $this->type = $type;
    }

    public function updateRegistration(string $registration): void
    {
        $this->registration = $registration;
    }

    public function refuelTo(int $fob): void
    {
        $this->fob = $fob;
    }

    public function assignToTrip(Trip $trip): void
    {
        $this->trip = $trip;
    }

    public function removeFromTrip(): void
    {
        $this->trip = null;
    }

    public function isInTrip(): bool
    {
        return $this->trip !== null;
    }
}
