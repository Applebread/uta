<?php

namespace App\Modules\AircraftBundle\Interface;

use App\Modules\AircraftBundle\Dto\AircraftTypeListDto;
use Symfony\Component\HttpFoundation\File\UploadedFile;

interface AircraftTypeCrudServiceInterface
{
    public function addAircraftType(string $icao, string $name, string $description, string $image, bool $heavy): void;

    public function updateAircraftType(int $id, string $icao, string $name, string $description, string $image, bool $heavy): void;

    public function deleteAircraftType(int $id): void;

    public function getAircraftTypesList(int $page, int $pageSize): AircraftTypeListDto;

    public function uploadAircraftTypeImage(UploadedFile $file): string;
}
