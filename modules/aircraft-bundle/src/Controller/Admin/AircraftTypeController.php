<?php

declare(strict_types=1);

namespace App\Modules\AircraftBundle\Controller\Admin;

use App\Dto\UploadStatusDto;
use App\Modules\AircraftBundle\Dto\AircraftTypeDto;
use App\Modules\AircraftBundle\Interface\AircraftTypeCrudServiceInterface;
use App\Modules\AircraftBundle\Repository\AircraftTypeRepository;
use OpenApi\Annotations as OA;
use OpenApi\Attributes\Parameter;
use OpenApi\Attributes\Tag;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Nelmio\ApiDocBundle\Annotation as NA;

class AircraftTypeController extends AbstractController
{
    public function __construct(
        private readonly AircraftTypeCrudServiceInterface $aircraftTypeCrudService,
        private readonly AircraftTypeRepository $aircraftTypeRepository
    )
    {
    }

    #[Route(path: '/api/admin/aircraft-types', methods: ['GET'])]
    #[Tag('Aircraft')]
    #[Parameter(name: 'page', in: 'query')]
    #[Parameter(name: 'page_size', in: 'query')]
    #[IsGranted('ROLE_PILOT')]
    public function list(Request $request): JsonResponse
    {
        return new JsonResponse(
            $this->aircraftTypeCrudService->getAircraftTypesList(
                (int) $request->get('page', 1),
                (int) $request->get('page_size', 15)
            )
        );
    }

    /** @OA\RequestBody(@OA\JsonContent(ref=@NA\Model(type=AircraftTypeDto::class))) */
    #[Route(path: '/api/admin/aircraft/type', methods: ['POST'])]
    #[Tag('Aircraft')]
    #[IsGranted('ROLE_ADMIN')]
    public function createAircraftType(Request $request): JsonResponse
    {
        $dto = AircraftTypeDto::fromRequest($request);

        $this->aircraftTypeCrudService->addAircraftType($dto->icao, $dto->name, $dto->description, $dto->image, $dto->heavy);

        return new JsonResponse($dto);
    }

    /** @OA\RequestBody(@OA\JsonContent(ref=@NA\Model(type=AircraftTypeDto::class))) */
    #[Route(path: '/api/admin/aircraft/type/{id}', methods: ['PUT'])]
    #[Tag('Aircraft')]
    #[IsGranted('ROLE_ADMIN')]
    public function updateAircraftType(int $id, Request $request): JsonResponse
    {
        $dto = AircraftTypeDto::fromRequest($request);

        $this->aircraftTypeCrudService->updateAircraftType($id, $dto->icao, $dto->name, $dto->description, $dto->image, $dto->heavy);

        return new JsonResponse($dto);
    }

    #[Route(path: '/api/admin/aircraft/type/{id}', methods: ['GET'])]
    #[Tag('Aircraft')]
    #[IsGranted('ROLE_ADMIN')]
    public function getAircraftType(int $id): JsonResponse
    {
        return new JsonResponse(AircraftTypeDto::fromEntity($this->aircraftTypeRepository->find($id)));
    }

    #[Route(path: '/api/admin/aircraft/type/image', name: 'upload_aircraft_type_image', methods: ['POST'])]
    #[Tag('Aircraft')]
    #[IsGranted('ROLE_ADMIN')]
    public function uploadTypeImage(Request $request): JsonResponse
    {
        $imagePath = $this->aircraftTypeCrudService->uploadAircraftTypeImage($request->files->get('aircraft_type_image'));

        return new JsonResponse(UploadStatusDto::success($imagePath));
    }

    #[Route(path: '/api/admin/aircraft/type/{id}', methods: ['DELETE'])]
    #[Tag('Aircraft')]
    #[IsGranted('ROLE_ADMIN')]
    public function removeAircraft(int $id): JsonResponse
    {
        $this->aircraftTypeCrudService->deleteAircraftType($id);

        return new JsonResponse();
    }
}
