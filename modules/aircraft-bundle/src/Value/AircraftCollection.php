<?php

namespace App\Modules\AircraftBundle\Value;

use App\Modules\AircraftBundle\Entity\Aircraft;
use App\Modules\AirportBundle\Entity\Airport;

final class AircraftCollection implements \Iterator
{

    private array $aircraftCollection;

    public function __construct(Aircraft ...$aircraftCollection)
    {
        $this->aircraftCollection = $aircraftCollection;
    }

    private function findByCallable(callable $callable): ?Aircraft
    {
        $res = array_filter($this->aircraftCollection, $callable);

        return array_shift($res);
    }

    public function getFirstAvailableInLocation(Airport $location): ?Aircraft
    {
        return $this->findByCallable(function (Aircraft $aircraft) use ($location) {
            return !$aircraft->isInTrip() && $aircraft->getLocation()->getIcao() === $location->getIcao();
        });
    }

    public function current(): Aircraft
    {
        return current($this->aircraftCollection);
    }

    public function next(): void
    {
        next($this->aircraftCollection);
    }

    public function key(): int
    {
        return key($this->aircraftCollection);
    }

    public function valid(): bool
    {
        return false !== current($this->aircraftCollection);
    }

    public function rewind(): void
    {
        reset($this->aircraftCollection);
    }
}
