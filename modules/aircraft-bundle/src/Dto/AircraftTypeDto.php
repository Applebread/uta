<?php

declare(strict_types=1);

namespace App\Modules\AircraftBundle\Dto;

use App\Modules\AircraftBundle\Entity\AircraftType;
use Symfony\Component\HttpFoundation\Request;

final class AircraftTypeDto
{
    private function __construct(
        public ?int $id,
        public string $icao,
        public string $name,
        public string $description,
        public string $image,
        public bool $heavy,
    )
    {
    }

    public static final function fromRequest(Request $request): self
    {
        $requestBody = json_decode($request->getContent(), true);

        return new self(
            null,
            $requestBody['icao'],
            $requestBody['name'],
            $requestBody['description'],
            $requestBody['image'],
            $requestBody['heavy'],
        );
    }

    public static final function fromEntity(AircraftType $aircraft): self
    {
        $me = new self(
            $aircraft->getId(),
            $aircraft->getIcao(),
            $aircraft->getName(),
            $aircraft->getDescription(),
            $aircraft->getImage(),
            $aircraft->isHeavy()
        );

        $me->id = $aircraft->getId();

        return $me;
    }
}
