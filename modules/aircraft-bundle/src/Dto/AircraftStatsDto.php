<?php

declare(strict_types=1);

namespace App\Modules\AircraftBundle\Dto;

use App\Modules\AircraftBundle\Entity\AircraftStats;

final class AircraftStatsDto
{
    private function __construct(
        public readonly string $lastFlightAt,
        public readonly string $acTotalFH,
        public readonly int $acTotalFC,
        public readonly string $latestFiftyCheckAt,
        public readonly string $latestTwoHundredCheckAt,
        public readonly string $latestThousandCheckAt,
    )
    {
    }

    public static final function fromEntity(AircraftStats $aircraftStats): self
    {
        return new self(
            $aircraftStats->getLastFlightAt() ? $aircraftStats->getLastFlightAt()->format('Y-m-d H:i:s') : 'Never',
            $aircraftStats->getFlightHours(),
            $aircraftStats->getAcTotalFC(),
            $aircraftStats->timeToCheck(AircraftStats::FIFTY_CHECK)->format('%H:%I'),
            $aircraftStats->timeToCheck(AircraftStats::TWO_HUNDRED_CHECK)->format('%H:%I'),
            $aircraftStats->timeToCheck(AircraftStats::ONE_THOUSAND_CHECK)->format('%H:%I'),
        );
    }
}
