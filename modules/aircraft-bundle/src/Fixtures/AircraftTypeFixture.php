<?php

declare(strict_types=1);

namespace App\Modules\AircraftBundle\Fixtures;

use App\Modules\AircraftBundle\Entity\AircraftType;
use App\Modules\AircraftBundle\Repository\AircraftTypeRepository;
use App\Modules\PilotBundle\Entity\TypeRating;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Persistence\ObjectManager;

final class AircraftTypeFixture extends Fixture
{

    private AircraftTypeRepository $aircraftTypeRepository;

    public function __construct(AircraftTypeRepository $aircraftTypeRepository)
    {
        $this->aircraftTypeRepository = $aircraftTypeRepository;
    }

    public function load(ObjectManager $manager)
    {
        $manager->persist(new AircraftType('B738', 'Boeing 737-800', 'Boeing 737-800', '', false, null, 850));
        $manager->persist(new AircraftType('B737', 'Boeing 737-700', 'Boeing 737-700', '', false, null, 850));
        $manager->persist(new AircraftType('B735', 'Boeing 737-500', 'Boeing 737-500', '', false, null, 800));
        $manager->persist(new AircraftType('B734', 'Boeing 737-400', 'Boeing 737-400', '', false, null, 800));
        $manager->persist(new AircraftType('B762', 'Boeing 767-200', 'Boeing 767-200', '', false, null, 850));
        $manager->persist(new AircraftType('B763', 'Boeing 767-300', 'Boeing 767-300', '', false, null, 850));
        $manager->persist(new AircraftType('AT75', 'ATR 72-500', 'ATR 72-500', '', false, null, 450));
        $manager->persist(new AircraftType('T154', 'Tupolev TU-154', 'Tupolev TU-154', '', false, null, 900));
        $manager->persist(new AircraftType('T134', 'Tupolev TU-134', 'Tupolev TU-134', '', false, null, 750));
        $manager->persist(new AircraftType('AN24', 'Antonov AN-24', 'Antonov AN-24', '', false, null, 450));
        $manager->persist($testType = new AircraftType('TTTT', 'TestType', 'TestType', '', false, null, 100));

        $manager->flush();

        $allTypes = $this->aircraftTypeRepository->findAll();

        $boeing373Classic = $this->filterTypeByRegex($allTypes, "/^B73[3-5]/m");
        $boeing373NG = $this->filterTypeByRegex($allTypes, "/^B73[6-9]/m");
        $boeing767 = $this->filterTypeByRegex($allTypes, "/^B76[2-3]/m");
        $atr = $this->filterTypeByRegex($allTypes, "/^AT[4,7]{1}[2,5,6]{1}/m");
        $tupolev154 = $this->filterTypeByRegex($allTypes, "/^T154/m");
        $tupolev134 = $this->filterTypeByRegex($allTypes, "/^T134/m");
        $antonov2426 = $this->filterTypeByRegex($allTypes, "/^AN2[4,6]{1}/m");

        $manager->persist($rating = new TypeRating('Boeing 737CL', new ArrayCollection($boeing373Classic)));
        foreach ($boeing373Classic as $acType) {
            $acType->setTypeRating($rating);
            $manager->persist($acType);
        }

        $manager->persist($rating = new TypeRating('Boeing 737NG', new ArrayCollection($boeing373NG)));
        foreach ($boeing373NG as $acType) {
            $acType->setTypeRating($rating);
            $manager->persist($acType);
        }

        $manager->persist($rating = new TypeRating('Boeing 767', new ArrayCollection($boeing767)));
        foreach ($boeing767 as $acType) {
            $acType->setTypeRating($rating);
            $manager->persist($acType);
        }

        $manager->persist($rating = new TypeRating('ATR-42/72', new ArrayCollection($atr)));
        foreach ($atr as $acType) {
            $acType->setTypeRating($rating);
            $manager->persist($acType);
        }

        $manager->persist($rating = new TypeRating('Ту-154', new ArrayCollection($tupolev154)));
        foreach ($tupolev154 as $acType) {
            $acType->setTypeRating($rating);
            $manager->persist($acType);
        }

        $manager->persist($rating = new TypeRating('Ту-134', new ArrayCollection($tupolev134)));
        foreach ($tupolev134 as $acType) {
            $acType->setTypeRating($rating);
            $manager->persist($acType);
        }

        $manager->persist($rating = new TypeRating('Ан-24/26', new ArrayCollection($antonov2426)));
        foreach ($antonov2426 as $acType) {
            $acType->setTypeRating($rating);
            $manager->persist($acType);
        }

        $manager->flush();
    }

    /**
     * @return AircraftType[]
     */
    private function filterTypeByRegex(array $allTypes, string $regexp): array
    {
        return array_filter($allTypes, function (AircraftType $type) use ($regexp) {
            return preg_match($regexp, $type->getIcao());
        });
    }
}
