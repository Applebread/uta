<?php

declare(strict_types=1);

namespace App\Modules\AircraftBundle\Repository;

use App\Modules\AircraftBundle\Entity\AircraftType;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use OutOfBoundsException;

final class AircraftTypeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AircraftType::class);
    }

    public function findByIcao(string $icao):? AircraftType
    {
        return $this->findOneBy(['icao' => $icao]);
    }

    /**
     * @param string $regexp
     * @return AircraftType[]
     */
    public function findAllByIcaoRegex(string $regexp): array
    {
        $qb = $this->createQueryBuilder('at');

        return $qb->andWhere('REGEXP(at.icao, :regexp) = true')
            ->setParameter('regexp', $regexp)
            ->getQuery()->getResult();
    }

    public function getFirst(): AircraftType
    {
        return $this->findOneBy([], ['id' => 'DESC',]);
    }

    public function getById(int $id): AircraftType
    {
        $aircraftType = $this->find($id);

        return $aircraftType === null ? throw new OutOfBoundsException("Aircraft not found") : $aircraftType;
    }

    public function deleteById(int $id): void
    {
        $this->_em->remove($this->find($id));
        $this->_em->flush();
    }

    public function save(AircraftType $aircraft): void
    {
        $this->_em->persist($aircraft);
        $this->_em->flush();
    }
}
