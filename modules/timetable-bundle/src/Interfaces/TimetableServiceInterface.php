<?php

namespace App\Modules\TimetableBundle\Interfaces;

use App\Modules\TimetableBundle\Dto\TimetableCollectionDto;
use DateTimeImmutable;

interface TimetableServiceInterface
{
    public function getActualSchedule(
        DateTimeImmutable $actualDate,
        ?string $from,
        ?string $to,
        ?string $aircraftType,
        bool $isAvailableForMe,
        int $page,
        int $pageSize
    ): TimetableCollectionDto;

    /**
     * @return string[]
     */
    public function getScheduledAirportsList(): array;

    public function getScheduledAircraftTypes(): array;
}
