<?php

declare(strict_types=1);

namespace App\Modules\TimetableBundle\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20221028150500 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE SEQUENCE flight_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE flight (id INT NOT NULL, from_id INT DEFAULT NULL, to_id INT DEFAULT NULL, days_of_week TEXT NOT NULL, scheduled_from TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, scheduled_until TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, payload_percent INT NOT NULL, status VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_C257E60E78CED90B ON flight (from_id)');
        $this->addSql('CREATE INDEX IDX_C257E60E30354A65 ON flight (to_id)');
        $this->addSql('COMMENT ON COLUMN flight.days_of_week IS \'(DC2Type:simple_array)\'');
        $this->addSql('COMMENT ON COLUMN flight.scheduled_from IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN flight.scheduled_until IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('CREATE TABLE flight_alternates (flight_id INT NOT NULL, airport_id INT NOT NULL, PRIMARY KEY(flight_id, airport_id))');
        $this->addSql('CREATE INDEX IDX_D211C97591F478C5 ON flight_alternates (flight_id)');
        $this->addSql('CREATE INDEX IDX_D211C975289F53C8 ON flight_alternates (airport_id)');
        $this->addSql('ALTER TABLE flight ADD CONSTRAINT FK_C257E60E78CED90B FOREIGN KEY (from_id) REFERENCES airport (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE flight ADD CONSTRAINT FK_C257E60E30354A65 FOREIGN KEY (to_id) REFERENCES airport (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE flight_alternates ADD CONSTRAINT FK_D211C97591F478C5 FOREIGN KEY (flight_id) REFERENCES flight (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE flight_alternates ADD CONSTRAINT FK_D211C975289F53C8 FOREIGN KEY (airport_id) REFERENCES airport (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP SEQUENCE flight_id_seq CASCADE');
        $this->addSql('ALTER TABLE flight DROP CONSTRAINT FK_C257E60E78CED90B');
        $this->addSql('ALTER TABLE flight DROP CONSTRAINT FK_C257E60E30354A65');
        $this->addSql('ALTER TABLE flight_alternates DROP CONSTRAINT FK_D211C97591F478C5');
        $this->addSql('ALTER TABLE flight_alternates DROP CONSTRAINT FK_D211C975289F53C8');
        $this->addSql('DROP TABLE flight');
        $this->addSql('DROP TABLE flight_alternates');
    }
}
