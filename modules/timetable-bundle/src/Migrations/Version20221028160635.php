<?php

declare(strict_types=1);

namespace App\Modules\TimetableBundle\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20221028160635 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Make flight number unique';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE UNIQUE INDEX UNIQ_C257E60EA9F64E43 ON flight (flight_number)');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('DROP INDEX UNIQ_C257E60EA9F64E43');
    }
}
