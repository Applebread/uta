<?php

declare(strict_types=1);

namespace App\Modules\TimetableBundle\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20221029115406 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Removed unique from flight number';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('DROP INDEX uniq_c257e60ea9f64e43');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('CREATE UNIQUE INDEX uniq_c257e60ea9f64e43 ON flight (flight_number)');
    }
}
