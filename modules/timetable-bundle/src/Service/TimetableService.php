<?php

namespace App\Modules\TimetableBundle\Service;

use App\Modules\PilotBundle\Interfaces\CurrentPilotInterface;
use App\Modules\TimetableBundle\Interfaces\TimetableServiceInterface;
use App\Modules\TimetableBundle\Repository\FlightRepository;
use App\Modules\TimetableBundle\Dto\TimetableCollectionDto;
use DateTimeImmutable;

class TimetableService implements TimetableServiceInterface
{
    public function __construct(private readonly FlightRepository $flightRepository, private readonly CurrentPilotInterface $pilot)
    {
    }

    public function getActualSchedule(
        DateTimeImmutable $actualDate,
        ?string $from = null,
        ?string $to = null,
        ?string $aircraftType = null,
        bool $isAvailableForMe,
        int $page = 1,
        int $pageSize = 15
    ): TimetableCollectionDto
    {
        $aircraftTypes = [];

        if ($aircraftType) {
            $aircraftTypes[] = $aircraftType;
        }

        $currentPilot = $this->pilot->getCurrentPilot();
        if ($isAvailableForMe) {
            if (!$from) {
                $from = $currentPilot->getLocation()->getIcao();
            }

            if ($aircraftType && $currentPilot->getTypeRatings()->isEmpty()) {
                return TimetableCollectionDto::empty();
            }
        }

        return $this->flightRepository->findByParameters($page, $pageSize, $actualDate, $from, $to);
    }

    public function getScheduledAirportsList(): array
    {
        return $this->flightRepository->getScheduledAirports();
    }

    public function getScheduledAircraftTypes(): array
    {
        return $this->flightRepository->getScheduledAircraftTypes();
    }
}
