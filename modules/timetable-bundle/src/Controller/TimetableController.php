<?php

namespace App\Modules\TimetableBundle\Controller;

use App\Modules\TimetableBundle\Dto\TimetableItemDto;
use App\Modules\TimetableBundle\Entity\FlightScheduleDetails;
use App\Modules\TimetableBundle\Interfaces\TimetableServiceInterface;
use App\Modules\TimetableBundle\Repository\FlightRepository;
use DateTimeImmutable;
use OpenApi\Attributes\Parameter;
use OpenApi\Attributes\Tag;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use App\Modules\TimetableBundle\Dto\TimetableRequestDto;
use Nelmio\ApiDocBundle\Annotation as NA;
use OpenApi\Annotations as OA;

class TimetableController extends AbstractController
{
    /**
     * @OA\RequestBody(@OA\JsonContent(ref=@NA\Model(type=TimetableRequestDto::class)))
     */
    #[Route(path: '/api/flight/timetable', name: 'timetable', methods: ['POST'])]
    #[Parameter(name: 'page', in: 'query')]
    #[Parameter(name: 'pageSize', in: 'query')]
    #[Tag('Flight')]
    #[IsGranted('ROLE_PILOT')]
    public function getTimetable(Request $request, TimetableServiceInterface $timetableService): JsonResponse
    {
        $timetableDto = TimetableRequestDto::fromRequest($request);

        return new JsonResponse($timetableService->getActualSchedule(
            $timetableDto->getDateOfFlight() ?? new DateTimeImmutable('today'),
            $timetableDto->getFrom(),
            $timetableDto->getTo(),
            $timetableDto->getAircraftType(),
            (int)$request->get('page', 1),
            (int)$request->get('pageSize', 15)
        ));
    }

    #[Route(path: '/api/flight/{id}', name: 'flight_details', methods: ['GET'])]
    #[Tag('Flight')]
    #[IsGranted('ROLE_PILOT')]
    public function getFlightDetails(int $id, FlightRepository $repository): JsonResponse
    {
        return new JsonResponse(TimetableItemDto::fromFlightEntity($repository->find($id)));
    }

    #[Route(path: '/api/flight/timetable/airports', name: 'timetable_airports', methods: ['GET'])]
    #[Tag('Flight')]
    #[IsGranted('ROLE_PILOT')]
    public function getScheduledAirportsList(TimetableServiceInterface $timetableService): JsonResponse
    {
        return new JsonResponse($timetableService->getScheduledAirportsList());
    }

    #[Route(path: '/api/flight/timetable/aircraft-types', name: 'timetable_ac_types', methods: ['GET'])]
    #[Tag('Flight')]
    #[IsGranted('ROLE_PILOT')]
    public function getScheduledAircraftTypes(TimetableServiceInterface $timetableService): JsonResponse
    {
        return new JsonResponse($timetableService->getScheduledAircraftTypes());
    }
}
