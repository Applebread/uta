<?php

declare(strict_types=1);

namespace App\Modules\TimetableBundle\Controller\Admin;

use App\Modules\TimetableBundle\Interfaces\TimetableCrudServiceInterface;
use App\Modules\TimetableBundle\Repository\FlightRepository;
use InvalidArgumentException;
use OpenApi\Annotations as OA;
use OpenApi\Attributes\Tag;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use App\Modules\TimetableBundle\Dto\TimetableItemDto;
use Nelmio\ApiDocBundle\Annotation as NA;

class TimetableController extends AbstractController
{

    /** @OA\RequestBody(@OA\JsonContent(ref=@NA\Model(type=TimetableItemDto::class))) */
    #[Route(path: '/api/admin/flight', methods: ['POST'])]
    #[Tag('Flight')]
    #[IsGranted('ROLE_ADMIN')]
    public function createFlight(Request $request, TimetableCrudServiceInterface $crudService): JsonResponse
    {
        $request = TimetableItemDto::fromRequest($request);

        try {
            return new JsonResponse(TimetableItemDto::fromFlightEntity($crudService->createFlightFromDto($request)));
        } catch (InvalidArgumentException $exception) {
            throw new BadRequestHttpException($exception->getMessage(), $exception);
        }
    }

    /** @OA\RequestBody(@OA\JsonContent(ref=@NA\Model(type=TimetableItemDto::class))) */
    #[Route(path: '/api/admin/flight/{id}', methods: ['PUT'])]
    #[Tag('Flight')]
    #[IsGranted('ROLE_ADMIN')]
    public function updateFlight(int $id, Request $request, TimetableCrudServiceInterface $crudService): JsonResponse
    {
        $request = TimetableItemDto::fromRequest($request);

        try {
            return new JsonResponse(TimetableItemDto::fromFlightEntity($crudService->updateFlightFromDto($id, $request)));
        } catch (InvalidArgumentException $exception) {
            throw new BadRequestHttpException($exception->getMessage(), $exception);
        }
    }

    #[Route(path: '/api/admin/flight/{id}', methods: ['GET'])]
    #[Tag('Flight')]
    #[IsGranted('ROLE_ADMIN')]
    public function getFlight(int $id, FlightRepository $flightRepository): JsonResponse
    {
        if (!$flight = $flightRepository->find($id)) {
            throw new NotFoundHttpException("Flight $id was not found");
        }

        return new JsonResponse(TimetableItemDto::fromFlightEntity($flight));
    }

    #[Route(path: '/api/admin/flight/{id}', methods: ['DELETE'])]
    #[Tag('Flight')]
    #[IsGranted('ROLE_ADMIN')]
    public function removeFlight(int $id, FlightRepository $flightRepository): JsonResponse
    {
        if (!$flight = $flightRepository->find($id)) {
            throw new NotFoundHttpException("Flight $id was not found");
        }

        $flightRepository->deleteRecordById($flight->getId());

        return new JsonResponse(null, Response::HTTP_NO_CONTENT);
    }
}
