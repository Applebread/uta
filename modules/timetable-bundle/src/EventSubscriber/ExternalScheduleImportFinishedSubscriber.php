<?php

declare(strict_types=1);

namespace App\Modules\TimetableBundle\EventSubscriber;

use App\Modules\AircraftBundle\Repository\AircraftTypeRepository;
use App\Modules\AirportBundle\Interfaces\AirportDetailsServiceInterface;
use App\Modules\AviationEdgeParserBundle\Event\AviationEdgeRouteImportFinishedEvent;
use App\Modules\FlightawareParserBundle\Event\FlightawareScheduleImportFinishedEvent;
use App\Modules\InfogateParserBundle\Event\InfogateScheduleImportFinishedEvent;
use App\Modules\TimetableBundle\Repository\FlightRepository;
use App\Modules\UtairParserBundle\Event\UtairScheduleImportFinishedEvent;
use App\Modules\UtairParserBundle\Value\UtairFlightsCollection;
use DateTimeImmutable;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class ExternalScheduleImportFinishedSubscriber implements EventSubscriberInterface
{

    public function __construct(
        private readonly FlightRepository $flightRepository,
        private readonly AirportDetailsServiceInterface $airportDetailsService,
        private readonly AircraftTypeRepository $aircraftTypeRepository
    )
    {
    }

    public static function getSubscribedEvents(): array
    {
        return [
            InfogateScheduleImportFinishedEvent::class => 'infogateScheduleImportFinished',
            AviationEdgeRouteImportFinishedEvent::class => 'aviationEdgeRoutesImportFinished',
            FlightawareScheduleImportFinishedEvent::class => 'flightawareEdgeRoutesImportFinished',
            UtairScheduleImportFinishedEvent::class => 'utairScheduleImportFinished'
        ];
    }

    public function infogateScheduleImportFinished(InfogateScheduleImportFinishedEvent $event): void
    {
        $infogateSchedule = $event->getScheduleCollection();

        foreach ($infogateSchedule->getIterator() as $infogateScheduleItem) {
            $fromAirport = $this->airportDetailsService->getAirportByIcao($infogateScheduleItem->from());
            $toAirport = $this->airportDetailsService->getAirportByIcao($infogateScheduleItem->to());
            $aircraftType = $this->aircraftTypeRepository->findByIcao($infogateScheduleItem->aircraftType());

            if (!$fromAirport || !$toAirport || !$aircraftType) {
                continue;
            }

            $this->flightRepository->createOrUpdateByFlightNumber(
                $infogateScheduleItem->flightNumber(),
                $infogateScheduleItem->from(),
                $infogateScheduleItem->to(),
                $infogateScheduleItem->daysInWeak(),
                (new DateTimeImmutable())->setTime(mt_rand(0, 23), mt_rand(0, 59)), // No info from provider, so just random departure time.
                $infogateScheduleItem->scheduledFrom(),
                $infogateScheduleItem->scheduledUntil(),
                $infogateScheduleItem->aircraftType()
            );
        }
    }

    public function aviationEdgeRoutesImportFinished(AviationEdgeRouteImportFinishedEvent $event): void
    {
        foreach ($event->getRoutesList() as $aviationEdgeRoute) {
            $fromAirport = $this->airportDetailsService->getAirportByIcao($aviationEdgeRoute->departureIcao());
            $toAirport = $this->airportDetailsService->getAirportByIcao($aviationEdgeRoute->arrivalIcao());
            $acType = $this->aircraftTypeRepository->getFirst();

            if (!$fromAirport || !$toAirport) {
                continue;
            }

            $this->flightRepository->createOrUpdateByFlightNumber(
                $aviationEdgeRoute->airlineIcao().$aviationEdgeRoute->flightNumber(),
                $aviationEdgeRoute->departureIcao(),
                $aviationEdgeRoute->arrivalIcao(),
                range(1, 7),
                (new DateTimeImmutable())->setTime(mt_rand(0, 23), mt_rand(0, 59)), // No info from provider, so just random departure time.
                new DateTimeImmutable(date("Y").'-01-01 00:00:00'),
                new DateTimeImmutable(date("Y").'-12-31 23:59:59'),
                $acType->getIcao()
            );
        }
    }

    public function flightawareEdgeRoutesImportFinished(FlightawareScheduleImportFinishedEvent $event): void
    {
        foreach ($event->getScheduleCollection() as $flightawareRoute) {
            $fromAirport = $this->airportDetailsService->getAirportByIcao($flightawareRoute->origin);
            $toAirport = $this->airportDetailsService->getAirportByIcao($flightawareRoute->destination);

            $acType = $flightawareRoute->aircraftType ?
                $this->aircraftTypeRepository->findByIcao($flightawareRoute->aircraftType) : null;

            if (!$acType) {
                $acType = $this->aircraftTypeRepository->getFirst();
            }

            if (!$fromAirport || !$toAirport || !$acType) {
                continue;

            }

            $this->flightRepository->createOrUpdateByFlightNumber(
                $flightawareRoute->identIcao,
                $flightawareRoute->origin,
                $flightawareRoute->destination,
                range(1, 7),
                new DateTimeImmutable($flightawareRoute->scheduledOff),
                new DateTimeImmutable(date("Y").'-01-01 00:00:00'),
                new DateTimeImmutable(date("Y").'-12-31 23:59:59'),
                $acType->getIcao()
            );
        }
    }

    private function createUtairLegacyScheduleCsv(UtairFlightsCollection $collection): void
    {
        $filename = (new DateTimeImmutable())->format('Y-m-d-H-i-s') . '.csv';

        $handler = fopen(__DIR__ . "/../docs/$filename", 'w');
        $addedRows = [];

        foreach ($collection as $flight) {
            foreach ($flight->getSegments() as $segment) {
                $departureIcao = $segment->getDeparture();
                $arrivalIcao = $segment->getArrival();

                if (in_array($segment->getUuid(), $addedRows)) {
                    continue;
                }
                fputcsv($handler, [
                    $segment->getFlightNumber(),
                    $departureIcao,
                    $arrivalIcao,
                    implode('', $segment->getDays()),
                    str_replace(':', '', $segment->getDepartureTime()),
                    $segment->getStartPeriodDate()->format('dm'),
                    $segment->getEndPeriodDate()->format('dm'),
                    $segment->getLegacyAircraftType(),
                    rand(46, 100)
                ],
                    ';'
                );
            }
        }
    }

    public function utairScheduleImportFinished(UtairScheduleImportFinishedEvent $event): void
    {
        if ($event->isLegacy()) {
            $this->createUtairLegacyScheduleCsv($event->getCollection());

            return;
        }

        foreach ($event->getCollection() as $flight) {
            foreach ($flight->getSegments() as $segment) {
                if (!$this->aircraftTypeRepository->findByIcao($segment->getAircraftType())) {
                    continue;
                }

                $fromAirport = $this->airportDetailsService->getAirportByIcao($segment->getDeparture());
                $toAirport = $this->airportDetailsService->getAirportByIcao($segment->getArrival());

                if (!$fromAirport || !$toAirport) {
                    continue;
                }

                $this->flightRepository->createOrUpdateByFlightNumber(
                    "UTA".$segment->getFlightNumber(),
                    $segment->getDeparture(),
                    $segment->getArrival(),
                    $segment->getDays(),
                    DateTimeImmutable::createFromFormat('H:i', $segment->getDepartureTime()),
                    $segment->getStartPeriodDate(),
                    $segment->getEndPeriodDate(),
                    $segment->getAircraftType(),
                );
            }
        }
    }
}
