<?php

namespace App\Modules\TimetableBundle\Entity;

use App\Modules\AirportBundle\Entity\Airport;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
class FlightAlternate
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private int $id;

    #[ORM\ManyToOne(targetEntity: Airport::class)]
    #[ORM\JoinColumn(name: 'alternate_icao', referencedColumnName: 'icao', nullable: false)]
    private Airport $alternateAirport;

    #[ORM\ManyToOne(targetEntity: Flight::class, inversedBy: 'alternates')]
    private Flight $flight;

    public function __construct($alternateAirport, Flight $flight)
    {
        $this->alternateAirport = $alternateAirport;
        $this->flight = $flight;
    }

    public function airport(): Airport
    {
        return $this->alternateAirport;
    }
}
