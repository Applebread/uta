<?php

declare(strict_types=1);

namespace App\Modules\TimetableBundle\Entity;

class FlightStatus
{
    const IDLE = 'idle';

    const BOOKING = 'booking';

    const IN_PROGRESS = 'progress';

    const DONE = 'done';

    private string $value;

    public function __construct(string $value)
    {
        $this->value = $value;
    }

    public function __toString(): string
    {
        return $this->value;
    }

    public function is(string ...$type): bool
    {
        return in_array($this->value, $type);
    }
}
