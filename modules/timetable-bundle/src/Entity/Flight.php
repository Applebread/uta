<?php

declare(strict_types=1);

namespace App\Modules\TimetableBundle\Entity;

use App\Modules\AircraftBundle\Entity\AircraftType;
use App\Modules\AirportBundle\Entity\Airport;
use App\Modules\TimetableBundle\Repository\FlightRepository;
use DateTimeImmutable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use InvalidArgumentException;

#[ORM\Entity(repositoryClass: FlightRepository::class)]
class Flight
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private ?int $id = null;

    #[ORM\Column(type: 'string', length: 7)]
    private string $flightNumber;

    #[ORM\ManyToOne(targetEntity: Airport::class, cascade: ['persist'])]
    #[ORM\JoinColumn(name:'from_icao', referencedColumnName: 'icao')]
    private Airport $from;

    #[ORM\ManyToOne(targetEntity: Airport::class, cascade: ['persist'])]
    #[ORM\JoinColumn(name:'to_icao', referencedColumnName: 'icao')]
    private Airport $to;

    #[ORM\Column(type: 'simple_array')]
    private array $daysOfWeek;

    #[ORM\Column(type: 'datetime_immutable')]
    private DateTimeImmutable $departureTime;

    #[ORM\Column(type: 'datetime_immutable')]
    private DateTimeImmutable $scheduledSince;

    #[ORM\Column(type: 'datetime_immutable')]
    private DateTimeImmutable $scheduledTill;

    #[ORM\ManyToOne(targetEntity: AircraftType::class)]
    private ?AircraftType $aircraftType;

    #[ORM\OneToMany(mappedBy: 'flights', targetEntity: FlightAlternate::class, cascade: ['remove', 'persist'])]
    private ?Collection $alternates;

    public function __construct(
        string $flightNumber,
        Airport $from,
        Airport $to,
        array $daysOfWeek,
        DateTimeImmutable $departureTime,
        DateTimeImmutable $scheduledSince,
        DateTimeImmutable $scheduledTill,
        AircraftType $aircraftType,
    )
    {
        $this->setFlightNumber($flightNumber);
        $this->updateRoute($from, $to);
        $this->updateSchedule($scheduledSince, $scheduledTill, $departureTime, $daysOfWeek);
        $this->setAircraftType($aircraftType);
        $this->alternates = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFlightNumber(): string
    {
        return $this->flightNumber;
    }

    public function getFrom(): Airport
    {
        return $this->from;
    }

    public function getTo(): Airport
    {
        return $this->to;
    }

    public function addAlternate(Airport $alternate): void
    {
        $this->alternates->add(new FlightAlternate($alternate, $this));
    }

    public function updateSchedule(
        DateTimeImmutable $scheduledSince,
        DateTimeImmutable $scheduledTill,
        DateTimeImmutable $departureTime,
        array $daysOfWeek,
    ): void
    {
        $daysOfWeek = array_filter($daysOfWeek, function ($day) { return is_int($day) && $day > 0 && $day <= 7; });
        $daysOfWeek = array_unique($daysOfWeek);;

        if ($scheduledSince > $scheduledTill || empty($daysOfWeek)) {
            throw new InvalidArgumentException("Incorrect scheduled period values");
        }

        sort($daysOfWeek);

        $this->scheduledSince = $scheduledSince->setTime(0, 0);
        $this->scheduledTill = $scheduledTill->setTime(0, 0);
        $this->departureTime = $departureTime;
        $this->daysOfWeek = $daysOfWeek;
    }

    public function getFlightScheduleDetails(): FlightScheduleDetails
    {
        return new FlightScheduleDetails($this->scheduledSince, $this->scheduledTill, $this->departureTime, $this->daysOfWeek);
    }

    public function updateRoute(Airport $from, Airport $to): void
    {
        if ($from->getIcao() === $to->getIcao()) {
            throw new InvalidArgumentException("Departure and arrival should be different");
        }

        $this->from = $from;
        $this->to = $to;
    }

    public function setFlightNumber(string $flightNumber): void
    {
        $this->flightNumber = $flightNumber;
    }

    public function getAircraftType(): ?AircraftType
    {
        return $this->aircraftType;
    }

    public function setAircraftType(?AircraftType $aircraftType): void
    {
        $this->aircraftType = $aircraftType;
    }

    public function getUuid(): string
    {
        return implode(':', [
            $this->flightNumber,
            $this->from->getIcao(),
            $this->to->getIcao(),
            $this->scheduledSince->format('Y.m.d'),
            $this->scheduledTill->format('Y.m.d'),
            implode(',', $this->daysOfWeek),
            $this->aircraftType->getIcao()
        ]);
    }
}
