<?php

declare(strict_types=1);

namespace App\Modules\TimetableBundle\Dto;

use App\Modules\TimetableBundle\Entity\Flight;
use Symfony\Component\HttpFoundation\Request;

final class TimetableItemDto
{
    private function __construct(
        public readonly ?int $id,
        public readonly string $flightNumber,
        public readonly string $from,
        public readonly string $to,
        public readonly ?string $aircraftType,
        public readonly string $etd,
        /** @var int[] */
        public readonly array $daysOfWeek,
        public readonly string $scheduledSince,
        public readonly string $scheduledTill,
    )
    {
    }

    public static function fromFlightEntity(Flight $flight): self
    {
        return new self(
            $flight->getId(),
            $flight->getFlightNumber(),
            $flight->getFrom()->getIcao(),
            $flight->getTo()->getIcao(),
            $flight->getAircraftType()?->getIcao(),
            $flight->getFlightScheduleDetails()->departureTime()->format('H:i'),
            $flight->getFlightScheduleDetails()->daysOfWeek(),
            $flight->getFlightScheduleDetails()->scheduledSince()->format('Y-m-d'),
            $flight->getFlightScheduleDetails()->scheduledTill()->format('Y-m-d'),
        );
    }

    public static function fromRequest(Request $request): self
    {
        $data = json_decode($request->getContent(), true);

        return new self(
            null,
            $data['flightNumber'],
            $data['from'],
            $data['to'],
            $data['aircraftType'],
            $data['etd'],
            $data['daysOfWeek'],
            $data['scheduledSince'],
            $data['scheduledTill']
        );
    }
}
