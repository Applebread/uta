<?php

declare(strict_types=1);

namespace App\Modules\TimetableBundle\Dto;

use App\Dto\AbstractPaginatedCollection;

class TimetableCollectionDto extends AbstractPaginatedCollection
{
    public function __construct(int $totalResults, TimetableItemDto ...$items)
    {
        $this->items = $items;
        $this->totalResults = $totalResults;
    }
    public function current(): TimetableItemDto
    {
        return current($this->items);
    }

    public static function empty(): self
    {
        return new self(0);
    }
}
