<?php

declare(strict_types=1);

namespace App\Modules\TimetableBundle\Repository;

use App\Modules\TimetableBundle\Dto\TimetableCollectionDto;
use App\Modules\TimetableBundle\Dto\TimetableItemDto;
use App\Modules\TimetableBundle\Entity\Flight;
use App\Modules\TimetableBundle\Factory\ParametersAwareFlightFactory;
use DateTimeImmutable;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\DBAL\Exception;
use Doctrine\Persistence\ManagerRegistry;

class FlightRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry, private readonly ParametersAwareFlightFactory $factory)
    {
        parent::__construct($registry, Flight::class);
    }

    public function save(Flight $flight): void
    {
        $this->_em->persist($flight);
        $this->_em->flush();
    }

    public function findByFlightNumber(string $flightNumber): ?Flight
    {
        return $this->findOneBy(['flightNumber' => $flightNumber]);
    }

    public function createOrUpdateByFlightNumber(
        string $flightNo,
        string $fromAirport,
        string $toAirport,
        array $daysOfWeek,
        DateTimeImmutable $departureTime,
        DateTimeImmutable $since,
        DateTimeImmutable $till,
        string $aircraftType
    ): Flight
    {
        $flight = $this->factory->createFromParameters($flightNo, $fromAirport, $toAirport, $daysOfWeek, $departureTime, $since, $till, $aircraftType);
        $existingFlight = $this->findByFlightNumber($flightNo);

        if ($existingFlight && $flight->getUuid() === $existingFlight->getUuid()) {
            $flight = $this->factory->updateFromParameters($existingFlight, $flightNo, $fromAirport, $toAirport, $daysOfWeek, $departureTime, $since, $till, $aircraftType);
        }

        $this->save($flight);

        return $flight;
    }

    public function createFromDto(TimetableItemDto $dto): void
    {
        $flight = $this->createOrUpdateByFlightNumber(
            $dto->flightNumber,
            $dto->from,
            $dto->to,
            $dto->daysOfWeek,
            new DateTimeImmutable($dto->etd),
            new DateTimeImmutable($dto->scheduledSince),
            new DateTimeImmutable($dto->scheduledTill),
            $dto->aircraftType
        );

        $this->save($flight);
    }

    public function deleteRecordById(int $id): void
    {
        $flightRef = $this->_em->getReference(Flight::class, $id);

        $this->_em->remove($flightRef);
        $this->_em->flush();
    }

    public function findByParameters(
        int $page,
        int $pageSize,
        ?DateTimeImmutable $actualDate,
        ?string $from = null,
        ?string $to = null,
    ): TimetableCollectionDto
    {
        $dayOfWeek = (int) $actualDate->format('w');
        $dayOfWeek = !$dayOfWeek ? 7 : $dayOfWeek;

        $qb = $this->createQueryBuilder('f');

        $qb->leftJoin('f.from', 'af');
        $qb->leftJoin('f.to', 'at');
        $qb->leftJoin('f.aircraftType', 'aircraftType');

        if ($from) {
            $qb->andWhere('af.icao = :from');
            $qb->setParameter('from', $from);
        }

        if ($to) {
            $qb->andWhere('at.icao = :to');
            $qb->setParameter('to', $to);
        }

        $qb->andWhere('f.scheduledSince < :date AND f.scheduledTill >= :date');
        $qb->andWhere($qb->expr()->like('f.daysOfWeek', ':dayOfWeek'));
        $qb->setParameter('date', $actualDate->format('Y-m-d'));
        $qb->setParameter('dayOfWeek', '%' . $dayOfWeek . '%');

        $totalCount = count($qb->getQuery()->getResult());

        $qb->setFirstResult(($page - 1) * $pageSize);
        $qb->setMaxResults($pageSize);

        $res = $qb->getQuery()->getResult();

        return new TimetableCollectionDto($totalCount, ...array_map(function (Flight $flight) {
            return TimetableItemDto::fromFlightEntity($flight);
        }, $res));
    }

    /**
     * @return string[]
     * @throws Exception
     */
    public function getScheduledAirports(): array
    {
        return $this->_em->getConnection()->fetchFirstColumn("
            SELECT DISTINCT a.icao FROM airport a
            INNER JOIN flight f on a.icao = f.from_icao
            INNER JOIN flight f2 on a.icao = f2.to_icao
        ");
    }

    /**
     * @return string[]
     * @throws Exception
     */
    public function getScheduledAircraftTypes(): array
    {
        return $this->_em->getConnection()->fetchFirstColumn("
            SELECT DISTINCT a.icao FROM aircraft_type a
            INNER JOIN flight f on a.id = f.aircraft_type_id
            INNER JOIN flight f2 on a.id = f2.aircraft_type_id
        ");
    }
}
