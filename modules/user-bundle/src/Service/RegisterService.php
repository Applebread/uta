<?php

declare(strict_types=1);

namespace App\Modules\UserBundle\Service;

use App\Modules\UserBundle\Dto\RegisterRequestDto;
use App\Modules\UserBundle\Entity\User;
use App\Modules\UserBundle\Event\NewUserRegisteredEvent;
use App\Modules\UserBundle\Interfaces\RegisterServiceInterface;
use App\Modules\UserBundle\Repository\UserRepository;
use Psr\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\PasswordHasher\PasswordHasherInterface;

class RegisterService implements RegisterServiceInterface
{
    public function __construct(
        private readonly PasswordHasherInterface $passwordHasher,
        private readonly UserRepository $userRepository,
        private readonly EventDispatcherInterface $eventDispatcher
    )
    {
    }

    public function register(RegisterRequestDto $request): void
    {
        if ($this->userRepository->existBy('login', $request->login) || $this->userRepository->existBy('email', $request->email)) {

            throw new BadRequestHttpException('User already exist');
        }

        $user = $this->userRepository->save(new User(
            $request->login,
            $request->email,
            $request->fullName,
            $request->getBirthDate(),
            $this->passwordHasher->hash($request->password),
            ['ROLE_USER']
        ));

        $this->eventDispatcher->dispatch(new NewUserRegisteredEvent($user->getId()));
    }
}
