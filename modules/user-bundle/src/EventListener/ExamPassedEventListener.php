<?php

namespace App\Modules\UserBundle\EventListener;

use App\Modules\ExamBundle\Event\ExamPassedEvent;
use App\Modules\UserBundle\Repository\UserRepository;

class ExamPassedEventListener
{
    public function __construct(private readonly UserRepository $repository)
    {
    }

    public function __invoke(ExamPassedEvent $event): void
    {
        if ($event->isSuccess() && $event->isEntrance()) {
            $this->repository->makePilot($event->userId());
        }
    }
}
