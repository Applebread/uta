<?php

declare(strict_types=1);

namespace App\Modules\UserBundle\Controller;

use App\Modules\UserBundle\Interfaces\RegisterServiceInterface;
use OpenApi\Attributes\Tag;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Modules\UserBundle\Dto\RegisterRequestDto;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Nelmio\ApiDocBundle\Annotation as NA;
use OpenApi\Annotations as OA;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class RegistrationController extends AbstractController
{
    public function __construct(private RegisterServiceInterface $registerService)
    {
    }

    /**
     * @OA\RequestBody(@OA\JsonContent(ref=@NA\Model(type=RegisterRequestDto::class)))
     */
    #[Route(path:'/api/auth/register', name: 'auth_register', methods:["POST"])]
    #[Tag('User')]
    public function register(Request $request, ValidatorInterface $validator, TranslatorInterface $translator): Response
    {
        $registerRequest = RegisterRequestDto::fromRequest($request);

        $errors = $validator->validate($registerRequest);

        if ($errors->count()) {
            $errorsData = [];

            foreach ($errors as $error) {
                $errorsData['errors'][$error->getPropertyPath()] = $translator->trans($error->getMessage());
            }

            return new JsonResponse($errorsData, Response::HTTP_BAD_REQUEST);
        }

        $this->registerService->register($registerRequest);

        return new JsonResponse(null, Response::HTTP_NO_CONTENT);
    }
}
