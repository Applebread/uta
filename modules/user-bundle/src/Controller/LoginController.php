<?php

declare(strict_types=1);

namespace App\Modules\UserBundle\Controller;

use App\Modules\UserBundle\Dto\AuthRequestDto;
use App\Modules\UserBundle\Dto\LoggedUserDto;
use OpenApi\Attributes\Tag;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Nelmio\ApiDocBundle\Annotation as NA;
use OpenApi\Annotations as OA;

class LoginController extends AbstractController
{
    /**
     * @OA\RequestBody(@OA\JsonContent(ref=@NA\Model(type=AuthRequestDto::class)))
     */
    #[Route(path:'/api/auth', name: 'auth_login', methods:["POST"])]
    #[Tag('User')]
    public function authUser(): JsonResponse
    {
        if (($user = $this->getUser()) === null) {
            return $this->json([
                'message' => 'missing credentials',
            ], Response::HTTP_UNAUTHORIZED);
        }

        return new JsonResponse(LoggedUserDto::fromEntity($user));
    }

    #[Route(path:'/api/auth/info', name: 'auth_info', methods:["GET"])]
    #[Tag('User')]
    public function userInfoController(): JsonResponse
    {
        return $this->authUser();
    }

    #[Route(path:'/api/logout', name: 'app_logout', methods:["GET"])]
    #[Tag('User')]
    public function logout(): JsonResponse
    {
        throw new \Exception('Don\'t forget to activate logout in security.yaml');
    }
}
