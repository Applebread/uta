<?php

declare(strict_types=1);

namespace App\Modules\UserBundle\Event;

final class NewUserRegisteredEvent
{
    public function __construct(private readonly int $userId)
    {
    }

    public function userId(): int
    {
        return $this->userId;
    }
}
