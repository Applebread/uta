<?php

declare(strict_types=1);

namespace App\Modules\UserBundle\Dto;

use DateTimeImmutable;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;

final class RegisterRequestDto
{
    private function __construct(
        #[Assert\NotBlank(message: 'REGISTER_EMPTY_PID_MESSAGE')]
        public readonly string $login,
        #[Assert\NotBlank(message: 'REGISTER_EMPTY_EMAIL_MESSAGE')]
        #[Assert\Email]
        public readonly string $email,
        #[Assert\NotBlank(message: 'REGISTER_EMPTY_FULL_NAME')]
        public readonly string $fullName,
        #[Assert\NotBlank(message: 'REGISTER_EMPTY_BIRTHDATE')]
        #[Assert\Date]
        public readonly string $birthDate,
        #[Assert\NotBlank(message: 'REGISTER_EMPTY_PASS_MESSAGE')]
        public readonly string $password,
        #[Assert\NotBlank(message: 'REGISTER_EMPTY_PASS_CONFIRM_MESSAGE')]
        #[Assert\EqualTo(propertyPath: 'password', message: 'REGISTER_PASS_CONFIRM_NOT_EQUALS')]
        public readonly string $confirmPassword
    )
    {
    }

    public static function fromRequest(Request $request): self
    {
        $body = $request->toArray();

        return new self(
            $body['login'],
            $body['email'],
            $body['fullName'],
            $body['birthDate'],
            $body['password'],
            $body['confirmPassword'],
        );
    }

    public function getBirthDate(): DateTimeImmutable
    {
        return DateTimeImmutable::createFromFormat('Y-m-d', $this->birthDate);
    }
}
