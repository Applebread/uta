<?php

declare(strict_types=1);

namespace App\Modules\UserBundle\Dto;

use App\Modules\UserBundle\Entity\User;

final class LoggedUserDto
{
    private function __construct(
        public string $login,
        public string $fullName,
        public string $email,
        public array $roles
    )
    {
    }

    public static function fromEntity(User $user): self
    {
        return new self($user->getLogin(), $user->getFullName(), $user->getEmail(), array_values($user->getRoles()));
    }
}
