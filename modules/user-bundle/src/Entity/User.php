<?php

declare(strict_types=1);

namespace App\Modules\UserBundle\Entity;

use App\Modules\UserBundle\Repository\UserRepository;
use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;

#[ORM\Entity(repositoryClass: UserRepository::class)]
#[ORM\Table(name: '`user`')]
#[ORM\InheritanceType('SINGLE_TABLE')]
#[ORM\DiscriminatorColumn(name: 'type', type: 'string')]
class User implements UserInterface, PasswordAuthenticatedUserInterface
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private int $id;

    #[ORM\Column(type: 'string', unique: true)]
    private string $login;

    #[ORM\Column(type: 'string')]
    private string $fullName;

    #[ORM\Column(type: 'date_immutable')]
    private DateTimeImmutable $birthDate;

    #[ORM\Column(type: 'string', length: 180, unique: true)]
    private string $email;

    #[ORM\Column(type: 'json')]
    private array $roles = [];

    #[ORM\Column(type: 'string')]
    private string $password;

    public function __construct(
        string            $login,
        string            $email,
        string            $fullName,
        DateTimeImmutable $birthDate,
        string            $password,
        array             $roles
    )
    {
        $this->login = $login;
        $this->email = $email;
        $this->fullName = $fullName;
        $this->birthDate = $birthDate;
        $this->password = $password;
        $this->roles = $roles;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUsername(): string
    {
        return $this->login;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function getFullName(): string
    {
        return $this->fullName;
    }

    public function getBirthDate(): DateTimeImmutable
    {
        return $this->birthDate;
    }

    public function getUserIdentifier(): string
    {
        return $this->login;
    }

    public function getRoles(): array
    {
        $roles = $this->roles;
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function getPassword(): string
    {
        return $this->password;
    }

    public function getSalt(): ?string
    {
        return null;
    }

    public function eraseCredentials()
    {
    }

    public function getLogin(): string
    {
        return $this->login;
    }

    public function grantRole(string $role): void
    {
        $this->roles[] = $role;
    }

    public function isRole(string $role): bool
    {
        return in_array($role, $this->roles);
    }
}
