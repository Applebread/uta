<?php

declare(strict_types=1);

namespace App\Modules\UserBundle\Repository;

use App\Modules\UserBundle\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class UserRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    public function existBy(string $key, string $value): bool
    {
        return (bool) $this->findOneBy([$key => $value]);
    }

    public function makePilot(int $id)
    {
        $user = $this->find($id);

        $this->_em->getConnection()->update(
            '"user"',
            ['type' => 'pilot'],
            ['id' => $id]
        );

        $user->grantRole('ROLE_PILOT');
        $this->save($user);
        $this->_em->clear();
    }

    public function save(User $user): User
    {
        $this->_em->persist($user);
        $this->_em->flush();

        return $user;
    }
}
