<?php

declare(strict_types=1);

namespace App\Modules\PilotBundle\Fixture;

use App\Modules\PilotBundle\Entity\Rank;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use function Symfony\Component\Translation\t;

class RankFixture extends Fixture
{

    public function __construct(private ParameterBagInterface $bag)
    {
    }

    public function load(ObjectManager $manager)
    {
        $fo = new Rank();
        $fo->setName('First officer');
        $fo->setIcon($this->bag->get('pilot_bundle.rank_icon_path').'/'.'bar1_sm.png');
        $fo->setInitial(true);

        $manager->persist($fo);

        $captain = new Rank();
        $captain->setName('Captain');
        $captain->setIcon($this->bag->get('pilot_bundle.rank_icon_path').'/'.'bar2_sm.png');

        $manager->persist($captain);

        $chief = new Rank();
        $chief->setName('Chief Pilot');
        $chief->setIcon($this->bag->get('pilot_bundle.rank_icon_path').'/'.'bar3_sm.png');

        $manager->persist($chief);

        $instructor = new Rank();
        $instructor->setName('Pilot instructor');
        $instructor->setIcon($this->bag->get('pilot_bundle.rank_icon_path').'/'.'bar4_sm.png');

        $manager->persist($instructor);

        $manager->flush();
    }
}
