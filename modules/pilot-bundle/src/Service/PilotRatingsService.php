<?php

declare(strict_types=1);

namespace App\Modules\PilotBundle\Service;

use App\Modules\PilotBundle\Entity\Minimum;
use App\Modules\PilotBundle\Entity\Pilot;
use App\Modules\PilotBundle\Entity\PilotMinimum;
use App\Modules\PilotBundle\Entity\PilotRank;
use App\Modules\PilotBundle\Entity\Rank;
use App\Modules\PilotBundle\Entity\TypeRating;
use App\Modules\PilotBundle\Interfaces\PilotRankServiceInterface;
use App\Modules\PilotBundle\Interfaces\PilotTypeRatingServiceInterface;
use App\Modules\PilotBundle\Repository\PilotRepository;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class PilotRatingsService implements PilotRankServiceInterface, PilotTypeRatingServiceInterface
{
    public function __construct(private readonly PilotRepository $pilotRepository)
    {
    }

    public function updatePilotRank(Pilot $pilot, Rank $rank): void
    {
        if ($pilotRank = $pilot->rank()) {
            $pilotRank->setRank($rank);
            $this->pilotRepository->save($pilot);

            return;
        }

        $pilot->setRank(new PilotRank($pilot, $rank));

        $this->pilotRepository->save($pilot);
    }

    public function updatePilotMinimum(Pilot $pilot, Minimum $minimum): void
    {
        if ($pilot->minimum()) {
            $pilot->minimum()->setMinimum($minimum);

            $this->pilotRepository->save($pilot);
        }

        $pilot->setMinimum(new PilotMinimum($pilot, $minimum));

        $this->pilotRepository->save($pilot);
    }

    public function grantRating(Pilot $pilot, TypeRating $rating): void
    {
        $pilot->grantTypeRating($rating);
        $this->pilotRepository->save($pilot);
    }

    public function removeRating(Pilot $pilot, TypeRating $rating): void
    {
        if (!$pilot->hasRating($rating)) {
            throw new BadRequestHttpException("Pilot haven't ".$rating." rating");
        }

        $pilot->removeTypeRating($rating);
        $this->pilotRepository->save($pilot);
    }
}
