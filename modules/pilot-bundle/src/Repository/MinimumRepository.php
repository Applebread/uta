<?php

declare(strict_types=1);

namespace App\Modules\PilotBundle\Repository;

use App\Modules\PilotBundle\Entity\Minimum;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use RuntimeException;

class MinimumRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Minimum::class);
    }

    public function getInitialMinimum(): Minimum
    {
        if (!$minimum = $this->findOneBy(['initial' => true])) {
            throw new RuntimeException("No initial minimum");
        }

        return $minimum;
    }
}
