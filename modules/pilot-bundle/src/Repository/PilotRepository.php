<?php

declare(strict_types=1);

namespace App\Modules\PilotBundle\Repository;

use App\Modules\PilotBundle\Dto\PilotsListDto;
use App\Modules\PilotBundle\Dto\PilotStatisticDto;
use App\Modules\PilotBundle\Entity\Pilot;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

final class PilotRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Pilot::class);
    }

    public function getById(int $id): Pilot
    {
        return $this->findOneBy(['id' => $id]);
    }

    public function getPilotsList(int $page, int $pageSize): PilotsListDto
    {
        $pilots = $this->findBy([], ['id' => 'ASC'], $pageSize, ($page - 1) * $pageSize);
        $totalCount = $this->count([]);

        $pilotsList = [];

        foreach ($pilots as $pilot) {
            $pilotsList[] = PilotStatisticDto::fromPilotEntity($pilot);
        }

        return new PilotsListDto($totalCount, ...$pilotsList);
    }

    public function save(Pilot $pilot): void
    {
        $this->_em->persist($pilot);
        $this->_em->flush();
    }
}
