<?php

namespace App\Modules\PilotBundle\Repository;

use App\Modules\PilotBundle\Entity\TypeRating;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class TypeRatingRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TypeRating::class);
    }
}
