<?php

declare(strict_types=1);

namespace App\Modules\PilotBundle\Repository;

use App\Modules\PilotBundle\Entity\LogbookItem;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class LogbookRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, LogbookItem::class);
    }

    public function allByPilotId(int $userId): array
    {
        return $this->findBy(['user' => $userId]);
    }

    public function save(LogbookItem $logbookItem): void
    {
        $this->_em->persist($logbookItem);
        $this->_em->flush();
    }
}
