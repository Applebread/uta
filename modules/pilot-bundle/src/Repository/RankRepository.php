<?php

declare(strict_types=1);

namespace App\Modules\PilotBundle\Repository;

use App\Modules\PilotBundle\Entity\Rank;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use RuntimeException;

class RankRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Rank::class);
    }

    public function getInitialRank(): Rank
    {
        if (!$rank = $this->findOneBy(['initial' => true])) {
            throw new RuntimeException("No initial rank");
        }

        return $rank;
    }
}
