<?php

namespace App\Modules\PilotBundle\EventSubscriber;

use App\Modules\AirportBundle\Interfaces\BaseAirportServiceInterface;
use App\Modules\ExamBundle\Event\ExamPassedEvent;
use App\Modules\ExamBundle\Interfaces\Entity\ExamInterface;
use App\Modules\ExamBundle\Interfaces\Repository\ExamRepositoryInterface;
use App\Modules\PilotBundle\Interfaces\PilotRankServiceInterface;
use App\Modules\PilotBundle\Interfaces\PilotTypeRatingServiceInterface;
use App\Modules\PilotBundle\Repository\MinimumRepository;
use App\Modules\PilotBundle\Repository\PilotRepository;
use App\Modules\PilotBundle\Repository\RankRepository;

class ExamPassedEventListener
{
    public function __construct(
        private readonly PilotRepository $pilotRepository,
        private readonly PilotRankServiceInterface $pilotRankService,
        private readonly MinimumRepository $minimumRepository,
        private readonly RankRepository $rankRepository,
        private readonly BaseAirportServiceInterface $baseAirportService,
        private readonly ExamRepositoryInterface $examRepository,
        private readonly PilotTypeRatingServiceInterface $pilotTypeRatingService
    )
    {
    }

    public function __invoke(ExamPassedEvent $event): void
    {
        if (!$pilot = $this->pilotRepository->getById($event->userId())) {
            return;
        }

        if (!$exam = $this->examRepository->getById($event->examId())) {
            return;
        }

        if ($event->isSuccess() && $event->isEntrance()) {
            $this->pilotRankService->updatePilotMinimum($pilot, $this->minimumRepository->getInitialMinimum());
            $this->pilotRankService->updatePilotRank($pilot, $this->rankRepository->getInitialRank());
            $pilot->moveToLocation($this->baseAirportService->getBaseAirport());

            $this->pilotRepository->save($pilot);

            return;
        }

        if ($exam->is(ExamInterface::EXAM_TYPE_TRAINING) && $event->isSuccess()) {
            $this->pilotTypeRatingService->grantRating($pilot, $exam->typeRating());
        }
    }
}
