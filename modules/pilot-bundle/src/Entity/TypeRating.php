<?php

declare(strict_types=1);

namespace App\Modules\PilotBundle\Entity;

use App\Modules\AircraftBundle\Entity\AircraftType;
use App\Modules\PilotBundle\Repository\TypeRatingRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: TypeRatingRepository::class)]
class TypeRating
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private int $id;

    #[ORM\Column(type: 'string')]
    private string $name;

    #[ORM\OneToMany(mappedBy: 'typeRating', targetEntity: AircraftType::class, cascade: ['all'], orphanRemoval: true)]
    private ?Collection $aircraftTypes;

    public function __construct(string $name, Collection $aircraftTypes)
    {
        $this->name = $name;
        $this->aircraftTypes = $aircraftTypes->count() ? $aircraftTypes : new ArrayCollection();
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function aircraftTypes(): ?Collection
    {
        return $this->aircraftTypes;
    }

    public function addAircraftType(AircraftType $aircraftType): void
    {
        $aircraftType->setTypeRating($this);
        $this->aircraftTypes->add($aircraftType);
    }

    public function __toString(): string
    {
        return $this->id . ': ' . $this->name;
    }
}
