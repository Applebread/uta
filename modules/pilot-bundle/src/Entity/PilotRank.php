<?php

namespace App\Modules\PilotBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
class PilotRank
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private int $id;

    #[ORM\OneToOne(targetEntity: Pilot::class)]
    private Pilot $pilot;

    #[ORM\ManyToOne(targetEntity: Rank::class, cascade: ['all'])]
    private Rank $rank;

    public function __construct(Pilot $pilot, Rank $rank)
    {
        $this->pilot = $pilot;
        $this->rank = $rank;
    }

    public function getPilot(): Pilot
    {
        return $this->pilot;
    }

    public function getRank(): Rank
    {
        return $this->rank;
    }

    public function setRank(Rank $rank): void
    {
        $this->rank = $rank;
    }
}
