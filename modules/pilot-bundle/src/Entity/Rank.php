<?php

declare(strict_types=1);

namespace App\Modules\PilotBundle\Entity;

use App\Modules\PilotBundle\Repository\RankRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: RankRepository::class)]
class Rank
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private int $id;

    #[ORM\Column(type: 'string')]
    private string $name;

    #[ORM\Column(type: 'string')]
    private string $icon;

    #[ORM\Column(type: 'boolean', nullable: false)]
    private bool $initial = false;

    public function getName(): string
    {
        return $this->name;
    }

    public function getIcon(): string
    {
        return $this->icon;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function setIcon(string $icon): void
    {
        $this->icon = $icon;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function isInitial(): bool
    {
        return $this->initial;
    }

    public function setInitial(bool $initial): void
    {
        $this->initial = $initial;
    }
}
