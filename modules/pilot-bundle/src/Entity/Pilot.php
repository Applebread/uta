<?php

namespace App\Modules\PilotBundle\Entity;

use App\Modules\AircraftBundle\Entity\AircraftType;
use App\Modules\AirportBundle\Entity\Airport;
use App\Modules\UserBundle\Entity\User;
use App\Modules\UserBundle\Repository\UserRepository;
use DateTimeImmutable;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\PersistentCollection;

#[ORM\Entity(repositoryClass: UserRepository::class)]
class Pilot extends User
{

    #[ORM\Column(type: 'integer', nullable: true)]
    private ?int $flightTime;

    #[ORM\ManyToOne(targetEntity: PilotRank::class, cascade: ['all'], inversedBy: 'pilot')]
    private ?PilotRank $rank;

    #[ORM\ManyToOne(targetEntity: PilotMinimum::class, cascade: ['all'], inversedBy: 'pilot')]
    private ?PilotMinimum $minimum;

    #[ORM\ManyToMany(targetEntity: TypeRating::class)]
    private ?PersistentCollection $typeRatings;

    #[ORM\ManyToOne(targetEntity: Airport::class, cascade: ['persist'])]
    #[ORM\JoinColumn(name:'location_id', referencedColumnName: 'icao')]
    private Airport $location;

    #[ORM\OneToMany(mappedBy: 'pilot', targetEntity: LogbookItem::class, cascade: ['remove', 'persist'])]
    private ?PersistentCollection $logbookItems;

    public function __construct(
        string                $login,
        string                $email,
        string                $fullName,
        DateTimeImmutable     $birthDate,
        string                $password,
        Airport               $location,
        ?int                  $flightTime = 0,
        ?PersistentCollection $logbookItems = null,
        ?PersistentCollection $typeRatings = null
    )
    {
        $this->logbookItems = $logbookItems;
        $this->typeRatings = $typeRatings;
        $this->flightTime = $flightTime;
        $this->location = $location;

        parent::__construct($login, $email, $fullName, $birthDate, $password, ['ROLE_PILOT']);
    }

    public function setRank(PilotRank $rank): void
    {
        $this->rank = $rank;
    }

    public function setMinimum(PilotMinimum $minimum): void
    {
        $this->minimum = $minimum;
    }

    public function removeTypeRating(TypeRating $rating): void
    {
        $this->typeRatings->removeElement($rating);
    }

    public function grantTypeRating(TypeRating $rating): void
    {
        if ($this->hasRating($rating)) {
            return;
        }

        $this->typeRatings->add($rating);
    }

    public function hasRating(TypeRating $rating): bool
    {
        return $this->typeRatings->contains($rating);
    }

    public function hasRatingIcaoString(string $ratingIcaoString): bool
    {
        foreach ($this->typeRatings as $typeRating) {
            foreach ($typeRating->aircraftTypes() as $aircraftType) {
                if ($aircraftType->getIcao() === $ratingIcaoString) {
                    return true;
                }
            }
        }

        return false;
    }

    public function getTypeRatings(): Collection
    {
        return $this->typeRatings;
    }

    public function flightTime(): ?int
    {
        return $this->{__FUNCTION__};
    }

    public function rank(): ?PilotRank
    {
        return $this->{__FUNCTION__};
    }

    public function minimum(): ?PilotMinimum
    {
        return $this->{__FUNCTION__};
    }

    public function logbookItems(): \Traversable
    {
        return $this->logbookItems->getIterator();
    }

    public function addLogbookItem(LogbookItem $item)
    {
        $this->logbookItems->add($item);
    }

    public function getLocation(): Airport
    {
        return $this->location;
    }

    public function moveToLocation(Airport $airport): void
    {
        $this->location = $airport;
    }
}
