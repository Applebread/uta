<?php

namespace App\Modules\PilotBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
class PilotMinimum
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private int $id;

    #[ORM\OneToOne(targetEntity: Pilot::class)]
    private Pilot $pilot;

    #[ORM\ManyToOne(targetEntity: Minimum::class, cascade: ['all'])]
    private Minimum $minimum;

    public function __construct(Pilot $pilot, Minimum $minimum)
    {
        $this->pilot = $pilot;
        $this->minimum = $minimum;
    }

    public function getPilot(): Pilot
    {
        return $this->pilot;
    }

    public function getMinimum(): Minimum
    {
        return $this->minimum;
    }

    public function setMinimum(Minimum $minimum): void
    {
        $this->minimum = $minimum;
    }
}
