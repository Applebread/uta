<?php

declare(strict_types=1);

namespace App\Modules\PilotBundle\Entity;

use App\Modules\PilotBundle\Repository\MinimumRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: MinimumRepository::class)]
class Minimum
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private int $id;

    #[ORM\Column(type: 'string')]
    private string $value;

    #[ORM\Column(type: 'boolean', nullable: false)]
    private bool $initial = false;

    public function getId(): int
    {
        return $this->id;
    }

    public function getValue(): string
    {
        return $this->value;
    }

    public function setValue(string $value): void
    {
        $this->value = $value;
    }

    public function isInitial(): bool
    {
        return $this->initial;
    }

    public function setInitial(bool $initial): void
    {
        $this->initial = $initial;
    }
}
