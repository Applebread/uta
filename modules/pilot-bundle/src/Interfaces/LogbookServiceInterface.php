<?php

namespace App\Modules\PilotBundle\Interfaces;

use DateTimeImmutable;

interface LogbookServiceInterface
{
    public function createReport(
        string $from,
        string $to,
        string $landingAirport,
        DateTimeImmutable $departure,
        DateTimeImmutable $arrival,
        string $route,
        ?string $delayReason =  null
    ): void;
}
