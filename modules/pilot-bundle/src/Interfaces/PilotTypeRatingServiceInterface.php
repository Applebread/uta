<?php

namespace App\Modules\PilotBundle\Interfaces;

use App\Modules\PilotBundle\Entity\Pilot;
use App\Modules\PilotBundle\Entity\TypeRating;

interface PilotTypeRatingServiceInterface
{
    public function grantRating(Pilot $pilot, TypeRating $rating): void;

    public function removeRating(Pilot $pilot, TypeRating $rating): void;
}
