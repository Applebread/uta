<?php

namespace App\Modules\PilotBundle\Dto;

use App\Modules\AircraftBundle\Dto\AircraftTypeDto;
use App\Modules\AircraftBundle\Entity\AircraftType;
use App\Modules\PilotBundle\Entity\TypeRating;

class TypeRatingDto
{
    public function __construct(
        public readonly int $id,
        public readonly string $name,
        /** @var AircraftTypeDto[] */
        public readonly array $aircraftTypes
    )
    {
    }

    public static function fromEntity(TypeRating $typeRating): self
    {
        return new self(
            $typeRating->getId(),
            $typeRating->getName(),
            $typeRating->aircraftTypes()
                ->map(function (AircraftType $aircraftType) {
                    return TypeRatingAircraftTypeDto::fromEntity($aircraftType);
                })->toArray()
        );
    }
}
