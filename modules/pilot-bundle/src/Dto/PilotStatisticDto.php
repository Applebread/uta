<?php

declare(strict_types=1);

namespace App\Modules\PilotBundle\Dto;

use App\Modules\PilotBundle\Entity\Pilot;
use App\Modules\PilotBundle\Entity\TypeRating;

final class PilotStatisticDto
{

    private function __construct(
        public int     $id,
        public string  $pid,
        public string  $email,
        public string  $name,
        public string  $flightTime,
        public string  $birthDate,
        public string  $avatar,
        public array   $typeRatings,
        public string  $location,
        public ?string $minimum,
        public ?string $rank,
        public ?string $rankIcon,
        public ?array  $logbookItems
    )
    {
    }

    public static final function fromPilotEntity(Pilot $pilot): self
    {
        return new self(
            $pilot->getId(),
            $pilot->getLogin(),
            $pilot->getEmail(),
            $pilot->getFullName(),
            $pilot->flightTime() ? date('H:i', mktime(0, $pilot->flightTime())) : '0:00',
            $pilot->getBirthDate()->format('Y-m-d'),
            '',
            $pilot->getTypeRatings()->map(function (TypeRating $typeRating) { return TypeRatingDto::fromEntity($typeRating); })->toArray(),
            $pilot->getLocation()->getIcao(),
            $pilot->minimum()?->getMinimum()->getValue(),
            $pilot->rank()?->getRank()->getName(),
            $pilot->rank()?->getRank()->getIcon(),
            array_map(
                [LogbookItemDto::class, 'fromEntity'],
                iterator_to_array($pilot->logbookItems())
            )
        );
    }
}
