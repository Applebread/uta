<?php

namespace App\Modules\PilotBundle\Dto;

use App\Modules\AircraftBundle\Entity\AircraftType;

class TypeRatingAircraftTypeDto
{
    private function __construct(
        public ?int $id,
        public string $icao,
        public string $name,
        public string $description,
        public string $image,
        public bool $heavy,
    )
    {
    }

    public static final function fromEntity(AircraftType $aircraft): self
    {
        $me = new self(
            $aircraft->getId(),
            $aircraft->getIcao(),
            $aircraft->getName(),
            $aircraft->getDescription(),
            $aircraft->getImage(),
            $aircraft->isHeavy()
        );

        $me->id = $aircraft->getId();

        return $me;
    }
}
