<?php

declare(strict_types=1);

namespace App\Modules\PilotBundle\Controller\Admin;

use App\Modules\PilotBundle\Dto\MinimumDto;
use App\Modules\PilotBundle\Entity\Minimum;
use App\Modules\PilotBundle\Entity\Pilot;
use App\Modules\PilotBundle\Interfaces\PilotRankServiceInterface;
use App\Modules\PilotBundle\Repository\MinimumRepository;
use OpenApi\Annotations as OA;
use OpenApi\Attributes\Tag;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class MinimumAdminController
{
    #[Route(path: '/api/minimum/list', name: 'minimum_list', methods: ["GET"])]
    #[Tag('Pilot')]
    #[IsGranted("ROLE_ADMIN")]
    public function minimumList(MinimumRepository $minimumRepository): JsonResponse
    {
        return new JsonResponse(array_map(function (Minimum $minimum) {
            return MinimumDto::fromEntity($minimum);
        }, $minimumRepository->findAll()));
    }

    /**
     * @OA\Response(response=204,description="Done")
     * @OA\Response(response=401,description="Unauthorized")
     * @OA\Response(response=403,description="Forbidden")
     */
    #[Route(path: '/api/pilot/{pilot}/minimum/{minimum}', name: 'minimum_update', methods: ["PUT"])]
    #[ParamConverter('pilot', class: 'App\Modules\PilotBundle\Entity\Pilot')]
    #[ParamConverter('minimum', class: 'App\Modules\PilotBundle\Entity\Minimum')]
    #[Tag('Pilot')]
    #[IsGranted("ROLE_ADMIN")]
    public function setPilotMinimum(Pilot $pilot, Minimum $minimum, PilotRankServiceInterface $pilotRankService): JsonResponse
    {
        $pilotRankService->updatePilotMinimum($pilot, $minimum);

        return new JsonResponse(null, Response::HTTP_NO_CONTENT);
    }
}
