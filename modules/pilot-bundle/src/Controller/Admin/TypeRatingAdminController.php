<?php

declare(strict_types=1);

namespace App\Modules\PilotBundle\Controller\Admin;

use App\Modules\PilotBundle\Dto\PilotStatisticDto;
use App\Modules\PilotBundle\Entity\Pilot;
use App\Modules\PilotBundle\Entity\TypeRating;
use App\Modules\PilotBundle\Interfaces\PilotTypeRatingServiceInterface;
use OpenApi\Attributes\Tag;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class TypeRatingAdminController
{
    #[Route(path: '/api/pilot/{pilot}/type-rating/{type}', name: 'grant_type_rating', methods: ["PUT"])]
    #[ParamConverter('pilot', class: Pilot::class)]
    #[ParamConverter('type', class: TypeRating::class)]
    #[Tag('Pilot')]
    #[IsGranted("ROLE_ADMIN")]
    public function grantTypeRating(Pilot $pilot, TypeRating $type, PilotTypeRatingServiceInterface $pilotTypeRatingService): JsonResponse
    {
        $pilotTypeRatingService->grantRating($pilot, $type);
        return new JsonResponse(PilotStatisticDto::fromPilotEntity($pilot), Response::HTTP_OK);
    }

    #[Route(path: '/api/pilot/{pilot}/type-rating/{type}', name: 'remove_type_rating', methods: ["DELETE"])]
    #[ParamConverter('pilot', class: Pilot::class)]
    #[ParamConverter('type', class: TypeRating::class)]
    #[Tag('Pilot')]
    #[IsGranted("ROLE_ADMIN")]
    public function removeTypeRating(Pilot $pilot, TypeRating $type, PilotTypeRatingServiceInterface $pilotTypeRatingService): JsonResponse
    {
        $pilotTypeRatingService->removeRating($pilot, $type);
        return new JsonResponse(PilotStatisticDto::fromPilotEntity($pilot), Response::HTTP_OK);
    }
}
