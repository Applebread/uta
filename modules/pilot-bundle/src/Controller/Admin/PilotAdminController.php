<?php

declare(strict_types=1);

namespace App\Modules\PilotBundle\Controller\Admin;

use App\Modules\PilotBundle\Repository\PilotRepository;
use OpenApi\Annotations as OA;
use OpenApi\Attributes\Parameter;
use OpenApi\Attributes\Tag;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;


class PilotAdminController extends AbstractController
{
    /**
     * @OA\Response(response=200, description="Pilots list")
     * @OA\Response(response=401, description="Unauthorized")
     * @OA\Response(response=403, description="Forbidden")
     */
    #[Route(path: '/api/pilots/list', name: 'pilots_list', methods: ["GET"])]
    #[Tag('Pilot')]
    #[Parameter(name: 'page', in: 'query')]
    #[Parameter(name: 'page_size', in: 'query')]
    #[IsGranted("ROLE_ADMIN")]
    public function getPilotsList(Request $request, PilotRepository $pilotRepository): JsonResponse
    {
        return new JsonResponse(
            $pilotRepository->getPilotsList(
                (int) $request->get('page', 1),
                (int) $request->get('page_size', 15)
            )
        );
    }
}
