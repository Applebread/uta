<?php

namespace App\Modules\PilotBundle\Controller\Admin;

use App\Modules\PilotBundle\Dto\RankDto;
use App\Modules\PilotBundle\Entity\Pilot;
use App\Modules\PilotBundle\Entity\Rank;
use App\Modules\PilotBundle\Interfaces\PilotRankServiceInterface;
use App\Modules\PilotBundle\Repository\RankRepository;
use OpenApi\Annotations as OA;
use OpenApi\Attributes\Tag;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class RankAdminController
{
    #[Route(path: '/api/rank/list', name: 'rank_list', methods: ["GET"])]
    #[Tag('Pilot')]
    #[IsGranted("ROLE_ADMIN")]
    public function rankList(RankRepository $rankRepository): JsonResponse
    {
        return new JsonResponse(array_map(function (Rank $rank) {
            return RankDto::fromEntity($rank);
        }, $rankRepository->findAll()));
    }

    /**
     * @OA\Response(response=204, description="Done")
     * @OA\Response(response=401, description="Unauthorized")
     * @OA\Response(response=403,description="Forbidden")
     */
    #[Route(path: '/api/pilot/{pilot}/rank/{rank}', name: 'rank_update', methods: ["PUT"])]
    #[ParamConverter('pilot', class: 'App\Modules\PilotBundle\Entity\Pilot')]
    #[ParamConverter('rank', class: 'App\Modules\PilotBundle\Entity\Rank')]
    #[Tag('Pilot')]
    #[IsGranted("ROLE_ADMIN")]
    public function updatePilotRank(Pilot $pilot, Rank $rank, PilotRankServiceInterface $pilotRankService): JsonResponse
    {
        $pilotRankService->updatePilotRank($pilot, $rank);

        return new JsonResponse(null, Response::HTTP_NO_CONTENT);
    }
}
