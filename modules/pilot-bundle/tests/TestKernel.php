<?php

namespace App\Modules\PilotBundle\Tests;

use App\Kernel;
use App\Modules\PilotBundle\PilotBundle;
use App\Modules\UserBundle\UserBundle;
use Doctrine\Bundle\DoctrineBundle\DoctrineBundle;
use Symfony\Bundle\FrameworkBundle\FrameworkBundle;

class TestKernel extends Kernel
{
    public function registerBundles(): array
    {
        return [
            new FrameworkBundle(),
            new DoctrineBundle(),

            new UserBundle(),
            new PilotBundle()
        ];
    }
}
