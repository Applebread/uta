<?php

declare(strict_types=1);

namespace App\Modules\AviationEdgeParserBundle\Value;

use App\Modules\AviationEdgeParserBundle\Interface\AviationEdgeRouteInterface;

class AviationEdgeRoute implements AviationEdgeRouteInterface
{
    public function __construct(
        private readonly string $airlineIcao,
        private readonly string $arrivalIcao,
        private readonly string $departureIcao,
        private readonly ?string $departureTime,
        private readonly string $flightNumber,
    )
    {
    }

    public function airlineIcao(): string
    {
        return $this->airlineIcao;
    }

    public function arrivalIcao(): string
    {
        return $this->arrivalIcao;
    }

    public function departureIcao(): string
    {
        return $this->departureIcao;
    }

    public function departureTime(): ?string
    {
        return $this->departureTime;
    }

    public function flightNumber(): string
    {
        return $this->flightNumber;
    }
}
