<?php

declare(strict_types=1);

namespace App\Modules\CheckWxApiBundle\Transport;

use GuzzleHttp\Client;
use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\MessageFormatter;
use GuzzleHttp\Middleware;
use GuzzleHttp\RequestOptions;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;

class CheckWxApiClient extends Client
{
    public function __construct(
        string $apiUrl,
        string $apiKey,
        callable $defaultHandler = null,
        LoggerInterface $logger = null
    )
    {
        $stack = HandlerStack::create($defaultHandler ?: null);
        $stack->push(Middleware::log($logger ?? new NullLogger(), new MessageFormatter(MessageFormatter::DEBUG)));

        parent::__construct([
            'base_uri' => $apiUrl,
            'handler' => $stack,
            RequestOptions::HEADERS => [
                'X-API-Key' => $apiKey
            ],
            RequestOptions::COOKIES => new CookieJar()
        ]);
    }
}
