<?php

declare(strict_types=1);

namespace App\Modules\CheckWxApiBundle\Service;

use App\Modules\CheckWxApiBundle\Interfaces\CheckWxServiceInterface;
use App\Modules\CheckWxApiBundle\Transport\CheckWxApiClient;
use Symfony\Contracts\Cache\CacheInterface;
use Symfony\Contracts\Cache\ItemInterface;

class CheckWxService implements CheckWxServiceInterface
{
    public function __construct(private readonly CheckWxApiClient $client, private readonly CacheInterface $cache)
    {
    }

    public function getWeatherByIcaoList(array $icaoList): array
    {
        return $this->cache->get(self::cacheKey($icaoList), function (ItemInterface $item) use ($icaoList) {
            $item->expiresAfter(600);
            $data = json_decode((string)$this->client->get('metar/' . implode(',', $icaoList))->getBody(), true);
            $result = [];

            foreach ($data['data'] as $key => $datum) {
                $result[$icaoList[$key]] = $datum;
            }

            return $result;
        });
    }

    private static function cacheKey(array $icaoList): string
    {
        return 'check_wx_api_'.md5(implode(',',$icaoList));
    }
}
