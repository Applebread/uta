<?php

namespace App\Modules\CheckWxApiBundle\Interfaces;

interface CheckWxServiceInterface
{
    public function getWeatherByIcaoList(array $icaoList): array;
}
