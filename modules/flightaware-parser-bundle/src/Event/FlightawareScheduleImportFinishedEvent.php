<?php

declare(strict_types=1);

namespace App\Modules\FlightawareParserBundle\Event;

use App\Modules\FlightawareParserBundle\Value\FlightawareFlightsCollection;

final class FlightawareScheduleImportFinishedEvent
{
    public function __construct(private readonly FlightawareFlightsCollection $scheduleCollection)
    {
    }

    public function getScheduleCollection(): FlightawareFlightsCollection
    {
        return $this->scheduleCollection;
    }
}
