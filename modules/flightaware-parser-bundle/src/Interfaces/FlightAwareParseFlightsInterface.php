<?php

namespace App\Modules\FlightawareParserBundle\Interfaces;

use App\Modules\FlightawareParserBundle\Value\FlightawareFlightsCollection;

interface FlightAwareParseFlightsInterface
{
    public function parseFlights(string $airlineIcao): FlightawareFlightsCollection;
}
