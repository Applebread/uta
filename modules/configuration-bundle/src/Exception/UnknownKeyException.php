<?php

namespace App\Modules\ConfigurationBundle\Exception;

use OutOfBoundsException;

class UnknownKeyException extends OutOfBoundsException
{
    public static function fromKey(string $key): void
    {
        throw new self("Unknown setting key '$key'");
    }
}
