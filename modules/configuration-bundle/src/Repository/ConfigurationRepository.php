<?php

declare(strict_types=1);

namespace App\Modules\ConfigurationBundle\Repository;

use App\Modules\ConfigurationBundle\Entity\Configuration;
use App\Modules\ConfigurationBundle\Exception\UnknownKeyException;
use App\Modules\ConfigurationBundle\Value\ConfigurationValuesCollection;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class ConfigurationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Configuration::class);
    }

    public function getByKey(string $key): Configuration
    {
        if (!$res = $this->findOneBy(['key' => $key])) {
            UnknownKeyException::fromKey($key);
        }

        return $res;
    }

    public function all(): ConfigurationValuesCollection
    {
        return new ConfigurationValuesCollection(...$this->findAll());
    }

    public function updateByKey(string $key, string $label, string $value): void
    {
        $this->_em->persist($this->getByKey($key)->update($label, $value));
        $this->_em->flush();
    }

    public function save(Configuration $configuration): void
    {
        $this->_em->persist($configuration);
        $this->_em->flush();
    }
}
