<?php

namespace App\Modules\ConfigurationBundle\Dto;

use App\Modules\ConfigurationBundle\Entity\Configuration;
use Symfony\Component\HttpFoundation\Request;

class ConfigurationValueDto
{
    public function __construct(
        public readonly string $key,
        public readonly string $label,
        public readonly string $value
    )
    {
    }

    public static function fromEntity(Configuration $entity): self
    {
        return new self($entity->getKey(), $entity->getLabel(), $entity->getValue());
    }
}
