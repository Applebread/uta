<?php

declare(strict_types=1);

namespace App\Modules\InfogateParserBundle\Value\Schedule;

use Generator;
use IteratorAggregate;
use Countable;

final class ScheduleCollection implements IteratorAggregate, Countable
{
    private array $scheduleItems;

    public function __construct(ScheduleItem ...$scheduleItems)
    {
        $this->scheduleItems = $scheduleItems;
        $this->orderByFlightNumber();
    }

    public function orderByFlightNumber(): void
    {
        usort($this->scheduleItems, function (ScheduleItem $a, ScheduleItem $b) {
            return (int) $a->flightNumber() > (int) $b->flightNumber() ? 1 : -1;
        });
    }

    public function add(ScheduleItem $scheduleItem) {
        $this->scheduleItems[] = $scheduleItem;
    }

    public function getIterator(): Generator
    {
        foreach ($this->scheduleItems as $scheduleItem) {
            yield $scheduleItem;
        }
    }

    public function clearAllThereFlightNumberNotContains(string $string): self
    {
        foreach ($this->scheduleItems as $key => $scheduleItem) {
            if (str_contains($scheduleItem->flightNumber(), $string)) {
                continue;
            }

            unset($this->scheduleItems[$key]);
        }

        return $this;
    }

    public function equalsWith(ScheduleCollection $scheduleCollection): bool
    {
        $diff = [];

        foreach ($this->scheduleItems as $scheduleItem) {
            $sameItemsInThatCollection = array_filter(iterator_to_array($scheduleCollection->getIterator()), function (ScheduleItem $comparedScheduleItem) use ($scheduleItem) {
                return $comparedScheduleItem->flightNumber() === $scheduleItem->flightNumber() &&
                    $comparedScheduleItem->from() === $scheduleItem->from() &&
                    $comparedScheduleItem->daysInWeak() === $scheduleItem->daysInWeak() &&
                    $comparedScheduleItem->aircraftType() === $scheduleItem->aircraftType();
            });

            if (!count($sameItemsInThatCollection)) {
                $diff[] = $scheduleItem;
            }
        }

        return !count($diff);
    }

    public function mergeWith(ScheduleCollection $scheduleCollection): void
    {
        $this->scheduleItems = [...$this->scheduleItems, ...iterator_to_array($scheduleCollection->getIterator())];
    }

    public function filter(callable $callable): self
    {
        $items = array_filter($this->scheduleItems, $callable);

        return new self(...$items);
    }

    public static function createFromSelf(ScheduleCollection $scheduleCollection): self
    {
        return new self(...iterator_to_array($scheduleCollection->getIterator()));
    }

    public function count(): int
    {
        return count($this->scheduleItems);
    }
}
