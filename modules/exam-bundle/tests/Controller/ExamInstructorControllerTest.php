<?php

declare(strict_types=1);

namespace App\Modules\ExamBundle\Tests\Controller;

use App\Modules\ExamBundle\Entity\TrainingExam;
use App\Modules\ExamBundle\Interfaces\Repository\ExamRepositoryInterface;
use App\Modules\PilotBundle\Repository\PilotRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class ExamInstructorControllerTest extends KernelTestCase
{
    private EntityManagerInterface $entityManager;
    private ExamRepositoryInterface $examRepo;
    private PilotRepository $pilotRepo;

    protected function setUp(): void
    {
        self::bootKernel();
        parent::setUp();

        $this->entityManager = static::getContainer()->get(EntityManagerInterface::class);
        $this->examRepo = static::getContainer()->get(ExamRepositoryInterface::class);
        $this->pilotRepo = static::getContainer()->get(PilotRepository::class);

        $this->entityManager->getConnection()->executeQuery('DELETE FROM exam');
    }

    public function testTrainingList(): void
    {
        $pilots = $this->pilotRepo->getPilotsList(1, 1);
        $pilot = $this->pilotRepo->find($pilots->current()->id);

        $newExam = new TrainingExam($pilot, new \DateTimeImmutable('today'), '');
        $this->examRepo->save($newExam);

        $exams = $this->examRepo->getTrainingWaitingList();

        $this->assertEquals(1, count(iterator_to_array($exams)));
    }
}
