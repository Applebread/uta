<?php

namespace App\Modules\ExamBundle\Interfaces\Service;

use App\Modules\ExamBundle\Dto\QuestionDto;
use App\Modules\ExamBundle\Entity\QuestionBank\BankQuestion;

interface BankQuestionCrudServiceInterface
{
    public function createOrUpdateFromDto(QuestionDto $questionDto): BankQuestion;
}
