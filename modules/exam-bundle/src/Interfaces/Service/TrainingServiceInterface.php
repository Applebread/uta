<?php

namespace App\Modules\ExamBundle\Interfaces\Service;

use App\Modules\ExamBundle\Dto\WaitingTrainingListDto;
use App\Modules\ExamBundle\Entity\TrainingExam;
use App\Modules\PilotBundle\Entity\Pilot;

interface TrainingServiceInterface
{
    public function markTrainingAsPassed(TrainingExam $exam, int $resultPercent): void;

    public function startTraining(TrainingExam $exam): void;

    public function stopTraining(TrainingExam $exam): void;

    public function getWaitingTrainingList(): WaitingTrainingListDto;

    public function assignInstructorToTraining(TrainingExam $exam): void;
}
