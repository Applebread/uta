<?php

namespace App\Modules\ExamBundle\Interfaces\Service;

use App\Modules\ExamBundle\Entity\Exam;
use App\Modules\ExamBundle\Value\ExamResult;
use App\Modules\ExamBundle\Value\PilotExamRequest;

interface ExamServiceInterface
{
    public function request(PilotExamRequest $request): Exam;

    public function getExamDetails(int $id): Exam;

    public function markQuestionAnswers(int $examId, int $questionId, array $answerIds): void;

    public function checkExam(int $examId): ExamResult;

    public function reassignFailedExam(int $examId): void;
}
