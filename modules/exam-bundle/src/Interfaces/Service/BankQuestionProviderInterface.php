<?php

namespace App\Modules\ExamBundle\Interfaces\Service;

use App\Modules\ExamBundle\Dto\QuestionsCollectionDto;

interface BankQuestionProviderInterface
{
    public function provideByType(string $type, int $page, int $pageSize, ?int $ratingId = null): QuestionsCollectionDto;

    public function provideExistentTypes(): array;
}
