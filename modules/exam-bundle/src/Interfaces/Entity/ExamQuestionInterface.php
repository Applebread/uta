<?php

namespace App\Modules\ExamBundle\Interfaces\Entity;

use IteratorAggregate;

interface ExamQuestionInterface
{
    public function question(): string;

    public function options(): IteratorAggregate;
}
