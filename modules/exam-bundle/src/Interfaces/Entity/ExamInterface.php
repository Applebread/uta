<?php

namespace App\Modules\ExamBundle\Interfaces\Entity;

use DateTimeImmutable;

interface ExamInterface
{
    const EXAM_TYPE_TYPE_RATING = 'type_rating';
    const EXAM_TYPE_MINIMUM = 'minimum';
    const EXAM_TYPE_COMMON = 'common';
    const EXAM_TYPE_TRAINING = 'training';

    public function id(): int;

    public function userId(): int;

    public function type(): ?string;

    public function isPassed(): bool;

    public function resultPercent(): ?int;

    public function markAsPassed(DateTimeImmutable $passedAt): void;
}
