<?php

namespace App\Modules\ExamBundle\Interfaces\Entity;

interface ExamQuestionOptionInterface
{
    public function description(): string;

    public function isCorrect(): bool;
}
