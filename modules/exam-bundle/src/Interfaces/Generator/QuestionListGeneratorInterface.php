<?php

namespace App\Modules\ExamBundle\Interfaces\Generator;

use App\Modules\ExamBundle\Value\QuestionCollection;

interface QuestionListGeneratorInterface
{
    public function generateQuestions(string $examType): QuestionCollection;
}
