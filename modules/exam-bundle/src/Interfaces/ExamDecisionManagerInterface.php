<?php

namespace App\Modules\ExamBundle\Interfaces;

use App\Modules\ExamBundle\Interfaces\Entity\ExamInterface;

interface ExamDecisionManagerInterface
{
    public function makeDecision(ExamInterface $exam): bool;
}
