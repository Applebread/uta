<?php

namespace App\Modules\ExamBundle\Interfaces\Repository;

use App\Modules\ExamBundle\Entity\QuestionBank\BankQuestionOption;

interface BankQuestionOptionRepositoryInterface
{
    public function findById(int $id): ?BankQuestionOption;
}
