<?php

namespace App\Modules\ExamBundle\Interfaces\Repository;

use App\Modules\ExamBundle\Dto\QuestionsCollectionDto;
use App\Modules\ExamBundle\Entity\QuestionBank\BankQuestion;

interface BankQuestionRepositoryInterface
{
    public function getById(int $id): BankQuestion;

    public function findRandomBankQuestions(string $type, int $limit): array;

    public function save(BankQuestion $question): void;
}
