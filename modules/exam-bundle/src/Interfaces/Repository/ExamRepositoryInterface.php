<?php

namespace App\Modules\ExamBundle\Interfaces\Repository;

use App\Modules\ExamBundle\Dto\UserExamCollectionDto;
use App\Modules\ExamBundle\Dto\WaitingTrainingListDto;
use App\Modules\ExamBundle\Interfaces\Entity\ExamInterface;
use App\Modules\UserBundle\Entity\User;

interface ExamRepositoryInterface
{
    public function getById(int $examId): ExamInterface;

    public function getUserExamList(User $user, ?int $page, ?int $pageSize): UserExamCollectionDto;

    public function save(ExamInterface $exam): ExamInterface;
}
