<?php

namespace App\Modules\ExamBundle\Interfaces\Voter;

use App\Modules\ExamBundle\Interfaces\Entity\ExamInterface;

interface ExamVoterInterface
{
    public function availableForExamTypes(): array;

    public function vote(ExamInterface $exam): bool;
}
