<?php

declare(strict_types=1);

namespace App\Modules\ExamBundle\Repository;

use App\Modules\ExamBundle\Entity\QuestionBank\BankQuestionOption;
use App\Modules\ExamBundle\Interfaces\Repository\BankQuestionOptionRepositoryInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class BankQuestionOptionRepository extends ServiceEntityRepository implements BankQuestionOptionRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, BankQuestionOption::class);
    }

    public function findById(int $id): ?BankQuestionOption
    {
        return $this->find($id);
    }
}
