<?php

namespace App\Modules\ExamBundle\Repository;

use App\Modules\ExamBundle\Dto\TrainingExamDto;
use App\Modules\ExamBundle\Dto\WaitingTrainingListDto;
use App\Modules\ExamBundle\Entity\TrainingExam;
use App\Modules\ExamBundle\Interfaces\Entity\ExamInterface;
use App\Modules\UserBundle\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class TrainingRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TrainingExam::class);
    }

    public function getTrainingWaitingList(?int $page = 1, ?int $pageSize = 15): WaitingTrainingListDto
    {
        $exams = $this->findBy([], ['id' => 'DESC'], $pageSize, ($page - 1) * $pageSize);
        $totalCount = $this->count([]);

        $examsList = [];

        foreach ($exams as $exam) {
            if (!$exam->is(ExamInterface::EXAM_TYPE_TRAINING)) {
                continue;
            }

            $examsList[] = TrainingExamDto::fromEntity($exam);
        }

        // TODO: What is fucking this?. Return's DTO from repo - very bad idea.
        return new WaitingTrainingListDto($totalCount, ...$examsList);
    }

    public function getTrainingWithAssignedInstructor(User $instructor): ?TrainingExam
    {
        return $this->findOneBy(['instructor' => $instructor, 'status' => TrainingExam::TRAINING_STATUS_STARTED], ['id' => 'DESC']);
    }
}
