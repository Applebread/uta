<?php

declare(strict_types=1);

namespace App\Modules\ExamBundle\Repository;

use App\Modules\ExamBundle\Dto\QuestionDto;
use App\Modules\ExamBundle\Dto\QuestionsCollectionDto;
use App\Modules\ExamBundle\Entity\QuestionBank\BankQuestion;
use App\Modules\ExamBundle\Interfaces\Repository\BankQuestionRepositoryInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class BankQuestionRepository extends ServiceEntityRepository implements BankQuestionRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, BankQuestion::class);
    }

    public function getById(int $id): BankQuestion
    {
        if (!$question = $this->find($id)) {
            throw new \InvalidArgumentException("Question with id $id not exist");
        }

        return $question;
    }

    public function save(BankQuestion $question): void
    {
        $this->_em->persist($question);
        $this->_em->flush();
    }

    public function findRandomBankQuestions(string $type, int $limit): array
    {
        $qb = $this->createQueryBuilder('bq');

        return $qb->select()
            ->where('bq.type=:type')
            ->orderBy('RANDOM()')
            ->setParameter('type', $type)
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult();
    }
}
