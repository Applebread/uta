<?php

declare(strict_types=1);

namespace App\Modules\ExamBundle\Repository;

use App\Modules\ExamBundle\Dto\ShortExamDto;
use App\Modules\ExamBundle\Dto\TrainingExamDto;
use App\Modules\ExamBundle\Dto\UserExamCollectionDto;
use App\Modules\ExamBundle\Dto\WaitingTrainingListDto;
use App\Modules\ExamBundle\Entity\Exam;
use App\Modules\ExamBundle\Interfaces\Entity\ExamInterface;
use App\Modules\ExamBundle\Interfaces\Repository\ExamRepositoryInterface;
use App\Modules\UserBundle\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use InvalidArgumentException;

class ExamRepository extends ServiceEntityRepository implements ExamRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Exam::class);
    }

    public function getById(int $examId): ExamInterface
    {
        if (!$exam = $this->find($examId)) {
            throw new InvalidArgumentException("Unknown exam with ID: $examId");
        }

        return $exam;
    }

    public function save(ExamInterface $exam): ExamInterface
    {
        $this->_em->persist($exam);
        $this->_em->flush();

        return $exam;
    }

    public function delete(ExamInterface $exam): void
    {
        $this->_em->remove($exam);
        $this->_em->flush();
    }

    public function getUserExamList(User $user, ?int $page = 1, ?int $pageSize = 15): UserExamCollectionDto
    {
        $exams = $this->findBy(['user' => $user], ['id' => 'DESC'], $pageSize, ($page - 1) * $pageSize);
        $totalCount = $this->count(['user' => $user]);

        $examsList = [];

        foreach ($exams as $exam) {
            $examsList[] = ShortExamDto::fromEntity($exam);
        }

        return new UserExamCollectionDto($totalCount, ...$examsList);
    }
}
