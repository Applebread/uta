<?php

declare(strict_types=1);

namespace App\Modules\ExamBundle\Voter;

use App\Modules\ExamBundle\Entity\TrainingExam;
use App\Modules\ExamBundle\Interfaces\Entity\ExamInterface;
use App\Modules\ExamBundle\Interfaces\Voter\ExamVoterInterface;

// "Training passed" flag should be marked by pilot-instructor manually, then voter will vote for training.
class TrainingExamVoter implements ExamVoterInterface
{
    /**
     * @param TrainingExam $exam
     */
    public function vote(ExamInterface $exam): bool
    {
        return $exam->isTrainingPassed();
    }

    // Specially hardcoded to assign this voter only for TrainingExam.
    public function availableForExamTypes(): array
    {
        return [ExamInterface::EXAM_TYPE_TRAINING];
    }
}
