<?php

namespace App\Modules\ExamBundle\Voter;

use App\Modules\ExamBundle\Interfaces\Entity\ExamInterface;
use App\Modules\ExamBundle\Interfaces\Voter\ExamVoterInterface;

class BaseExamVoter implements ExamVoterInterface
{
    private array $types;

    public function __construct(private readonly int $percentToPass, string ...$types)
    {
        $this->types = $types;
    }

    public function availableForExamTypes(): array
    {
        return $this->types;
    }

    public function vote(ExamInterface $exam): bool
    {
        return $exam->resultPercent() >= $this->percentToPass;
    }
}
