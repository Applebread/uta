<?php

declare(strict_types=1);

namespace App\Modules\ExamBundle\Value;

use App\Modules\ExamBundle\Entity\ExamQuestion;
use Iterator;

final class QuestionCollection implements Iterator
{
    /** @var ExamQuestion[] $questions */
    private array $questions;

    public function __construct(ExamQuestion ...$questions)
    {
        $this->questions = $questions;
    }

    public function current(): ?ExamQuestion
    {
        $current = current($this->questions);

        return $current === false ? null : $current;
    }

    public function next(): void
    {
        next($this->questions);
    }

    public function key(): ?int
    {
        return key($this->questions);
    }

    public function valid(): bool
    {
        return (bool) current($this->questions);
    }

    public function rewind(): void
    {
        reset($this->questions);
    }
}
