<?php

namespace App\Modules\ExamBundle\Value;

final class PilotExamRequest
{
    public function __construct(
        private int    $pilotId,
        private string $examType,
        private ?int   $typeRatingId = null,
        private ?int   $minimumId = null,
    )
    {
    }

    public function getPilotId(): int
    {
        return $this->pilotId;
    }

    public function getExamType(): string
    {
        return $this->examType;
    }

    public function getTypeRatingId(): ?int
    {
        return $this->typeRatingId;
    }

    public function getMinimumId(): ?int
    {
        return $this->minimumId;
    }
}
