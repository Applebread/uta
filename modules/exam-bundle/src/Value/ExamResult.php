<?php

namespace App\Modules\ExamBundle\Value;

final class ExamResult
{
    public function __construct(
        private readonly bool   $isSuccess,
        private readonly int    $resultPercent,
    )
    {
    }

    public function isSuccess(): bool
    {
        return $this->isSuccess;
    }

    public function getResultPercent(): int
    {
        return $this->resultPercent;
    }
}
