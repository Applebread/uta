<?php

declare(strict_types=1);

namespace App\Modules\ExamBundle\EventListener;

use App\Modules\ExamBundle\Entity\TrainingExam;
use App\Modules\ExamBundle\Event\ExamPassedEvent;
use App\Modules\ExamBundle\Interfaces\Entity\ExamInterface;
use App\Modules\ExamBundle\Interfaces\Repository\ExamRepositoryInterface;
use DateTimeImmutable;
use Symfony\Contracts\Translation\TranslatorInterface;

class ExamPassedEventListener
{
    public function __construct(
        private ExamRepositoryInterface $examRepository,
        private TranslatorInterface $translator
    )
    {
    }

    public function __invoke(ExamPassedEvent $event): void
    {
        if (!$exam = $this->examRepository->getById($event->examId())) {
            return;
        }

        if (!$event->isSuccess()) {
            return;
        }

        if ($exam->is(ExamInterface::EXAM_TYPE_TYPE_RATING)) {
            $this->examRepository->save(
                new TrainingExam(
                    $exam->user(),
                    new DateTimeImmutable(),
                    $this->translator->trans('TRAINING_EXAM_FOR_TYPE_RATING') . ': '.$exam->typeRating()->getName(),
                    $exam->typeRating()
                ),
            );
        }
    }
}
