<?php

declare(strict_types=1);

namespace App\Modules\ExamBundle\EventListener;

use App\Modules\ExamBundle\Entity\CommonExam;
use App\Modules\ExamBundle\Entity\Exam;
use App\Modules\ExamBundle\Interfaces\Generator\QuestionListGeneratorInterface;
use App\Modules\ExamBundle\Interfaces\Repository\ExamRepositoryInterface;
use App\Modules\UserBundle\Event\NewUserRegisteredEvent;
use App\Modules\UserBundle\Repository\UserRepository;
use DateTimeImmutable;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Contracts\Translation\TranslatorInterface;

final class NewRegisteredEventListener
{
    const NEW_USER_EXAM_DESCRIPTION = 'NEW_USER_EXAM_DESCRIPTION';

    public function __construct(
        private readonly ExamRepositoryInterface $examRepository,
        private readonly QuestionListGeneratorInterface $randomGenerator,
        private readonly UserRepository $userRepository,
        private readonly TranslatorInterface $translator
    )
    {
    }

    public function __invoke(NewUserRegisteredEvent $event): void
    {
        $user = $this->userRepository->find($event->userId());
        $randomQuestions = $this->randomGenerator->generateQuestions(Exam::EXAM_TYPE_COMMON);
        $exam = new CommonExam($user, new DateTimeImmutable('today'), $this->translator->trans(self::NEW_USER_EXAM_DESCRIPTION), new ArrayCollection(iterator_to_array($randomQuestions)));
        $this->examRepository->save($exam);
    }
}
