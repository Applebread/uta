<?php

declare(strict_types=1);

namespace App\Modules\ExamBundle\Entity;

use App\Modules\ExamBundle\Interfaces\Entity\ExamQuestionInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
class ExamQuestion implements ExamQuestionInterface
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private int $id;

    #[ORM\ManyToOne(targetEntity: Exam::class, cascade: ['remove', 'persist'], inversedBy: 'questions')]
    private Exam $exam;

    #[ORM\Column(type: 'text')]
    private readonly string $question;

    #[ORM\OneToMany(mappedBy: 'question', targetEntity: ExamQuestionOption::class, cascade: ['remove', 'persist'], orphanRemoval: true)]
    #[ORM\OrderBy(['id' => 'DESC'])]
    private Collection $options;

    public function __construct(string $question, Collection $options)
    {
        $this->question =$question;

        $this->options = new ArrayCollection();
        foreach ($options as $option) {
            $this->addOption($option);
        }
    }

    public function answeredCorrect(): bool
    {
        /** @var ExamQuestionOption $option */
        foreach ($this->options as $option) {
            if (($option->isCorrect() && !$option->isAnswer()) || (!$option->isCorrect() && $option->isAnswer())) {
                return false;
            }
        }

        return true;
    }

    public function removeAnswers(): void
    {
        /** @var ExamQuestionOption $option */
        foreach ($this->options() as $option) {
            $option->markIsNotAnswer();
        }
    }

    public function selectAnswers(...$answerIds): void
    {
        $this->removeAnswers();

        /** @var ExamQuestionOption $option */
        foreach ($this->options() as $option) {
            if (in_array($option->id(), $answerIds)) {
                $option->markAnswer();
            }
        }
    }

    /**
     * @return ArrayCollection<ExamQuestionOption>
     */
    public function correctOptions(): ArrayCollection
    {
        return $this->options->filter(function (ExamQuestionOption $option) { return $option->isCorrect(); });
    }

    public function onlyOneAnswerIsPossible(): bool
    {
        return $this->correctOptions()->count() === 1;
    }

    public function question(): string
    {
        return $this->question;
    }

    public function options(): Collection
    {
        return $this->options;
    }

    public function exam(): Exam
    {
        return $this->exam;
    }

    public function setExam(Exam $exam): void
    {
        $this->exam = $exam;
    }

    public function addOption(ExamQuestionOption $option): void
    {
        $option->setQuestion($this);
        $this->options->add($option);
    }

    /**
     * @return int
     */
    public function id(): int
    {
        return $this->id;
    }
}
