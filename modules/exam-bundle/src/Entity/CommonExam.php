<?php

declare(strict_types=1);

namespace App\Modules\ExamBundle\Entity;

use App\Modules\UserBundle\Entity\User;
use DateTimeImmutable;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
class CommonExam extends Exam
{
    public function __construct(User $user, DateTimeImmutable $date, string $description, Collection $questions)
    {
        parent::__construct($user, $date, self::EXAM_TYPE_COMMON, $description, $questions);
    }

    public function type(): string
    {
        return self::EXAM_TYPE_COMMON;
    }
}
