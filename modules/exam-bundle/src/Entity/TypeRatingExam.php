<?php

declare(strict_types=1);

namespace App\Modules\ExamBundle\Entity;

use App\Modules\AircraftBundle\Entity\AircraftType;
use App\Modules\PilotBundle\Entity\TypeRating;
use App\Modules\UserBundle\Entity\User;
use DateTimeImmutable;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
class TypeRatingExam extends Exam
{
    #[ORM\ManyToOne(targetEntity: TypeRating::class, cascade: ['persist'])]
    private TypeRating $typeRating;

    public function __construct(
        User $user,
        DateTimeImmutable $date,
        string $description,
        TypeRating $typeRating,
        Collection $questions
    )
    {
        parent::__construct($user, $date, self::EXAM_TYPE_TYPE_RATING, $description, $questions);
        $this->typeRating = $typeRating;
    }

    public function typeRating(): TypeRating
    {
        return $this->typeRating;
    }

    public function type(): string
    {
        return self::EXAM_TYPE_TYPE_RATING;
    }
}
