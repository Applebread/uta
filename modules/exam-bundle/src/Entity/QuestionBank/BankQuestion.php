<?php

declare(strict_types=1);

namespace App\Modules\ExamBundle\Entity\QuestionBank;

use App\Modules\PilotBundle\Entity\TypeRating;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use InvalidArgumentException;

#[ORM\Entity]
class BankQuestion
{
    const TYPE_MINIMUM = 'minimum';
    const TYPE_TYPE_RATING = 'type_rating';
    const TYPE_COMMON = 'common';

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private int $id;

    #[ORM\Column(type: 'string')]
    private string $type;

    #[ORM\Column(type: 'text')]
    private string $question;

    #[ORM\OneToMany(mappedBy: 'question', targetEntity: BankQuestionOption::class, cascade: ['remove', 'persist'])]
    private Collection $options;

    #[ORM\ManyToOne(targetEntity: TypeRating::class, cascade: ['persist'])]
    private ?TypeRating $typeRating;

    public function __construct(
        string $type,
        string $question,
        array $options,
        ?TypeRating $typeRating = null
    )
    {
        $this->updateValues($type, $question, $options, $typeRating);
    }

    public function updateValues(string $type, string $question, array $options, ?TypeRating $typeRating): void
    {
        if (!in_array($type, [self::TYPE_COMMON, self::TYPE_MINIMUM, self::TYPE_TYPE_RATING])) {
            throw new InvalidArgumentException("Unknown question type");
        }

        $this->type = $type;
        $this->question = $question;
        $this->options = new ArrayCollection();

        foreach ($options as $option) {
            $this->addOption($option);
        }

        if ($typeRating) {
            $this->updateTypeRating($typeRating);
        }
    }

    public function id(): int
    {
        return $this->id;
    }

    public function updateTypeRating(TypeRating $typeRating): void
    {
        if ($this->type !== self::TYPE_TYPE_RATING) {
            throw new InvalidArgumentException("Question is not for type rating. Unable to set typeRating");
        }

        $this->typeRating = $typeRating;
    }

    public function typeRating(): ?TypeRating
    {
        if ($this->type !== self::TYPE_TYPE_RATING) {
            return null;
        }

        return $this->typeRating;
    }

    public function question(): string
    {
        return $this->question;
    }

    public function options(): Collection
    {
        return $this->options;
    }

    public function type(): string
    {
        return $this->type;
    }

    public function addOption(BankQuestionOption $option): void
    {
        $option->setQuestion($this);
        $this->options->add($option);
    }
}
