<?php

declare(strict_types=1);

namespace App\Modules\ExamBundle\Entity\QuestionBank;

use Doctrine\ORM\Mapping as ORM;


#[ORM\Entity]
class BankQuestionOption
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private int $id;

    #[ORM\Column(type: 'text')]
    private string $text;

    #[ORM\Column(type: 'boolean')]
    private bool $isCorrect;

    #[ORM\ManyToOne(targetEntity: BankQuestion::class)]
    private BankQuestion $question;

    public function __construct(string $text, bool $isCorrect = false)
    {
        $this->updateValues($text, $isCorrect);
    }

    public function updateValues(string $text, bool $isCorrect): void
    {
        $this->text = $text;
        $this->isCorrect = $isCorrect;
    }

    public function id(): int
    {
        return $this->id;
    }

    public function text(): string
    {
        return $this->text;
    }

    public function isCorrect(): bool
    {
        return $this->isCorrect;
    }

    public function question(): BankQuestion
    {
        return $this->question;
    }

    public function setQuestion(BankQuestion $question): void
    {
        $this->question = $question;
    }
}
