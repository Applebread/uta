<?php

declare(strict_types=1);

namespace App\Modules\ExamBundle\Entity;

use App\Modules\ExamBundle\Interfaces\Entity\ExamQuestionOptionInterface;
use Doctrine\ORM\Mapping as ORM;
use function Symfony\Component\Translation\t;

#[ORM\Entity]
class ExamQuestionOption implements ExamQuestionOptionInterface
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private int $id;

    #[ORM\ManyToOne(cascade: ['remove', 'persist'], inversedBy: 'options')]
    private ExamQuestion $question;

    #[ORM\Column(type: 'boolean')]
    private bool $answer = false;

    public function __construct(
        #[ORM\Column(type: 'string')]
        private readonly string $description,
        #[ORM\Column(type: 'boolean')]
        private readonly bool $isCorrect
    )
    {
    }

    public function description(): string
    {
        return $this->description;
    }

    public function isCorrect(): bool
    {
        return $this->isCorrect;
    }

    public function question(): ExamQuestion
    {
        return $this->question;
    }

    public function setQuestion(ExamQuestion $question): void
    {
        $this->question = $question;
    }

    public function markAnswer(): void
    {
        $this->answer = true;
    }

    public function markIsNotAnswer(): void
    {
        $this->answer = false;
    }

    public function isAnswer(): bool
    {
        return $this->answer;
    }

    public function id(): int
    {
        return $this->id;
    }
}
