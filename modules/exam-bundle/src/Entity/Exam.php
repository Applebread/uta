<?php

declare(strict_types=1);

namespace App\Modules\ExamBundle\Entity;

use App\Modules\ExamBundle\Interfaces\Entity\ExamInterface;
use App\Modules\ExamBundle\Repository\ExamRepository;
use App\Modules\ExamBundle\Value\ExamResult;
use App\Modules\UserBundle\Entity\User;
use DateTimeImmutable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: ExamRepository::class)]
#[ORM\InheritanceType('SINGLE_TABLE')]
#[ORM\DiscriminatorColumn(name: 'type', type: 'string')]
abstract class Exam implements ExamInterface
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private int $id;

    #[ORM\Column(type: 'text')]
    private string $description = '';

    #[ORM\Column(type: 'datetime_immutable')]
    private DateTimeImmutable $date;

    #[ORM\OneToMany(mappedBy: 'exam',targetEntity: ExamQuestion::class, cascade: ['persist', 'remove'], orphanRemoval: true)]
    private Collection $questions;

    #[ORM\Column(type: 'boolean')]
    private bool $isPassed = false;

    #[ORM\Column(type: 'boolean', nullable: true)]
    private ?bool $isSuccess = null;

    #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    private ?DateTimeImmutable $passedAt = null;

    #[ORM\Column(type: 'integer', nullable: true)]
    #[Assert\Range(min: 0, max: 100)]
    protected ?int $resultPercent = null;

    #[ORM\ManyToOne(targetEntity: User::class)]
    private User $user;

    public function __construct(
        User $user,
        DateTimeImmutable $date,
        string $type,
        string $description = '',
        Collection $questions = new ArrayCollection()
    )
    {
        $this->user = $user;
        $this->date = $date;
        $this->description = $description;
        $this->type = $type;
        $this->questions = new ArrayCollection();

        foreach ($questions as $question) {
            $this->addQuestion($question);
        }
    }

    public function id(): int
    {
        return $this->id;
    }

    public function userId(): int
    {
        return $this->user->getId();
    }

    public function user(): User
    {
        return $this->user;
    }

    public function userName(): string
    {
        return $this->user->getFullName();
    }

    abstract public function type(): string;

    public function is(string $type): bool
    {
        return $this->type() === $type;
    }

    public function isPassed(): bool
    {
        return $this->isPassed;
    }

    public function resultPercent(): ?int
    {
        return $this->resultPercent;
    }

    public function questions(): Collection
    {
        return $this->questions;
    }

    public function addQuestion(ExamQuestion $question): void
    {
        $question->setExam($this);
        $this->questions->add($question);
    }

    public function markAsPassed(DateTimeImmutable $passedAt): void
    {
        $correctAnswersCount = count($this->correctAnsweredQuestions());
        $this->resultPercent = $correctAnswersCount ? ($correctAnswersCount * 100) / $this->questions->count() : 0;
        $this->isPassed = true;
        $this->passedAt = $passedAt;
    }

    public function markSuccess(): void
    {
        $this->isSuccess = true;
    }

    /**
     * @return ExamQuestion[]
     */
    public function correctAnsweredQuestions(): array
    {
        return $this->questions->filter(function (ExamQuestion $question) { return $question->answeredCorrect(); })->toArray();
    }

    public function passedAt(): ?DateTimeImmutable
    {
        return $this->passedAt;
    }

    public function date(): DateTimeImmutable
    {
        return $this->date;
    }

    public function description(): string
    {
        return $this->description;
    }

    public function isSuccess(): bool
    {
        return $this->isSuccess ?? false;
    }

    public function getExamResult(): ?ExamResult
    {
        if (!$this->isPassed || !$this->passedAt) {
            return null;
        }

        return new ExamResult($this->isSuccess ?? false, $this->resultPercent);
    }
}
