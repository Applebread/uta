<?php

declare(strict_types=1);

namespace App\Modules\ExamBundle\Entity;

use App\Modules\PilotBundle\Entity\Minimum;
use App\Modules\UserBundle\Entity\User;
use DateTimeImmutable;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
class MinimumExam extends Exam
{
    private Minimum $minimum;

    public function __construct(
        User $user,
        Minimum $minimum,
        DateTimeImmutable $date,
        string $description,
        Collection $questions)
    {
        parent::__construct($user, $date, self::EXAM_TYPE_MINIMUM, $description, $questions);
        $this->minimum = $minimum;
    }

    public function minimum(): Minimum
    {
        return $this->minimum;
    }

    public function type(): string
    {
        return self::EXAM_TYPE_MINIMUM;
    }
}
