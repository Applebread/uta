<?php

declare(strict_types=1);

namespace App\Modules\ExamBundle\Entity;

use App\Modules\ExamBundle\Exception\TrainingAttemptToAssignNotInstructorException;
use App\Modules\ExamBundle\Exception\TrainingNotAssignedException;
use App\Modules\ExamBundle\Exception\TrainingWrongInstructorException;
use App\Modules\PilotBundle\Entity\Pilot;
use App\Modules\PilotBundle\Entity\TypeRating;
use App\Modules\UserBundle\Entity\User;
use DateTimeImmutable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use InvalidArgumentException;

#[ORM\Entity]
class TrainingExam extends Exam
{
    const TRAINING_STATUS_IDLE = 'idle';
    const TRAINING_STATUS_ASSIGNED = 'assigned';
    const TRAINING_STATUS_STARTED = 'started';
    const TRAINING_STATUS_PASSED = 'passed';

    #[ORM\Column(type: 'string')]
    private string $status = self::TRAINING_STATUS_IDLE;

    #[ORM\ManyToOne(targetEntity: TypeRating::class, cascade: ['persist'])]
    private TypeRating $typeRating;

    #[ORM\ManyToOne(targetEntity: User::class)]
    private ?User $instructor = null;

    public function __construct(User $user, DateTimeImmutable $date, string $description, TypeRating $typeRating)
    {
        $this->typeRating = $typeRating;
        parent::__construct($user, $date, self::EXAM_TYPE_TRAINING, $description, new ArrayCollection());
    }

    public function isTrainingPassed(): bool
    {
        return $this->status === self::TRAINING_STATUS_PASSED;
    }

    public function markTrainingAsAssigned(Pilot $instructor): void
    {
        $this->assertInstructor($instructor);

        if ($this->status !== self::TRAINING_STATUS_IDLE) {
            throw new InvalidArgumentException("Impossible to assign not idle training");
        }

        $this->instructor = $instructor;
        $this->status = self::TRAINING_STATUS_ASSIGNED;
    }

    public function markTrainingAsStarted(Pilot $instructor): void
    {
        $this->assertInstructor($instructor);

        if ($this->instructor->getId() !== $instructor->getId()) {
            TrainingWrongInstructorException::throwFromAction("start");
        }

        if ($this->status !== self::TRAINING_STATUS_ASSIGNED) {
            TrainingNotAssignedException::throwFromAction("start");
        }

        $this->status = self::TRAINING_STATUS_STARTED;
    }

    public function markTrainingAsPassed(Pilot $instructor, int $resultPercent): void
    {
        $this->assertInstructor($instructor);

        if ($this->instructor->getId() !== $instructor->getId()) {
            TrainingWrongInstructorException::throwFromAction("mark passed");
        }

        if ($this->status !== self::TRAINING_STATUS_STARTED) {
            throw new InvalidArgumentException("Impossible to mark not started training as passed");
        }

        $this->status = self::TRAINING_STATUS_PASSED;
        $this->instructor = $instructor;
        $this->markAsPassed(new DateTimeImmutable());
        $this->markSuccess();
        $this->resultPercent = min(100, max(0, $resultPercent));
    }

    public function stopTraining(Pilot $instructor): void
    {
        $this->assertInstructor($instructor);

        if ($this->instructor->getId() !== $instructor->getId()) {
            TrainingWrongInstructorException::throwFromAction("stop");
        }

        if ($this->status !== self::TRAINING_STATUS_STARTED) {
            throw new InvalidArgumentException("Impossible to mark stopped, training is not started yet");

        }

        $this->status = self::TRAINING_STATUS_IDLE;
        $this->instructor = null;
    }

    public function getInstructorName(): ?string
    {
        return $this->instructor?->getFullName();
    }

    public function getInstructorId(): ?int
    {
        return $this->instructor?->getId();
    }

    public function type(): string
    {
        return self::EXAM_TYPE_TRAINING;
    }

    public function typeRating(): TypeRating
    {
        return $this->typeRating;
    }

    private function assertInstructor(Pilot $instructor): void
    {
        if (!$instructor->isRole("ROLE_INSTRUCTOR")) {
            TrainingAttemptToAssignNotInstructorException::throwFromNoRole();
        }
    }

    public function getStatus(): string
    {
        return $this->status;
    }
}
