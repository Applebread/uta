<?php

namespace App\Modules\ExamBundle\Exception;

use InvalidArgumentException;

class TrainingAttemptToAssignNotInstructorException extends InvalidArgumentException
{
    public static function throwFromNoRole()
    {
        throw new self("You have no instructor role");
    }
}
