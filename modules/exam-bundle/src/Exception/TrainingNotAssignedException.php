<?php

namespace App\Modules\ExamBundle\Exception;

use InvalidArgumentException;

class TrainingNotAssignedException extends InvalidArgumentException
{
    public static function throwFromAction(string $action): void
    {
        throw new self("Impossible to $action not assigned training");
    }
}
