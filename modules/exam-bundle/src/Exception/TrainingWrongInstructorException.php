<?php

namespace App\Modules\ExamBundle\Exception;

use InvalidArgumentException;

class TrainingWrongInstructorException extends InvalidArgumentException
{
    public static function throwFromAction(string $action): void
    {
        throw new self("Unable to $action exam which is not assigned to you");
    }
}
