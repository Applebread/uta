<?php

namespace App\Modules\ExamBundle\Dto;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;

final class PilotExamRequestDto
{
    public function __construct(
        #[Assert\NotBlank(message: "Exam type is required")]
        public string $examType,
        public ?int   $typeRatingId = null,
        public ?int   $minimumId = null,
    )
    {
    }

    public static function fromRequest(Request $request): self
    {
        $body = $request->toArray();

        return new self(
            $body['examType'],
            $body['typeRatingId'] ?? null,
            $body['minimumId'] ?? null,
        );
    }
}
