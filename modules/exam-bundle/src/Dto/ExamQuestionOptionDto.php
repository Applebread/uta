<?php

declare(strict_types=1);

namespace App\Modules\ExamBundle\Dto;

use App\Modules\ExamBundle\Entity\ExamQuestionOption;

class ExamQuestionOptionDto
{
    private function __construct(public int $id, public string $text, public bool $checked)
    {
    }

    public static function fromEntity(ExamQuestionOption $option): self
    {
        return new self($option->id(), $option->description(), $option->isAnswer());
    }
}
