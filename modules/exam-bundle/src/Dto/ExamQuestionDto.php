<?php

declare(strict_types=1);

namespace App\Modules\ExamBundle\Dto;

use App\Modules\ExamBundle\Entity\ExamQuestion;
use App\Modules\ExamBundle\Entity\ExamQuestionOption;
use App\Modules\ExamBundle\Entity\TypeRatingExam;

class ExamQuestionDto
{
    private function __construct(
        public readonly int $id,
        public readonly string $question,
        public bool $onlyOneAnswerIsPossible,
        /** @var QuestionOptionDto[] */
        public readonly array $options,
        public readonly ?string $aircraftType = null
    )
    {
    }

    public static function fromEntity(ExamQuestion $question): self
    {
        $exam = $question->exam();

        return new self(
            $question->id(),
            $question->question(),
            $question->onlyOneAnswerIsPossible(),
            array_map(function (ExamQuestionOption $option) {
                return ExamQuestionOptionDto::fromEntity($option);
            }, iterator_to_array($question->options())),
            $exam instanceof TypeRatingExam ? $exam->typeRating()->getName() : null
        );
    }
}
