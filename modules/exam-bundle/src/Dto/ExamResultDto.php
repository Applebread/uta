<?php

namespace App\Modules\ExamBundle\Dto;

use App\Modules\ExamBundle\Value\ExamResult;

final class ExamResultDto
{
    public function __construct(
        public readonly bool $isSuccess,
        public readonly int  $resultPercent
    )
    {
    }

    public static final function fromExamResult(ExamResult $examResult): self
    {
        return new self($examResult->isSuccess(), $examResult->getResultPercent());
    }
}
