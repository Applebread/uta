<?php

namespace App\Modules\ExamBundle\Dto;

use App\Dto\AbstractPaginatedCollection;

final class WaitingTrainingListDto extends AbstractPaginatedCollection
{

    public function __construct(int $totalResults, TrainingExamDto ...$trainingExamsDto)
    {
        $this->items = $trainingExamsDto;
        $this->totalResults = $totalResults;
    }

    public function current(): TrainingExamDto
    {
        return current($this->items);
    }
}
