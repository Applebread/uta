<?php

declare(strict_types=1);

namespace App\Modules\ExamBundle\Dto;

use App\Modules\ExamBundle\Entity\QuestionBank\BankQuestionOption;

class QuestionOptionDto
{
    private function __construct(public ?int $id, public string $text, public bool $isCorrect)
    {
    }

    public static function fromEntity(BankQuestionOption $option): self
    {
        return new self($option->id(), $option->text(), $option->isCorrect());
    }

    public static function fromArray(array $option): self
    {
        return new self($option['id'] ?? null, $option['text'], $option['isCorrect'] ?? null);
    }
}
