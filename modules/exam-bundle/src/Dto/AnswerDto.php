<?php

namespace App\Modules\ExamBundle\Dto;

use Symfony\Component\HttpFoundation\Request;

final class AnswerDto
{
    public function __construct(
        /** @var int[] */
        public array $answerOptionsIds,
    )
    {
    }

    public static function fromRequest(Request $request): self
    {
        $data = json_decode($request->getContent(), true);
        return new self($data['answerOptionsIds']);
    }
}
