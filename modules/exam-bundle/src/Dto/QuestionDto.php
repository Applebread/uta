<?php

declare(strict_types=1);

namespace App\Modules\ExamBundle\Dto;

use App\Modules\ExamBundle\Entity\QuestionBank\BankQuestion;
use App\Modules\ExamBundle\Entity\QuestionBank\BankQuestionOption;
use Symfony\Component\HttpFoundation\Request;

class QuestionDto
{
    private function __construct(
        public readonly ?int $id,
        public readonly string $type,
        public readonly string $question,
        /** @var QuestionOptionDto[] */
        public readonly array $options,
        public readonly ?string $aircraftType = null
    )
    {
    }

    public static function fromEntity(BankQuestion $question): self
    {
        return new self(
            $question->id(),
            $question->type(),
            $question->question(),
            array_map(function (BankQuestionOption $option) {
                return QuestionOptionDto::fromEntity($option);
            }, iterator_to_array($question->options())),
            $question->typeRating()?->getName()
        );
    }

    public static function fromRequest(Request $request, ?int $id = null): self
    {
        $data = json_decode($request->getContent(), true);

        return new self(
            $id,
            $data['type'],
            $data['question'],
            array_map(function (array $option) {
                return QuestionOptionDto::fromArray($option);
            }, $data['options']),
                $data['aircraftType'] ?? null
        );
    }
}
