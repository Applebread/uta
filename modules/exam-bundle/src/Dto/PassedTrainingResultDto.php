<?php

namespace App\Modules\ExamBundle\Dto;

use Symfony\Component\HttpFoundation\Request;

final class PassedTrainingResultDto
{
    public function __construct(public readonly int $resultPercent)
    {
    }

    public static function fromRequest(Request $request):self
    {
        $data = $request->toArray();
        return new self($data['resultPercent']);
    }
}
