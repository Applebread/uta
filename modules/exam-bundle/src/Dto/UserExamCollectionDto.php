<?php

namespace App\Modules\ExamBundle\Dto;

use App\Dto\AbstractPaginatedCollection;

class UserExamCollectionDto extends AbstractPaginatedCollection
{
    public function __construct(int $totalResults, ShortExamDto ...$examDto)
    {
        $this->items = $examDto;
        $this->totalResults = $totalResults;
    }

    public function current(): ShortExamDto
    {
        return current($this->items);
    }
}
