<?php

namespace App\Modules\ExamBundle\Dto;

use App\Modules\ExamBundle\Entity\Exam;
use App\Modules\ExamBundle\Entity\ExamQuestion;
use App\Modules\ExamBundle\Interfaces\Entity\ExamInterface;
use App\Modules\ExamBundle\Value\ExamResult;

final class DetailedExamDto
{
    public function __construct(
        public readonly int $id,
        public readonly string $description,
        public readonly string $type,
        public readonly string $date,
        public readonly array $questions,
        public readonly bool $isPassed,
        public readonly ?string $passedAt = null,
        public readonly ?string $typeRating = null,
        public readonly ?ExamResultDto $examResult = null,
        public readonly ?string $instructorName = null
    )
    {
    }

    public static final function fromEntity(Exam $exam): self
    {
        return new self(
            $exam->id(),
            $exam->description(),
            $exam->type(),
            $exam->date()->format('Y-m-d'),
            array_map(function (ExamQuestion $question) {
                return ExamQuestionDto::fromEntity($question);
            }, $exam->questions()->toArray()),
            $exam->isPassed(),
            $exam->passedAt()?->format('Y-m-d H:i:s'),
            $exam->is(ExamInterface::EXAM_TYPE_TYPE_RATING) ? $exam->typeRating()->getName() : null,
            $exam->getExamResult() ? ExamResultDto::fromExamResult($exam->getExamResult()) : null,
            $exam->is(ExamInterface::EXAM_TYPE_TRAINING) ? $exam->getInstructorName() : null
        );
    }
}
