<?php

namespace App\Modules\ExamBundle\Dto;

use App\Dto\AbstractPaginatedCollection;

final class QuestionsCollectionDto extends AbstractPaginatedCollection
{

    public function __construct(int $totalResults, QuestionDto ...$questiions)
    {
        $this->items = $questiions;
        $this->totalResults = $totalResults;
    }

    public static function createEmpty(): self
    {
        return new self(0);
    }

    public function current(): QuestionDto
    {
        return current($this->items);
    }
}
