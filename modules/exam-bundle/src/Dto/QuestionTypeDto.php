<?php

declare(strict_types=1);

namespace App\Modules\ExamBundle\Dto;

final class QuestionTypeDto
{
    public function __construct(
        public readonly string $type,
        public readonly ?int $ratingId = null,
        public readonly ?string $ratingName = null,
    )
    {
    }
}
