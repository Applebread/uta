<?php

namespace App\Modules\ExamBundle\Dto;

use App\Modules\ExamBundle\Entity\Exam;
use App\Modules\ExamBundle\Entity\TypeRatingExam;
use App\Modules\ExamBundle\Interfaces\Entity\ExamInterface;

class ShortExamDto
{
    public function __construct(
        public readonly int $id,
        public readonly string $description,
        public readonly string $type,
        public readonly string $date,
        public readonly bool $isPassed,
        public readonly ?string $passedAt = null,
        public readonly ?string $aircraftType = null,
        public readonly ?ExamResultDto $examResult = null,
        public readonly ?string $instructorName = null
    )
    {
    }

    public static function fromEntity(Exam $exam): self
    {
        return new self(
            $exam->id(),
            $exam->description(),
            $exam->type(),
            $exam->date()->format('Y-m-d'),
            $exam->isPassed(),
            $exam->passedAt()?->format('Y-m-d H:i:s'),
            $exam->is(ExamInterface::EXAM_TYPE_TYPE_RATING) ? $exam->typeRating()->getName() : null,
            $exam->getExamResult() ? ExamResultDto::fromExamResult($exam->getExamResult()) : null,
            $exam->is(ExamInterface::EXAM_TYPE_TRAINING) ? $exam->getInstructorName() : null
        );
    }
}
