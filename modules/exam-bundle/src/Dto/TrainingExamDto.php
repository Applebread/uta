<?php

declare(strict_types=1);

namespace App\Modules\ExamBundle\Dto;

use App\Modules\ExamBundle\Entity\TrainingExam;

final class TrainingExamDto
{
    private function __construct(
        public readonly string $description,
        public readonly string $rating,
        public readonly string $date,
        public readonly int $examId,
        public readonly int $pilotId,
        public readonly string $pilotName,
        public readonly string $status,
        public readonly ?int $resultPercent,
        public readonly ?string $instructorName = null,
        public readonly ?int $instructorId = null
    ) {}

    public static final function fromEntity(TrainingExam $exam): self
    {
        return new self(
            $exam->description(),
            $exam->typeRating()->getName(),
            $exam->date()->format('Y-m-d H:i:s'),
            $exam->id(),
            $exam->userId(),
            $exam->userName(),
            $exam->getStatus(),
            $exam->resultPercent(),
            $exam->getInstructorName(),
            $exam->getInstructorId()
        );
    }
}
