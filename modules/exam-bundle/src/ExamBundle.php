<?php

declare(strict_types=1);

namespace App\Modules\ExamBundle;

use Exception;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class ExamBundle extends Bundle
{
    public function getContainerExtension()
    {
        return null;
    }

    /**
     * @throws Exception
     */
    public function build(ContainerBuilder $container)
    {
        if ($container->isCompiled()) {
            @trigger_error('Container already compiled. Can not set parameters', E_USER_WARNING);

            return;
        }

        $loader = new YamlFileLoader($container, new FileLocator(__DIR__ . '/Resources/config'));
        $loader->load('parameters.yaml');
        $loader->load('services.yaml');

        parent::build($container);
    }
}
