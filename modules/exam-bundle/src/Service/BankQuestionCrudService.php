<?php

declare(strict_types=1);

namespace App\Modules\ExamBundle\Service;

use App\Modules\AircraftBundle\Repository\AircraftTypeRepository;
use App\Modules\ExamBundle\Dto\QuestionDto;
use App\Modules\ExamBundle\Dto\QuestionOptionDto;
use App\Modules\ExamBundle\Entity\QuestionBank\BankQuestion;
use App\Modules\ExamBundle\Entity\QuestionBank\BankQuestionOption;
use App\Modules\ExamBundle\Interfaces\Repository\BankQuestionOptionRepositoryInterface;
use App\Modules\ExamBundle\Interfaces\Repository\BankQuestionRepositoryInterface;
use App\Modules\ExamBundle\Interfaces\Service\BankQuestionCrudServiceInterface;

class BankQuestionCrudService implements BankQuestionCrudServiceInterface
{
    public function __construct(
        private readonly BankQuestionRepositoryInterface $questionRepository,
        private readonly BankQuestionOptionRepositoryInterface $bankQuestionOptionRepository,
        private readonly AircraftTypeRepository $aircraftTypeRepository
    )
    {
    }

    public function createOrUpdateFromDto(QuestionDto $questionDto, ?int $id = null): BankQuestion
    {
        $aircraftType = $questionDto->aircraftType ?
            $this->aircraftTypeRepository->findByIcao($questionDto->aircraftType) : null;

        $options = array_map(function(QuestionOptionDto $optionDto) {
            if ($optionDto->id) {
                $option = $this->bankQuestionOptionRepository->findById($optionDto->id);
                $option->updateValues($optionDto->text, $optionDto->isCorrect);

                return $option;
            }

            return new BankQuestionOption($optionDto->text, $optionDto->isCorrect);
        }, $questionDto->options);

        if ($id) {
            $question = $this->questionRepository->find($id);
            $question->updateValues(
                $questionDto->type,
                $questionDto->question,
                $options,
                $aircraftType
            );
        } else {
            $question = new BankQuestion(
                $questionDto->type,
                $questionDto->question,
                $options,
                $aircraftType
            );
        }

        $this->questionRepository->save($question);

        return $question;
    }
}
