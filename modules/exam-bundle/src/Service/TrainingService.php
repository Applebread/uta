<?php

declare(strict_types=1);

namespace App\Modules\ExamBundle\Service;

use App\Modules\ExamBundle\Dto\WaitingTrainingListDto;
use App\Modules\ExamBundle\Entity\TrainingExam;
use App\Modules\ExamBundle\Event\ExamPassedEvent;
use App\Modules\ExamBundle\Event\InstructorAssignedToExamEvent;
use App\Modules\ExamBundle\Interfaces\Repository\ExamRepositoryInterface;
use App\Modules\ExamBundle\Interfaces\Service\TrainingServiceInterface;
use App\Modules\ExamBundle\Repository\TrainingRepository;
use App\Modules\PilotBundle\Entity\Pilot;
use App\Modules\PilotBundle\Interfaces\CurrentPilotInterface;
use InvalidArgumentException;
use Psr\EventDispatcher\EventDispatcherInterface;

class TrainingService implements TrainingServiceInterface
{
    private Pilot $currentInstructor;

    public function __construct(
        private readonly ExamRepositoryInterface $examRepository,
        private readonly TrainingRepository $trainingRepository,
        private readonly EventDispatcherInterface $eventDispatcher,
        CurrentPilotInterface $currentPilotService,
    )
    {
        $this->currentInstructor = $currentPilotService->getCurrentPilot();
    }

    public function markTrainingAsPassed(TrainingExam $exam, int $resultPercent = 100): void
    {
        $exam->markTrainingAsPassed($this->currentInstructor, $resultPercent);
        $this->examRepository->save($exam);

        $this->eventDispatcher->dispatch(new ExamPassedEvent($exam->id(), $exam->userId(), true, false));
    }

    public function startTraining(TrainingExam $exam): void
    {
        if ($this->trainingRepository->getTrainingWithAssignedInstructor($this->currentInstructor)) {
            throw new InvalidArgumentException('Impossible to start training. Please stop previous first.');
        }

        $exam->markTrainingAsStarted($this->currentInstructor);
        $this->examRepository->save($exam);
    }

    public function stopTraining(TrainingExam $exam): void
    {
        $exam->stopTraining($this->currentInstructor);
        $this->examRepository->save($exam);
    }

    public function getWaitingTrainingList(): WaitingTrainingListDto
    {
        return $this->trainingRepository->getTrainingWaitingList();
    }

    public function assignInstructorToTraining(TrainingExam $exam): void
    {
        $exam->markTrainingAsAssigned($this->currentInstructor);
        $this->examRepository->save($exam);

        $this->eventDispatcher->dispatch(new InstructorAssignedToExamEvent($exam->id(), $this->currentInstructor->getId()));
    }
}
