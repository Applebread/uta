<?php

declare(strict_types=1);

namespace App\Modules\ExamBundle\Service;

use App\Modules\AircraftBundle\Repository\AircraftTypeRepository;
use App\Modules\ExamBundle\Dto\QuestionDto;
use App\Modules\ExamBundle\Dto\QuestionsCollectionDto;
use App\Modules\ExamBundle\Dto\QuestionTypeDto;
use App\Modules\ExamBundle\Entity\QuestionBank\BankQuestion;
use App\Modules\ExamBundle\Interfaces\Repository\BankQuestionRepositoryInterface;
use App\Modules\ExamBundle\Interfaces\Service\BankQuestionProviderInterface;
use App\Modules\PilotBundle\Repository\TypeRatingRepository;

class BankQuestionProvider implements BankQuestionProviderInterface
{

    public function __construct(
        private readonly BankQuestionRepositoryInterface $bankQuestionRepository,
        private readonly AircraftTypeRepository          $aircraftTypeRepository, private readonly TypeRatingRepository $typeRatingRepository
    )
    {
    }

    public function provideByType(string $type, int $page, int $pageSize, ?int $ratingId = null): QuestionsCollectionDto
    {
        $conditions = ['type' => $type];

        if ($ratingId) {
            if (!$rating = $this->typeRatingRepository->find($ratingId)) {
                return QuestionsCollectionDto::createEmpty();
            }

            $conditions['typeRating'] = $rating;
        }

        $questions = $this->bankQuestionRepository->findBy($conditions, ['id' => 'DESC'], $pageSize, ($page - 1) * $pageSize);
        $totalCount = $this->bankQuestionRepository->count($conditions);

        $questionsList = [];

        foreach ($questions as $question) {
            $questionsList[] = QuestionDto::fromEntity($question);
        }

        return new QuestionsCollectionDto($totalCount, ...$questionsList);
    }

    public function provideExistentTypes(): array
    {
        $existentTypes = [];

        /** @var BankQuestion[] $questions */
        $questions = $this->bankQuestionRepository->findAll();

        foreach ($questions as $question) {
            if ($question->typeRating()) {
                $existentTypes[$question->type().$question->typeRating()->getId()] = new QuestionTypeDto($question->type(), $question->typeRating()->getId(), $question->typeRating()->getName());
            } else {
                $existentTypes[$question->type()] = new QuestionTypeDto($question->type());
            }
        }

        return array_values($existentTypes);
    }
}
