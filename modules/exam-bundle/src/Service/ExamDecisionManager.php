<?php

declare(strict_types=1);

namespace App\Modules\ExamBundle\Service;

use App\Modules\ExamBundle\Interfaces\Entity\ExamInterface;
use App\Modules\ExamBundle\Interfaces\ExamDecisionManagerInterface;
use App\Modules\ExamBundle\Interfaces\Repository\ExamRepositoryInterface;
use App\Modules\ExamBundle\Interfaces\Voter\ExamVoterInterface;
use Psr\EventDispatcher\EventDispatcherInterface;

class ExamDecisionManager implements ExamDecisionManagerInterface
{
    private array $voters;

    public function __construct(iterable $voters)
    {
        $this->setVoters(...$voters);
    }

    private function setVoters(ExamVoterInterface ...$voters) {
        $this->voters = $voters;
    }

    public function makeDecision(ExamInterface $exam): bool
    {
        $availableVoters = array_filter($this->voters, function (ExamVoterInterface $voter) use ($exam) {
            return in_array($exam->type(), $voter->availableForExamTypes());
        });

        foreach ($availableVoters as $voter) {
            if (!$voter->vote($exam)) {
                return false;
            }
        }

        return true;
    }
}
