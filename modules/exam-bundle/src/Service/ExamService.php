<?php

declare(strict_types=1);

namespace App\Modules\ExamBundle\Service;

use App\Modules\ExamBundle\Entity\CommonExam;
use App\Modules\ExamBundle\Entity\Exam;
use App\Modules\ExamBundle\Entity\ExamQuestion;
use App\Modules\ExamBundle\Entity\MinimumExam;
use App\Modules\ExamBundle\Entity\TrainingExam;
use App\Modules\ExamBundle\Entity\TypeRatingExam;
use App\Modules\ExamBundle\Event\ExamPassedEvent;
use App\Modules\ExamBundle\Factory\PilotRequestExamFactory;
use App\Modules\ExamBundle\Interfaces\ExamDecisionManagerInterface;
use App\Modules\ExamBundle\Interfaces\Generator\QuestionListGeneratorInterface;
use App\Modules\ExamBundle\Interfaces\Service\ExamServiceInterface;
use App\Modules\ExamBundle\Repository\ExamRepository;
use App\Modules\ExamBundle\Value\ExamResult;
use App\Modules\ExamBundle\Value\PilotExamRequest;
use DateTimeImmutable;
use Doctrine\Common\Collections\ArrayCollection;
use InvalidArgumentException;
use Psr\EventDispatcher\EventDispatcherInterface;
use RuntimeException;

class ExamService implements ExamServiceInterface
{
    public function __construct(
        private readonly ExamRepository $examRepository,
        private readonly ExamDecisionManagerInterface $decisionManager,
        private readonly EventDispatcherInterface $eventDispatcher,
        private readonly QuestionListGeneratorInterface $questionListGenerator,
        private readonly PilotRequestExamFactory $requestExamFactory
    )
    {
    }

    public function request(PilotExamRequest $request): Exam
    {
        $exam = $this->requestExamFactory->createExamRequestedByPilot($request->getPilotId(), $request->getExamType(), $request->getTypeRatingId(), $request->getMinimumId());
        $this->examRepository->save($exam);

        return $exam;
    }

    public function getExamDetails(int $id): Exam
    {
        return $this->assertExamIsApplicable($id);
    }

    public function markQuestionAnswers(int $examId, int $questionId, array $answerIds): void
    {
        $exam = $this->assertExamIsApplicable($examId);

        /** @var ExamQuestion $question */
        if (!$question = $exam->questions()->filter(function (ExamQuestion $question) use ($questionId) { return $question->id() === $questionId; })->first()) {
            throw new InvalidArgumentException("Wrong question id: $questionId");
        }

        if ($question->onlyOneAnswerIsPossible() && count($answerIds) > 1) {
            throw new InvalidArgumentException("The only one answer is possible");
        }

        $question->selectAnswers(...$answerIds);
        $this->examRepository->save($exam);
    }

    public function checkExam(int $examId): ExamResult
    {
        $exam = $this->assertExamIsApplicable($examId);

        $exam->markAsPassed(new DateTimeImmutable("now"));
        $success = $this->decisionManager->makeDecision($exam);
        if ($success) {
            $exam->markSuccess();
        }

        $this->examRepository->save($exam);
        $this->eventDispatcher->dispatch(new ExamPassedEvent($exam->id(), $exam->userId(), $exam->isSuccess(), $this->examRepository->count(['user' => $exam->user()]) === 1));

        return $exam->getExamResult();
    }

    public function reassignFailedExam(int $examId): void
    {
        $exam = $this->assertExamIsApplicable($examId);
        if ($exam->isSuccess()) {
            throw new InvalidArgumentException("Impossible to reassign successfully passed exam. id: $examId");
        }

        $reassigned = match ($exam->type()) {
            Exam::EXAM_TYPE_COMMON => new CommonExam($exam->user(), new DateTimeImmutable('+1 day'), $exam->description(), new ArrayCollection(iterator_to_array($this->questionListGenerator->generateQuestions($exam->type())))),
            Exam::EXAM_TYPE_TYPE_RATING => new TypeRatingExam($exam->user(), new DateTimeImmutable('+1 day'), $exam->description(), $exam->typeRating(), new ArrayCollection(iterator_to_array($this->questionListGenerator->generateQuestions($exam->type())))),
            Exam::EXAM_TYPE_MINIMUM => new MinimumExam($exam->user(), $exam->minimum(), new DateTimeImmutable('+1 day'), $exam->description(), new ArrayCollection(iterator_to_array($this->questionListGenerator->generateQuestions($exam->type())))),
            Exam::EXAM_TYPE_TRAINING => new TrainingExam($exam->user(), new DateTimeImmutable('+1 day'), $exam->description()),
            default => throw new RuntimeException("Unknown exam type: " . $exam->type()),
        };

        $this->examRepository->save($reassigned);
        $this->examRepository->delete($exam);
    }

    private function assertExamIsApplicable(int $examId): Exam
    {
        if (!$exam = $this->examRepository->find($examId)) {
            throw new InvalidArgumentException("Unknown exam with id: $examId");
        }

        if (new DateTimeImmutable('now') < $exam->date()) {
            throw new InvalidArgumentException("To early for this exam");
        }

        return $exam;
    }
}
