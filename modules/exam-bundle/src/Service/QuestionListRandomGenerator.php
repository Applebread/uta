<?php

declare(strict_types=1);

namespace App\Modules\ExamBundle\Service;

use App\Modules\ExamBundle\Entity\ExamQuestion;
use App\Modules\ExamBundle\Entity\ExamQuestionOption;
use App\Modules\ExamBundle\Entity\QuestionBank\BankQuestion;
use App\Modules\ExamBundle\Entity\QuestionBank\BankQuestionOption;
use App\Modules\ExamBundle\Interfaces\Entity\ExamInterface;
use App\Modules\ExamBundle\Interfaces\Generator\QuestionListGeneratorInterface;
use App\Modules\ExamBundle\Interfaces\Repository\BankQuestionRepositoryInterface;
use App\Modules\ExamBundle\Value\QuestionCollection;
use Doctrine\Common\Collections\ArrayCollection;
use InvalidArgumentException;

class QuestionListRandomGenerator implements QuestionListGeneratorInterface
{
    public function __construct(
        private readonly BankQuestionRepositoryInterface $bankQuestionRepository,
        private readonly int $maxCommonQuestions,
        private readonly int $maxTypeRatingQuestions,
        private readonly int $maxMinimumQuestions
    )
    {
    }

    public function generateQuestions(string $examType): QuestionCollection
    {
       $bankQuestions = $this->bankQuestionRepository->findRandomBankQuestions($examType, $this->getMaxCount($examType));

       return new QuestionCollection(...array_map(function (BankQuestion $bankQuestion) {
           return new ExamQuestion($bankQuestion->question(), new ArrayCollection(array_map(function (BankQuestionOption $bankOption) {
               return new ExamQuestionOption($bankOption->text(), $bankOption->isCorrect());
           }, $bankQuestion->options()->getValues())));
       },  $bankQuestions));
    }

    private function getMaxCount(string $examType): int
    {
        switch ($examType) {
            case ExamInterface::EXAM_TYPE_TYPE_RATING:
                return $this->maxTypeRatingQuestions;
            case ExamInterface::EXAM_TYPE_COMMON:
                return $this->maxCommonQuestions;
            case ExamInterface::EXAM_TYPE_MINIMUM:
                return $this->maxMinimumQuestions;
            case ExamInterface::EXAM_TYPE_TRAINING:
                return 0;
        }

        throw new InvalidArgumentException("Unknown type '$examType' to select max count");
    }
}
