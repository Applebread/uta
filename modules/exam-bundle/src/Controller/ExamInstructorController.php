<?php

declare(strict_types=1);

namespace App\Modules\ExamBundle\Controller;

use App\Modules\ExamBundle\Dto\PassedTrainingResultDto;
use App\Modules\ExamBundle\Entity\TrainingExam;
use App\Modules\ExamBundle\Interfaces\Service\TrainingServiceInterface;
use App\Modules\ExamBundle\Repository\TrainingRepository;
use OpenApi\Attributes\Tag;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Annotations as OA;
use Nelmio\ApiDocBundle\Annotation as NA;

class ExamInstructorController
{

    /** @OA\RequestBody(@OA\JsonContent(ref=@NA\Model(type=PassedTrainingResultDto::class))) */
    #[Tag('ExamInstructor')]
    #[Route(path: '/api/exam/training-passed/{exam}', name: 'mark_training_passed', methods: ['POST'])]
    #[ParamConverter('exam', class: TrainingExam::class)]
    #[IsGranted('ROLE_INSTRUCTOR')]
    public function markTrainingExamAsPassed(Request $request, TrainingExam $exam, TrainingServiceInterface $trainingService): JsonResponse
    {
        $resultPercent = PassedTrainingResultDto::fromRequest($request);
        $trainingService->markTrainingAsPassed($exam, $resultPercent->resultPercent);

        return new JsonResponse(null, Response::HTTP_NO_CONTENT);
    }

    #[Tag('ExamInstructor')]
    #[Route(path: '/api/exam/training-started/{exam}', name: 'mark_training_started', methods: ['POST'])]
    #[ParamConverter('exam', class: TrainingExam::class)]
    #[IsGranted('ROLE_INSTRUCTOR')]
    public function markTrainingStarted(TrainingExam $exam, TrainingServiceInterface $trainingService): JsonResponse
    {
        $trainingService->startTraining($exam);

        return new JsonResponse(null, Response::HTTP_NO_CONTENT);
    }

    #[Tag('ExamInstructor')]
    #[Route(path: '/api/exam/training-stopped/{exam}', name: 'mark_training_stopped', methods: ['POST'])]
    #[ParamConverter('exam', class: TrainingExam::class)]
    #[IsGranted('ROLE_INSTRUCTOR')]
    public function markTrainingStopped(TrainingExam $exam, TrainingServiceInterface $trainingService): JsonResponse
    {
        $trainingService->stopTraining($exam);

        return new JsonResponse(null, Response::HTTP_NO_CONTENT);
    }

    #[Tag('ExamInstructor')]
    #[Route(path: '/api/exam/training-assign/{exam}', name: 'instructor_assign_to_training', methods: ['POST'])]
    #[ParamConverter('exam', class: TrainingExam::class)]
    #[IsGranted('ROLE_INSTRUCTOR')]
    public function assignToTrainingExam(TrainingExam $exam, TrainingServiceInterface $trainingService): JsonResponse
    {
        $trainingService->assignInstructorToTraining($exam);

        return new JsonResponse(null, Response::HTTP_NO_CONTENT);
    }

    #[Tag('ExamInstructor')]
    #[Route(path: '/api/exam/training/waiting', name: 'get_training_waiting_list', methods: ['GET'])]
    #[IsGranted('ROLE_INSTRUCTOR')]
    public function trainingList(Request $request, TrainingRepository $trainingRepository): JsonResponse
    {
        return new JsonResponse($trainingRepository->getTrainingWaitingList(
            (int) $request->get('page', 1),
            (int) $request->get('pageSize', 15)
        ));
    }
}
