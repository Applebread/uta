<?php

declare(strict_types=1);

namespace App\Modules\ExamBundle\Controller;

use App\Modules\ExamBundle\Dto\QuestionDto;
use App\Modules\ExamBundle\Entity\QuestionBank\BankQuestion;
use App\Modules\ExamBundle\Interfaces\Repository\BankQuestionRepositoryInterface;
use App\Modules\ExamBundle\Interfaces\Service\BankQuestionCrudServiceInterface;
use App\Modules\ExamBundle\Interfaces\Service\BankQuestionProviderInterface;
use Nelmio\ApiDocBundle\Annotation as NA;
use OpenApi\Annotations as OA;
use OpenApi\Attributes\Parameter;
use OpenApi\Attributes\Tag;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ExamAdminController
{
    #[Tag('ExamAdmin')]
    #[Route(path: '/api/admin/exam/question-bank/types', methods: ['GET'])]
    #[IsGranted('ROLE_ADMIN')]
    public function getOptionTypes(): JsonResponse
    {
        return new JsonResponse([BankQuestion::TYPE_TYPE_RATING, BankQuestion::TYPE_MINIMUM, BankQuestion::TYPE_COMMON]);
    }

    #[Tag('ExamAdmin')]
    #[Route(path: '/api/admin/exam/question-bank/existent-types', methods: ['GET'])]
    #[IsGranted('ROLE_ADMIN')]
    public function getExistentTypes(BankQuestionProviderInterface $provider): JsonResponse
    {
        return new JsonResponse($provider->provideExistentTypes());
    }

    #[Tag('ExamAdmin')]
    #[Route(path: '/api/admin/exam/question-bank', methods: ['GET'])]
    #[Parameter(name: 'type', in: 'query')]
    #[Parameter(name: 'page', in: 'query')]
    #[Parameter(name: 'page_size', in: 'query')]
    #[Parameter(name: 'ac_type', in: 'query')]
    #[IsGranted('ROLE_ADMIN')]
    public function getQuestionsByTypes(Request $request, BankQuestionProviderInterface $provider): JsonResponse
    {
        return new JsonResponse($provider->provideByType(
            $request->get('type'),
            (int) $request->get('page', 1),
            (int) $request->get('page_size', 15),
            (int) $request->get('ac_type')
        ));
    }

    #[Tag('ExamAdmin')]
    #[Route(path: '/api/admin/exam/question-bank/{id}', methods: ['GET'])]
    #[IsGranted('ROLE_ADMIN')]
    public function getBankQuestionDetails(int $id, BankQuestionRepositoryInterface $bankQuestionRepository): JsonResponse
    {
        return new JsonResponse(QuestionDto::fromEntity($bankQuestionRepository->getById($id)));
    }

    /**
     * @OA\RequestBody(@OA\JsonContent(ref=@NA\Model(type=QuestionDto::class)))
     */
    #[Tag('ExamAdmin')]
    #[Route(path: '/api/admin/exam/question-bank', methods: ['POST'])]
    #[IsGranted('ROLE_ADMIN')]
    public function addQuestion(Request $request, BankQuestionCrudServiceInterface $crudService): JsonResponse
    {
        $question = $crudService->createOrUpdateFromDto(QuestionDto::fromRequest($request));

        return new JsonResponse(QuestionDto::fromEntity($question));
    }

    /**
     * @OA\RequestBody(@OA\JsonContent(ref=@NA\Model(type=QuestionDto::class)))
     */
    #[Tag('ExamAdmin')]
    #[Route(path: '/api/admin/exam/question-bank/{id}', methods: ['PUT'])]
    #[IsGranted('ROLE_ADMIN')]
    public function updateQuestion(int $id, Request $request, BankQuestionCrudServiceInterface $crudService): JsonResponse
    {
        $question = $crudService->createOrUpdateFromDto(QuestionDto::fromRequest($request, $id), $id);

        return new JsonResponse(QuestionDto::fromEntity($question));
    }
}
