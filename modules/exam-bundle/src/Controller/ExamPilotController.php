<?php

declare(strict_types=1);

namespace App\Modules\ExamBundle\Controller;

use App\Modules\ExamBundle\Dto\DetailedExamDto;
use App\Modules\ExamBundle\Dto\ExamResultDto;
use App\Modules\ExamBundle\Dto\PilotExamRequestDto;
use App\Modules\ExamBundle\Entity\Exam;
use App\Modules\ExamBundle\Entity\ExamQuestion;
use App\Modules\ExamBundle\Interfaces\Repository\ExamRepositoryInterface;
use App\Modules\ExamBundle\Interfaces\Service\ExamServiceInterface;
use App\Modules\ExamBundle\Value\PilotExamRequest;
use InvalidArgumentException;
use OpenApi\Annotations as OA;
use Nelmio\ApiDocBundle\Annotation as NA;
use OpenApi\Attributes\Parameter;
use OpenApi\Attributes\Tag;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\Finder\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;
use App\Modules\ExamBundle\Dto\AnswerDto;

class ExamPilotController
{

    #[Tag('ExamPilot')]
    #[Route(path: '/api/exam/{exam}', name: 'get_exam_details', methods: ['GET'])]
    #[ParamConverter('exam', class: Exam::class)]
    #[IsGranted('ROLE_USER')]
    public function getExamDetails(Security $security, Exam $exam, ExamServiceInterface $examService): Response
    {
        if ($security->getUser()->getId() !== $exam->userId()) {
            throw new AccessDeniedException('Forbidden', 403);
        }

        try {
            return new JsonResponse(DetailedExamDto::fromEntity($examService->getExamDetails($exam->id())));
        } catch (InvalidArgumentException $exception) {
            throw new BadRequestHttpException($exception->getMessage(), $exception);
        }
    }

    #[Tag('ExamPilot')]
    #[Route(path: '/api/pilot/exams', name: 'get_exam_list', methods: ['GET'])]
    #[Parameter(name: 'page', in: 'query', required: false)]
    #[Parameter(name: 'page_size', in: 'query', required: false)]
    #[IsGranted('ROLE_USER')]
    public function currentUserExamList(Security $security, ExamRepositoryInterface $examRepository, ?int $page, ?int $pageSize): JsonResponse
    {
        try {
            return new JsonResponse($examRepository->getUserExamList($security->getUser(), $page, $pageSize));
        } catch (InvalidArgumentException $exception) {
            throw new BadRequestHttpException($exception->getMessage(), $exception);
        }
    }

    /** @OA\RequestBody(@OA\JsonContent(ref=@NA\Model(type=AnswerDto::class))) */
    #[Tag('ExamPilot')]
    #[Route(path: '/api/pilot/exams/{question}/answer', name: 'exam_question_answer', methods: ['POST'])]
    #[ParamConverter('question', class: ExamQuestion::class)]
    #[IsGranted('ROLE_USER')]
    public function makeAnswer(Request $request, ExamQuestion $question, ExamServiceInterface $examService): JsonResponse
    {
        try {
            $answer = AnswerDto::fromRequest($request);
            $examService->markQuestionAnswers($question->exam()->id(), $question->id(), $answer->answerOptionsIds);

            return new JsonResponse();
        } catch (InvalidArgumentException $exception) {
            throw new BadRequestHttpException($exception->getMessage(), $exception);
        }
    }

    #[Tag('ExamPilot')]
    #[Route(path: '/api/pilot/exams/{exam}/check', name: 'check_exam', methods: ['POST'])]
    #[ParamConverter('exam', class: Exam::class)]
    #[IsGranted('ROLE_USER')]
    public function checkExam(Exam $exam, ExamServiceInterface $examService): JsonResponse
    {
        try {
            return new JsonResponse(ExamResultDto::fromExamResult($examService->checkExam($exam->id())));
        } catch (InvalidArgumentException $exception) {
            throw new BadRequestHttpException($exception->getMessage(), $exception);
        }
    }

    #[Tag('ExamPilot')]
    #[Route(path: '/api/pilot/exams/{exam}/reassign', name: 'reassign_failed_exam', methods: ['POST'])]
    #[ParamConverter('exam', class: Exam::class)]
    #[IsGranted('ROLE_USER')]
    public function reassignExam(Exam $exam, ExamServiceInterface $examService): JsonResponse
    {
        try {
            $examService->reassignFailedExam($exam->id());
        } catch (InvalidArgumentException $exception) {
            throw new BadRequestHttpException($exception->getMessage(), $exception);
        }

        return new JsonResponse();
    }

    /** @OA\RequestBody(@OA\JsonContent(ref=@NA\Model(type=PilotExamRequestDto::class))) */
    #[Tag('ExamPilot')]
    #[Route(path: '/api/pilot/exams/request', name: 'request_exam', methods: ['POST'])]
    #[IsGranted('ROLE_USER')]
    public function requestExam(Security $security, Request $request, ExamServiceInterface $examService): JsonResponse
    {
        $request = PilotExamRequestDto::fromRequest($request);

        try {
            $exam = $examService->request(new PilotExamRequest($security->getUser()->getId(), $request->examType, $request->typeRatingId, $request->minimumId));
        } catch (InvalidArgumentException $exception) {
            throw new BadRequestHttpException($exception->getMessage(), $exception);
        }

        return new JsonResponse(DetailedExamDto::fromEntity($exam));
    }
}
