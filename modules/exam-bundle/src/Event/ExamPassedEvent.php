<?php

declare(strict_types=1);

namespace App\Modules\ExamBundle\Event;

final class ExamPassedEvent
{
    public function __construct(
        private readonly int $examId,
        private readonly int $userId,
        private readonly bool $isSuccess,
        private readonly bool $isEntrance
    )
    {
    }

    public function examId(): int
    {
        return $this->examId;
    }

    public function userId(): int
    {
        return $this->userId;
    }

    public function isSuccess(): bool
    {
        return $this->isSuccess;
    }

    public function isEntrance(): bool
    {
        return $this->isEntrance;
    }
}
