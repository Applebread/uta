<?php

namespace App\Modules\ExamBundle\Event;

final class InstructorAssignedToExamEvent
{
    public function __construct(private readonly int $examId, private readonly int $instructorId)
    {
    }

    public function getInstructorId(): int
    {
        return $this->instructorId;
    }

    public function getExamId(): int
    {
        return $this->examId;
    }
}
