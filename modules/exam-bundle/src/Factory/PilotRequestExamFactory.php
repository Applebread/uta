<?php

namespace App\Modules\ExamBundle\Factory;

use App\Modules\AircraftBundle\Repository\AircraftTypeRepository;
use App\Modules\ExamBundle\Entity\Exam;
use App\Modules\ExamBundle\Entity\MinimumExam;
use App\Modules\ExamBundle\Entity\TypeRatingExam;
use App\Modules\ExamBundle\Interfaces\Entity\ExamInterface;
use App\Modules\ExamBundle\Interfaces\Generator\QuestionListGeneratorInterface;
use App\Modules\PilotBundle\Repository\MinimumRepository;
use App\Modules\PilotBundle\Repository\PilotRepository;
use App\Modules\PilotBundle\Repository\TypeRatingRepository;
use DateTimeImmutable;
use Doctrine\Common\Collections\ArrayCollection;
use InvalidArgumentException;

final class PilotRequestExamFactory
{
    public function __construct(
        private readonly PilotRepository $pilotRepository,
        private readonly AircraftTypeRepository $aircraftTypeRepository,
        private readonly MinimumRepository $minimumRepository,
        private readonly QuestionListGeneratorInterface $questionListGenerator,
        private readonly TypeRatingRepository $typeRatingRepository,
    )
    {
    }

    public function createExamRequestedByPilot(int $pilotId, string $examType, ?int $typeRatingId = null, ?int $minimumId = null): Exam
    {
        $now = new DateTimeImmutable();

        if (!$pilot = $this->pilotRepository->getById($pilotId)) {
            throw new InvalidArgumentException("Unknown pilot with id '$pilotId'");
        }

        if ($examType === ExamInterface::EXAM_TYPE_TYPE_RATING) {
            if (!$typeRatingId || !($typeRating = $this->typeRatingRepository->find($typeRatingId))) {
                throw new InvalidArgumentException("Unknown aircraft type: '$typeRatingId'");
            }
        }

        if ($examType === ExamInterface::EXAM_TYPE_MINIMUM) {
            if (!$minimumId || !($minimum = $this->minimumRepository->find($minimumId))) {
                throw new InvalidArgumentException("Unknown minimum id: '$minimumId'");
            }
        }

        switch ($examType) {
            case ExamInterface::EXAM_TYPE_TYPE_RATING:
                return new TypeRatingExam($pilot, $now, 'Type rating for '. $typeRating->getName(), $typeRating, new ArrayCollection(iterator_to_array($this->questionListGenerator->generateQuestions($examType))));
            case ExamInterface::EXAM_TYPE_MINIMUM:
                return new MinimumExam($pilot, $minimum, $now, 'Exam to decrease meteo-minumum to ' . $minimum->getValue(), new ArrayCollection(iterator_to_array($this->questionListGenerator->generateQuestions($examType))));
            case ExamInterface::EXAM_TYPE_TRAINING:
            case ExamInterface::EXAM_TYPE_COMMON:
                throw new InvalidArgumentException("Unable to create $examType exam by user request. It should be created automatically by schedule.");
            default:
                throw new InvalidArgumentException("Unknown exam type: '$examType'");
        }
    }
}
