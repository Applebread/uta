<?php

declare(strict_types=1);

namespace App\Modules\ExamBundle\Fixtures;

use App\Modules\ExamBundle\Entity\QuestionBank\BankQuestion;
use App\Modules\ExamBundle\Entity\QuestionBank\BankQuestionOption;
use App\Modules\ExamBundle\Interfaces\Repository\BankQuestionRepositoryInterface;
use App\Modules\PilotBundle\Entity\TypeRating;
use App\Modules\PilotBundle\Repository\TypeRatingRepository;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ObjectManager;

class BankQuestionsFixture extends Fixture
{
    private const FIXTURE_DATA = [
        [
            'question' => 'In the event of an electrical failure when flying an aircraft using an Electronic Flight Display, the primary pitch instrument is the',
            'question_type' => BankQuestion::TYPE_COMMON,
            'options' => [
                [
                    'text' => 'Standby altimeter',
                    'isCorrect' => true
                ],
                [
                    'text' => 'Standby airspeed indicator',
                    'isCorrect' => false
                ],
                [
                    'text' => 'Standby attitude indicator',
                    'isCorrect' => false
                ]
            ]
        ],
        [
            'question' => 'What is the recommended climb procedure when a nonradar departure control instructs a pilot to climb to the assigned altitude?',
            'question_type' => BankQuestion::TYPE_COMMON,
            'options' => [
                [
                    'text' => 'Maintain an optimum climb on the centerline of the airway without intermediate level-offs until 1,000 feet below assigned altitude, then 500 to 1500 feet per minute.',
                    'isCorrect' => true
                ],
                [
                    'text' => 'Maintain a continuous optimum climb until reaching assigned altitude and report passing each 1,000 foot level.',
                    'isCorrect' => false
                ],
                [
                    'text' => 'Climb at a maximum angle of climb to within 1,000 feet of the assigned altitude, then 500 feet per minute the last 1,000 feet.',
                    'isCorrect' => false
                ]
            ]
        ],
        [
            'question' => 'When is radar service terminated during a visual approach?',
            'question_type' => BankQuestion::TYPE_COMMON,
            'options' => [
                [
                    'text' => 'Automatically when ATC instructs the pilot to contact the tower.',
                    'isCorrect' => true
                ],
                [
                    'text' => 'Immediately upon acceptance of the approach by the pilot.',
                    'isCorrect' => false
                ],
                [
                    'text' => 'When ATC advises "Radar Services Terminated, Resume Own Navigation."',
                    'isCorrect' => false
                ]
            ]
        ],
        [
            'question' => 'Under which condition does ATC issue a STAR?',
            'question_type' => BankQuestion::TYPE_COMMON,
            'options' => [
                [
                    'text' => 'When ATC deems it appropriate, unless the pilot requests "No STAR."',
                    'isCorrect' => true
                ],
                [
                    'text' => 'Only if the pilot requests a STAR in the "Remarks" section of the flight plan.',
                    'isCorrect' => false
                ],
                [
                    'text' => 'To all pilots wherever STARs are available.',
                    'isCorrect' => false
                ]
            ]
        ],
        [
            'question' => 'An instrument rated pilot, who has not logged any instrument time in 1 year or more, cannot serve as pilot in command under IFR, unless the pilot:',
            'question_type' => BankQuestion::TYPE_COMMON,
            'options' => [
                [
                    'text' => 'completes the required 6 hours and six approaches, followed by an instrument proficiency check given by an FAA-designated examiner.',
                    'isCorrect' => false
                ],
                [
                    'text' => 'passes an instrument proficiency check in the category of aircraft involved, followed by 6 hours and six instrument approaches, 3 of those hours in the category of aircraft involved.',
                    'isCorrect' => true
                ],
                [
                    'text' => 'passes an instrument proficiency check in the category of aircraft involved, given by an approved FAA examiner, instrument instructor, or FAA inspector.',
                    'isCorrect' => false
                ]
            ]
        ],
        [
            'question' => 'When performance is limited, such as with an inoperative engine, to prevent receiving:',
            'question_type' => BankQuestion::TYPE_TYPE_RATING,
            'options' => [
                [
                    'text' => 'Select ALT OFF.',
                    'isCorrect' => false
                ],
                [
                    'text' => 'Select TCAS OFF.',
                    'isCorrect' => false
                ],
                [
                    'text' => 'Select TA/RA.',
                    'isCorrect' => false
                ],
                [
                    'text' => 'Select TA.',
                    'isCorrect' => true
                ]
            ],
            'aircraftType' => 'B738'
        ],
        [
            'question' => 'When are the GPWS windshear warnings available?',
            'question_type' => BankQuestion::TYPE_TYPE_RATING,
            'options' => [
                [
                    'text' => 'Below 1500 feet AGL.',
                    'isCorrect' => true
                ],
                [
                    'text' => 'Below 2500 feet AGL.',
                    'isCorrect' => false
                ],
                [
                    'text' => 'Below 3000 feet AGL.',
                    'isCorrect' => false
                ],
                [
                    'text' => 'Below 5000 feet AGL.',
                    'isCorrect' => false
                ]
            ],
            'aircraftType' => 'B738'
        ],
        [
            'question' => 'If all zone controls and primary pack controls fail, the standby pack controls command the packs to produce air temperatures which will satisfy the average temperature demand of the two cabin zones. The trim air modulating valves will:',
            'question_type' => BankQuestion::TYPE_TYPE_RATING,
            'options' => [
                [
                    'text' => 'Open.',
                    'isCorrect' => false
                ],
                [
                    'text' => 'Close.',
                    'isCorrect' => true
                ]
            ],
            'aircraftType' => 'B738'
        ],
        [
            'question' => 'From where does the Cabin Auto Controller get its inputs.',
            'question_type' => BankQuestion::TYPE_TYPE_RATING,
            'options' => [
                [
                    'text' => 'From the static ports',
                    'isCorrect' => false
                ],
                [
                    'text' => 'From the Captain’s CDU',
                    'isCorrect' => false
                ],
                [
                    'text' => 'From the ADIRU’s',
                    'isCorrect' => true
                ],
                [
                    'text' => 'From the EFIS control panel.',
                    'isCorrect' => false
                ],
            ],
            'aircraftType' => 'B738'
        ]
    ];

    public function __construct(
        private readonly BankQuestionRepositoryInterface $questionRepository,
        private readonly TypeRatingRepository $typeRatingRepository,
        private readonly EntityManagerInterface $entityManager
    )
    {
    }

    public function load(ObjectManager $manager)
    {
        foreach (self::FIXTURE_DATA as $question) {
            $questionOptions = array_map(static function (array $option): BankQuestionOption {
                return new BankQuestionOption($option['text'], $option['isCorrect'] ?? null);
            }, $question['options']);

            $typeRating = null;

            if (!empty($question['aircraftType'])) {
                $ratingIdRes = $this->entityManager->getConnection()->fetchFirstColumn(
                    "SELECT type_rating_id from type_rating tr 
                           LEFT JOIN aircraft_type act ON tr.id = act.type_rating_id 
                           WHERE act.icao = '{$question['aircraftType']}'"
                );

                if (!empty($ratingIdRes[0])) {
                    $typeRating = $this->typeRatingRepository->find($ratingIdRes[0]);
                }
            }

            $question = new BankQuestion(
                $question['question_type'],
                $question['question'],
                $questionOptions,
                $typeRating
            );

            $this->questionRepository->save($question);
        }
    }
}
