<?php

declare(strict_types=1);

namespace App\Modules\FlightTrackerBundle\Dto;

use DateTimeImmutable;

final class FlightStep
{
    private function __construct(
        public readonly int               $flightId,
        public readonly string            $flightNumber,
        public readonly string            $login,
        public readonly string            $lat,
        public readonly string            $lon,
        public readonly int               $tas,
        public readonly int               $bank,
        public readonly int               $pitch,
        public readonly bool              $parkingBrake,
        public readonly int               $flapsPosition,
        public readonly DateTimeImmutable $created,
    )
    {
    }

    public static function fromMessageString(string $message): self
    {
        $message = json_decode($message, true);

        return self::fromMessageArray($message);
    }

    public static function fromMessageArray(int $flightId, string $flightNumber, array $message): self
    {
        return new self(
            $flightId,
            $flightNumber,
            $message['pid'],
            $message['lat'],
            $message['lon'],
            $message['tas'],
            $message['bank'],
            $message['pitch'],
            $message['parkingBrake'],
            $message['flapsPosition'],
            new DateTimeImmutable('now')
        );
    }
}
