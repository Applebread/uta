<?php

declare(strict_types=1);

namespace App\Modules\FlightTrackerBundle\Exception;

use InvalidArgumentException;

class InvalidFlightStatusException extends InvalidArgumentException
{
    public static function fromFlightIdAndStatus(int $flightId, string $status): self
    {
        return new self("impossible to write track for flight $flightId has wrong status '$status'");
    }
}
