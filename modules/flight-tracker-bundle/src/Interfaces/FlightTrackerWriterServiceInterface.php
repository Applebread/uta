<?php

namespace App\Modules\FlightTrackerBundle\Interfaces;

use App\Modules\FlightTrackerBundle\Dto\FlightStepsVector;
use App\Modules\PilotBundle\Entity\Pilot;
use App\Modules\TimetableBundle\Entity\Flight;

interface FlightTrackerWriterServiceInterface
{
    public function writeTrack(Pilot $pilot, Flight $flight, FlightStepsVector $flightStepsVector): void;
}
