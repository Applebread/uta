<?php

declare(strict_types=1);

namespace App\Modules\AiracUploaderBundle\MessageHandler;

use App\Modules\AiracUploaderBundle\Interfaces\AiracServiceInterface;
use App\Modules\AiracUploaderBundle\Message\AiracUploadRunMessage;
use Doctrine\ORM\EntityManagerInterface;

class AiracUploadRunMessageHandler
{
    public function __construct(private AiracServiceInterface $airacService, private EntityManagerInterface $em)
    {
    }

    public function __invoke(AiracUploadRunMessage $message): void
    {
        $this->em->getConnection()->connect();

        $this->airacService->processAirac($message->getAiracId());
    }
}
