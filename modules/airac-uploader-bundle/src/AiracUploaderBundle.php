<?php

namespace App\Modules\AiracUploaderBundle;

use Exception;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class AiracUploaderBundle extends Bundle
{
    public function getContainerExtension()
    {
        return null;
    }

    /**
     * @throws Exception
     */
    public function build(ContainerBuilder $container)
    {
        if ($container->isCompiled()) {
            @trigger_error('Container already compiled. Can not set parameters', E_USER_WARNING);

            return;
        }

        $container->setParameter('airac_uploader_bundle_path', dirname(__DIR__));

        $loader = new YamlFileLoader($container, new FileLocator(__DIR__.'/Resources/config'));
        $loader->load('parameters.yaml');
        $loader->load('services.yaml');
        $loader->load('twig.yaml');
        $loader->load('messenger.yaml');

        parent::build($container);
    }
}
