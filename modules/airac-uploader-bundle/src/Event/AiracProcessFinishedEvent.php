<?php

declare(strict_types=1);

namespace App\Modules\AiracUploaderBundle\Event;

use App\Modules\AiracUploaderBundle\Value\AiracAirportCollection;
use App\Modules\AiracUploaderBundle\Value\AiracWaypointCollection;
use Symfony\Contracts\EventDispatcher\Event;

class AiracProcessFinishedEvent extends Event
{
    public const NAME = 'airac_upload_process.finished';

    public function __construct(
        private AiracAirportCollection $airportCollection,
        private AiracWaypointCollection $waypointCollection
    )
    {
    }

    public function airportCollection(): AiracAirportCollection
    {
        return $this->{__FUNCTION__};
    }

    public function waypointCollection(): AiracWaypointCollection
    {
        return $this->{__FUNCTION__};
    }
}
