<?php

namespace App\Modules\AiracUploaderBundle\Controller;

use App\Modules\AiracUploaderBundle\Controller\Dto\AiracFileStatusDto;
use App\Modules\AiracUploaderBundle\Exception\ProcessorNotFoundException;
use App\Modules\AiracUploaderBundle\Exception\UnableToUnpackException;
use App\Modules\AiracUploaderBundle\Interfaces\AiracServiceInterface;
use OpenApi\Attributes\Tag;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AiracController extends AbstractController
{
    public function __construct(private AiracServiceInterface $airacService)
    {
    }

    #[Tag('Navigation')]
    #[Route(path: 'api/admin/navigation/upload-airac', name: 'upload_new_airac', methods: ['POST'])]
    #[IsGranted('ROLE_ADMIN')]
    public function uploadArchive(Request $request): Response
    {
        try {
            $airacId = $this->airacService->uploadNewAiracZip($request->files->get('airac_file'));
        } catch (ProcessorNotFoundException | UnableToUnpackException $exception) {
            return $this->json(['error' => $exception->getMessage()], Response::HTTP_BAD_REQUEST);
        }

        return $this->json(['started' => true, 'id' => $airacId]);
    }

    #[Tag('Navigation')]
    #[Route(path: 'api/admin/navigation/airac/progress/{airacId}', name: 'check_airac_progress', methods: ['GET'])]
    #[IsGranted('ROLE_ADMIN')]
    public function checkUploadProgress(int $airacId): JsonResponse
    {
        $airac = $this->airacService->getAirac($airacId);

        return $this->json($airac ? AiracFileStatusDto::fromEntity($airac) : null);
    }

    #[Tag('Navigation')]
    #[Route(path: 'api/admin/navigation/airac/current', name: 'current_airac', methods: ['GET'])]
    #[IsGranted('ROLE_ADMIN')]
    public function currentUploadedAirac(): JsonResponse
    {
        $airac = $this->airacService->getLatestFileStatus();

        return $this->json($airac ? AiracFileStatusDto::fromEntity($airac) : null);
    }
}
