<?php

namespace App\Modules\AiracUploaderBundle\Service\Processor;

use App\Modules\AiracUploaderBundle\Entity\Airac;
use App\Modules\AiracUploaderBundle\Event\AiracProcessFinishedEvent;
use App\Modules\AiracUploaderBundle\Interfaces\AiracProcessorInterface;
use App\Modules\AiracUploaderBundle\Value\AiracAirport;
use App\Modules\AiracUploaderBundle\Value\AiracAirportCollection;
use App\Modules\AiracUploaderBundle\Value\AiracWaypoint;
use App\Modules\AiracUploaderBundle\Value\AiracWaypointCollection;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

final class KLN90BNativeProcessor implements AiracProcessorInterface
{
    public function __construct(private EventDispatcherInterface $eventDispatcher)
    {
    }

    private const NAME = 'EADT KLN 90B';

    public function process(string $filesPath): void
    {
        $airports = $this->readAirports($filesPath);

        $this->eventDispatcher->dispatch(
            new AiracProcessFinishedEvent(
                new AiracAirportCollection(...$airports),
                new AiracWaypointCollection()),
            AiracProcessFinishedEvent::NAME
        );
    }

    public function supported(string $filesPath): bool
    {
        $cycleData = $this->getCycleData($filesPath);

        return !empty($cycleData['name']) && $cycleData['name'] === self::NAME;
    }

    public function cycle(string $filesPath): string
    {
        $cycleData = $this->getCycleData($filesPath);

        return $cycleData['cycle'];
    }

    private function getCycleData(string $filesPath): array
    {
        $cycleJson = file_get_contents($filesPath . DIRECTORY_SEPARATOR . 'cycle.json');

        return json_decode($cycleJson, true);
    }

    public function readAirports(string $filesPath): array
    {
        $airports = [];

        $airportsCsvStream = fopen($filesPath . DIRECTORY_SEPARATOR . 'airports.txt', 'r');

        while ($row = fgetcsv($airportsCsvStream, null, '|')) {
            if (empty($row[0]) || $row['0'] !== 'A') {
                continue;
            }
            $airports[] = new AiracAirport(
                $row[1],
                $row[2],
                $row[5],
                (int)$row[3],
                (int)$row[4]
            );
        }

        fclose($airportsCsvStream);

        return $airports;
    }

    public function readWaypoints(string $filesPath): array
    {
        $waypoints = [];

        $waypointsCsvStream = fopen($filesPath . DIRECTORY_SEPARATOR . 'waypoints.txt', 'r');

        while ($row = fgetcsv($waypointsCsvStream, null, '|')) {
            $waypoints[] = new AiracWaypoint(
                $row[0],
                (int) $row[1],
                (int) $row[2],
                $row[3]
            );
        }

        fclose($waypointsCsvStream);

        return $waypoints;
    }
}
