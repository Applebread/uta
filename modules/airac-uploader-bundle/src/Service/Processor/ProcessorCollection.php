<?php

namespace App\Modules\AiracUploaderBundle\Service\Processor;

use App\Modules\AiracUploaderBundle\Interfaces\AiracProcessorInterface;
use Generator;
use Iterator;

final class ProcessorCollection implements Iterator
{
    private array $processors;

    public function __construct(iterable ...$processors)
    {
        $this->processors = iterator_to_array($processors[0]->getIterator());
    }

    public function getIterator(): Generator
    {
        foreach ($this->processors as $uploader) {
            yield $uploader;
        }
    }

    public function current(): AiracProcessorInterface
    {
        return current($this->processors);
    }

    public function next(): void
    {
        next($this->processors);
    }

    public function key(): mixed
    {
        return key($this->processors);
    }

    public function valid(): bool
    {
        return false !== current($this->processors);
    }

    public function rewind(): void
    {
        reset($this->processors);
    }
}
