<?php

namespace App\Modules\AiracUploaderBundle\Service;

use App\Modules\AiracUploaderBundle\Entity\Airac;
use App\Modules\AiracUploaderBundle\Exception\ProcessorNotFoundException;
use App\Modules\AiracUploaderBundle\Exception\UnableToUnpackException;
use App\Modules\AiracUploaderBundle\Interfaces\AiracServiceInterface;
use App\Modules\AiracUploaderBundle\Message\AiracUploadRunMessage;
use App\Modules\AiracUploaderBundle\Repository\AiracRepository;
use App\Modules\AiracUploaderBundle\Service\Processor\ProcessorCollection;
use App\Modules\AiracUploaderBundle\Value\AiracFile;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Messenger\MessageBusInterface;
use ValueError;
use ZipArchive;

class AiracService implements AiracServiceInterface
{
    private string $extractedPath;

    public function __construct(
        private AiracRepository     $airacRepository,
        private ProcessorCollection $processorCollection,
        private string              $airacFilePath,
        private MessageBusInterface $messageBus
    )
    {
        $this->extractedPath = $this->airacFilePath . DIRECTORY_SEPARATOR . 'extracted';
    }

    public function currentUploadedVersion(): ?string
    {
        $latestUploadedRecord = $this->airacRepository->findOneBy([], ['id' => 'DESC']);

        return $latestUploadedRecord ? $latestUploadedRecord->getCycle() : null;
    }

    public function uploadNewAiracZip(UploadedFile $file): int
    {
        $this->unpackFile($file);
        $airac = $this->airacRepository->createAiracRecord($file);
        $airac->refreshUpdated();
        $airac->setStatus(Airac::STATUS_STARTING);
        $this->airacRepository->save($airac);

        $this->messageBus->dispatch(new AiracUploadRunMessage($airac->getId()));

        return $airac->getId();
    }

    private function clearFolder(string $folder): void
    {
        if (is_dir($folder)) {
            $objects = scandir($folder);
            foreach ($objects as $object) {
                if ($object != "." && $object != "..") {
                    if (is_dir($folder . DIRECTORY_SEPARATOR . $object) && !is_link($folder . "/" . $object))
                        $this->clearFolder($folder . DIRECTORY_SEPARATOR . $object);
                    else
                        unlink($folder . DIRECTORY_SEPARATOR . $object);
                }
            }
            rmdir($folder);
        }
    }

    private function unpackFile(UploadedFile $file): void
    {
        $this->clearFolder($this->airacFilePath.DIRECTORY_SEPARATOR.'extracted');

        $zip = new ZipArchive();
        move_uploaded_file($file->getPathname(), $this->airacFilePath.DIRECTORY_SEPARATOR.$file->getClientOriginalName());

        if (!$zip->open($this->airacFilePath . DIRECTORY_SEPARATOR . $file->getClientOriginalName())) {
            UnableToUnpackException::fromFileName($file->getClientOriginalName());
        }

        try {
            $zip->extractTo($this->extractedPath);
        } catch (ValueError $exception) {
            UnableToUnpackException::fromFileName($file->getClientOriginalName());
        }
    }

    public function processAirac(int $airacId): void
    {
        $airac = $this->airacRepository->find($airacId);
        $airac->setStatus(Airac::STATUS_PROGRESS);
        $airac->refreshUpdated();

        foreach ($this->processorCollection as $processor) {
            if (!$processor->supported($this->extractedPath)) {
                continue;
            }
            $airac->setCycle($processor->cycle($this->extractedPath));
            $this->airacRepository->save($airac);

            $processor->process($this->extractedPath);

            $airac->refreshUpdated();
            $airac->setStatus(Airac::STATUS_FINISHED);
            $this->airacRepository->save($airac);

            return;
        }

        $airac->refreshUpdated();
        $airac->setStatus(Airac::STATUS_FAILED);
        $this->airacRepository->save($airac);

        ProcessorNotFoundException::fromFileName($airac->getArchiveName());
    }

    public function getAirac(int $airacId): ?Airac
    {
        return $this->airacRepository->find($airacId);
    }

    public function getLatestFileStatus(): ?Airac
    {
        return $this->airacRepository->findOneBy([], ['updated' => 'DESC']);
    }
}
