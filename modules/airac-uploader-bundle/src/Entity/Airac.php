<?php

declare(strict_types=1);

namespace App\Modules\AiracUploaderBundle\Entity;

use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;
use App\Modules\AiracUploaderBundle\Repository\AiracRepository;
use RuntimeException;
use Symfony\Component\HttpFoundation\File\UploadedFile;

#[ORM\Entity(repositoryClass: AiracRepository::class)]
class Airac
{
    const STATUS_STARTING = 'starting';

    const STATUS_PROGRESS = 'progress';

    const STATUS_FINISHED = 'finished';

    const STATUS_FAILED = 'failed';

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private int $id;

    #[ORM\Column(type: 'string', nullable: true)]
    private ?string $cycle = null;

    private ?UploadedFile $archive;

    #[ORM\Column(type: 'string')]
    private string $archiveName;

    #[ORM\Column(type: 'datetime_immutable')]
    private DateTimeImmutable $updated;

    #[ORM\Column(type: 'string', nullable: true)]
    private string $status;

    public function getId(): int
    {
        return $this->id;
    }

    public function upload(string $pathToFolder): void
    {
        if (null === $this->getArchive()) {
            return;
        }

        $this->getArchive()->move(
            $pathToFolder,
            $this->getArchive()->getClientOriginalName()
        );

        $this->archiveName = $this->getArchive()->getClientOriginalName();

        $this->setArchive(null);
        $this->refreshUpdated();
    }

    public function refreshUpdated(): void
    {
        $this->setUpdated(new DateTimeImmutable());
    }


    public function getArchive(): ?UploadedFile
    {
        return $this->archive;
    }

    public function setArchive(?UploadedFile $archive): void
    {
        $this->archive = $archive;
    }

    public function getUpdated(): DateTimeImmutable
    {
        return $this->updated;
    }

    public function setUpdated(DateTimeImmutable $updated): void
    {
        $this->updated = $updated;
    }

    public function getArchiveName(): string
    {
        return $this->archiveName;
    }

    public function setArchiveName(string $archiveName): void
    {
        $this->archiveName = $archiveName;
    }

    public function setCycle(?string $cycle): void
    {
        $this->cycle = $cycle;
    }

    public function getCycle(): ?string
    {
        return $this->cycle;
    }

    public function getStatus(): string
    {
        return $this->status;
    }

    public function setStatus(string $status): void
    {
        if (!in_array($status, [self::STATUS_STARTING, self::STATUS_FAILED, self::STATUS_FINISHED, self::STATUS_PROGRESS])) {
            throw new RuntimeException('Unknown Airac Status');
        }

        $this->status = $status;
    }
}
