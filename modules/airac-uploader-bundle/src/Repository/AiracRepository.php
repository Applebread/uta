<?php

namespace App\Modules\AiracUploaderBundle\Repository;

use App\Modules\AiracUploaderBundle\Entity\Airac;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class AiracRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Airac::class);
    }

    public final function createAiracRecord(UploadedFile $file): Airac
    {
        $airac = new Airac();

        $airac->setArchive($file);
        $airac->setArchiveName($file->getClientOriginalName());

        return $airac;
    }

    public function save(Airac $airac): void
    {
        $this->_em->persist($airac);
        $this->_em->flush();
    }
}
