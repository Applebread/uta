<?php

declare(strict_types=1);

namespace App\Modules\AiracUploaderBundle\Value;

use Generator;
use IteratorAggregate;

final class AiracWaypointCollection implements IteratorAggregate
{
    private array $waypoints;

    public function __construct(AiracWaypoint ...$waypoints)
    {
        $this->waypoints = $waypoints;
    }

    public function getIterator(): Generator
    {
        foreach ($this->waypoints as $airport) {
            yield $airport;
        }
    }
}
