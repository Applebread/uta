<?php

declare(strict_types=1);

namespace App\Modules\AiracUploaderBundle\Value;

use Generator;
use IteratorAggregate;

final class AiracAirportCollection implements IteratorAggregate
{
    private array $airports;

    public function __construct(AiracAirport ...$airports)
    {
        $this->airports = $airports;
    }

    public function getIterator(): Generator
    {
        foreach ($this->airports as $airport) {
            yield $airport;
        }
    }
}
