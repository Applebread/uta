<?php

declare(strict_types=1);

namespace App\Modules\AiracUploaderBundle\Value;

class AiracWaypoint
{
    public function __construct(
        private string $name,
        private int    $latitude,
        private int    $longitude,
        private string $fir,
    )
    {
    }

    public function name(): string
    {
        return $this->{__FUNCTION__};
    }

    public function fir(): int
    {
        return $this->{__FUNCTION__};
    }

    public function latitude(): int
    {
        return $this->{__FUNCTION__};
    }

    public function longitude(): int
    {
        return $this->{__FUNCTION__};
    }
}
