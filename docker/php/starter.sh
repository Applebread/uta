#!/bin/bash
composer install
bin/console cache:clear
bin/console doctrine:migrations:migrate -y


if [ -e .env.local ]
then
    echo ".env.local exist, skipping"
else
    touch .env.local
fi

if [ -e public/uploads ]
then
  echo "public/uploads exist, skipping"
else
  mkdir "public/uploads"
  mkdir "public/uploads/airac"
  mkdir "public/uploads/images"

  echo "Creating permissions..."

  chmod -R 777 public/uploads/*
  chown -R www-data:www-data public/uploads
fi

bin/console assets:install public

bin/console messenger:consume -vvv async > var/log/messenger.log &
echo "Consumer started"

php-fpm
