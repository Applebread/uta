<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20250215112857 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Fixed flight alternate entity relations';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE flight_alternate DROP CONSTRAINT flight_alternate_flights_id_constr');
        $this->addSql('DROP INDEX flight_alternates_flight_id_idx');
        $this->addSql('ALTER TABLE flight_alternate RENAME COLUMN flights_id TO flight_id');
        $this->addSql('ALTER TABLE flight_alternate ADD CONSTRAINT flight_alternate_flights_id_constr FOREIGN KEY (flight_id) REFERENCES flight (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX flight_alternates_flight_id_idx ON flight_alternate (flight_id)');
    }

    public function down(Schema $schema): void
    {
        // no rollback
    }
}
