<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20250125180358 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Added aircraft type average speed';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE aircraft_type ADD average_speed INT NOT NULL DEFAULT 0.0');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE aircraft_type DROP average_speed');
    }
}
