<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20240825091232 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Type-rating to pilot relation';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE TABLE pilot_type_rating (pilot_id INT NOT NULL, type_rating_id INT NOT NULL, PRIMARY KEY(pilot_id, type_rating_id))');
        $this->addSql('CREATE INDEX IDX_309844D6CE55439B ON pilot_type_rating (pilot_id)');
        $this->addSql('CREATE INDEX IDX_309844D6BEF4AED6 ON pilot_type_rating (type_rating_id)');
        $this->addSql('ALTER TABLE pilot_type_rating ADD CONSTRAINT FK_309844D6CE55439B FOREIGN KEY (pilot_id) REFERENCES "user" (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE pilot_type_rating ADD CONSTRAINT FK_309844D6BEF4AED6 FOREIGN KEY (type_rating_id) REFERENCES type_rating (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE "user" DROP type_ratings');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE pilot_type_rating DROP CONSTRAINT FK_309844D6CE55439B');
        $this->addSql('ALTER TABLE pilot_type_rating DROP CONSTRAINT FK_309844D6BEF4AED6');
        $this->addSql('DROP TABLE pilot_type_rating');
        $this->addSql('ALTER TABLE "user" ADD type_ratings TEXT DEFAULT NULL');
        $this->addSql('COMMENT ON COLUMN "user".type_ratings IS \'(DC2Type:simple_array)\'');
    }
}
