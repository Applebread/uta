<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20250215113439 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE flight DROP CONSTRAINT flight_from_constr');
        $this->addSql('ALTER TABLE flight DROP CONSTRAINT flight_to_constr');
        $this->addSql('DROP INDEX flight_to_idx');
        $this->addSql('DROP INDEX flight_from_idx');

        $this->addSql('ALTER TABLE flight ADD from_icao VARCHAR(4) DEFAULT NULL');
        $this->addSql('ALTER TABLE flight ADD to_icao VARCHAR(4) DEFAULT NULL');
        $this->addSql('ALTER TABLE flight DROP "from"');
        $this->addSql('ALTER TABLE flight DROP "to"');

        $this->addSql('ALTER TABLE flight ADD CONSTRAINT flight_from_constr FOREIGN KEY (from_icao) REFERENCES airport (icao) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE flight ADD CONSTRAINT flight_to_constr FOREIGN KEY (to_icao) REFERENCES airport (icao) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX flight_from_idx ON flight (from_icao)');
        $this->addSql('CREATE INDEX flight_to_idx ON flight (to_icao)');

        $this->addSql('ALTER INDEX flight_alternate_icao_idx RENAME TO UNIQ_1326F8737FBA826E');
        $this->addSql('ALTER INDEX flight_alternates_flight_id_idx RENAME TO IDX_1326F87391F478C5');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('CREATE TABLE airports_dictionary (iata VARCHAR(3) NOT NULL, icao VARCHAR(4) NOT NULL)');
        $this->addSql('CREATE INDEX airports_dictionary_iata ON airports_dictionary (iata)');
        $this->addSql('CREATE INDEX airports_dictionary_icao ON airports_dictionary (icao)');
        $this->addSql('ALTER TABLE flight DROP CONSTRAINT FK_C257E60EF986E920');
        $this->addSql('ALTER TABLE flight DROP CONSTRAINT FK_C257E60E6BFE250C');
        $this->addSql('DROP INDEX IDX_C257E60EF986E920');
        $this->addSql('DROP INDEX IDX_C257E60E6BFE250C');
        $this->addSql('ALTER TABLE flight ADD "from" VARCHAR(4) DEFAULT NULL');
        $this->addSql('ALTER TABLE flight ADD "to" VARCHAR(4) DEFAULT NULL');
        $this->addSql('ALTER TABLE flight DROP from_icao');
        $this->addSql('ALTER TABLE flight DROP to_icao');
        $this->addSql('ALTER TABLE flight ADD CONSTRAINT flight_from_constr FOREIGN KEY ("from") REFERENCES airport (icao) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE flight ADD CONSTRAINT flight_to_constr FOREIGN KEY ("to") REFERENCES airport (icao) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX flight_to_idx ON flight ("to")');
        $this->addSql('CREATE INDEX flight_from_idx ON flight ("from")');
        $this->addSql('ALTER TABLE minimum ALTER initial SET DEFAULT false');
        $this->addSql('ALTER TABLE minimum ALTER initial DROP NOT NULL');
        $this->addSql('ALTER TABLE rank ALTER initial SET DEFAULT false');
        $this->addSql('ALTER TABLE rank ALTER initial DROP NOT NULL');
        $this->addSql('ALTER INDEX idx_1326f87391f478c5 RENAME TO flight_alternates_flight_id_idx');
        $this->addSql('ALTER INDEX uniq_1326f8737fba826e RENAME TO flight_alternate_icao_idx');
        $this->addSql('ALTER TABLE aircraft_type ALTER average_speed SET DEFAULT 0.0');
        $this->addSql('ALTER TABLE exam_question_option ALTER answer SET DEFAULT false');
    }
}
