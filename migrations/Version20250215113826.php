<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20250215113826 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('DROP INDEX flight_alternate_icao_idx');
        $this->addSql('CREATE INDEX flight_alternate_icao_idx ON flight_alternate (alternate_icao)');
    }

    public function down(Schema $schema): void
    {
       // no rollback
    }
}
