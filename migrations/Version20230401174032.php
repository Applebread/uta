<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230401174032 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE flight ADD aircraft_type_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE flight ADD CONSTRAINT FK_C257E60EB5DC64D FOREIGN KEY (aircraft_type_id) REFERENCES aircraft_type (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_C257E60EB5DC64D ON flight (aircraft_type_id)');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('COMMENT ON COLUMN "user".birth_date IS NULL');
        $this->addSql('ALTER TABLE flight DROP CONSTRAINT FK_C257E60EB5DC64D');
        $this->addSql('DROP INDEX IDX_C257E60EB5DC64D');
        $this->addSql('ALTER TABLE flight DROP aircraft_type_id');
    }
}
