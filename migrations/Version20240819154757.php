<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240819154757 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Added answer column for exam questions';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE exam_question_option ADD answer BOOLEAN NOT NULL DEFAULT false');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE exam_question_option DROP answer');
    }
}
