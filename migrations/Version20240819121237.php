<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240819121237 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Added description field for exams';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE exam ADD description TEXT NOT NULL');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE exam DROP description');
    }
}
