<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230527182950 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE flight_track_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE flight_track_item_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE trip_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE trip_leg_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE flight_track (id INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE flight_track_item (id INT NOT NULL, track_id INT DEFAULT NULL, latitude NUMERIC(20, 16) DEFAULT NULL, longitude NUMERIC(20, 16) DEFAULT NULL, bank INT NOT NULL, pitch INT NOT NULL, is_landing_lights_on BOOLEAN NOT NULL, parking_brake SMALLINT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_A73CC7A65ED23C43 ON flight_track_item (track_id)');
        $this->addSql('CREATE TABLE trip (id INT NOT NULL, captain_id INT DEFAULT NULL, first_officer_id INT DEFAULT NULL, booking_date_time TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, finished TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, status VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_7656F53B3346729B ON trip (captain_id)');
        $this->addSql('CREATE INDEX IDX_7656F53BF2A9FF54 ON trip (first_officer_id)');
        $this->addSql('COMMENT ON COLUMN trip.booking_date_time IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN trip.finished IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('CREATE TABLE trip_leg (id INT NOT NULL, trip_id INT DEFAULT NULL, track_id INT DEFAULT NULL, from_id INT DEFAULT NULL, to_id INT DEFAULT NULL, status VARCHAR(255) NOT NULL, created TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, departure_date_time TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, arrived TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, selected_alternates TEXT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_F91979F5A5BC2E0E ON trip_leg (trip_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_F91979F55ED23C43 ON trip_leg (track_id)');
        $this->addSql('CREATE INDEX IDX_F91979F578CED90B ON trip_leg (from_id)');
        $this->addSql('CREATE INDEX IDX_F91979F530354A65 ON trip_leg (to_id)');
        $this->addSql('COMMENT ON COLUMN trip_leg.created IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN trip_leg.departure_date_time IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN trip_leg.arrived IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN trip_leg.selected_alternates IS \'(DC2Type:simple_array)\'');
        $this->addSql('ALTER TABLE flight_track_item ADD CONSTRAINT FK_A73CC7A65ED23C43 FOREIGN KEY (track_id) REFERENCES flight_track (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE trip ADD CONSTRAINT FK_7656F53B3346729B FOREIGN KEY (captain_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE trip ADD CONSTRAINT FK_7656F53BF2A9FF54 FOREIGN KEY (first_officer_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE trip_leg ADD CONSTRAINT FK_F91979F5A5BC2E0E FOREIGN KEY (trip_id) REFERENCES trip (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE trip_leg ADD CONSTRAINT FK_F91979F55ED23C43 FOREIGN KEY (track_id) REFERENCES flight_track (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE trip_leg ADD CONSTRAINT FK_F91979F578CED90B FOREIGN KEY (from_id) REFERENCES airport (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE trip_leg ADD CONSTRAINT FK_F91979F530354A65 FOREIGN KEY (to_id) REFERENCES airport (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP SEQUENCE flight_track_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE flight_track_item_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE trip_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE trip_leg_id_seq CASCADE');
        $this->addSql('ALTER TABLE flight_track_item DROP CONSTRAINT FK_A73CC7A65ED23C43');
        $this->addSql('ALTER TABLE trip DROP CONSTRAINT FK_7656F53B3346729B');
        $this->addSql('ALTER TABLE trip DROP CONSTRAINT FK_7656F53BF2A9FF54');
        $this->addSql('ALTER TABLE trip_leg DROP CONSTRAINT FK_F91979F5A5BC2E0E');
        $this->addSql('ALTER TABLE trip_leg DROP CONSTRAINT FK_F91979F55ED23C43');
        $this->addSql('ALTER TABLE trip_leg DROP CONSTRAINT FK_F91979F578CED90B');
        $this->addSql('ALTER TABLE trip_leg DROP CONSTRAINT FK_F91979F530354A65');
        $this->addSql('DROP TABLE flight_track');
        $this->addSql('DROP TABLE flight_track_item');
        $this->addSql('DROP TABLE trip');
        $this->addSql('DROP TABLE trip_leg');
    }
}
