<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20240825110023 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Exam now grant few aircraft types grouped by type rating.';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE bank_question DROP CONSTRAINT fk_87b753c9b5dc64d');
        $this->addSql('DROP INDEX idx_87b753c9b5dc64d');
        $this->addSql('ALTER TABLE bank_question RENAME COLUMN aircraft_type_id TO type_rating_id');
        $this->addSql('ALTER TABLE bank_question ADD CONSTRAINT FK_87B753C9BEF4AED6 FOREIGN KEY (type_rating_id) REFERENCES aircraft_type (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_87B753C9BEF4AED6 ON bank_question (type_rating_id)');
        $this->addSql('ALTER TABLE exam DROP CONSTRAINT fk_38bba6c6b5dc64d');
        $this->addSql('DROP INDEX idx_38bba6c6b5dc64d');
        $this->addSql('ALTER TABLE exam RENAME COLUMN aircraft_type_id TO type_rating_id');
        $this->addSql('ALTER TABLE exam ADD CONSTRAINT FK_38BBA6C6BEF4AED6 FOREIGN KEY (type_rating_id) REFERENCES type_rating (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_38BBA6C6BEF4AED6 ON exam (type_rating_id)');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE exam DROP CONSTRAINT FK_38BBA6C6BEF4AED6');
        $this->addSql('DROP INDEX IDX_38BBA6C6BEF4AED6');
        $this->addSql('ALTER TABLE exam RENAME COLUMN type_rating_id TO aircraft_type_id');
        $this->addSql('ALTER TABLE exam ADD CONSTRAINT fk_38bba6c6b5dc64d FOREIGN KEY (aircraft_type_id) REFERENCES aircraft_type (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX idx_38bba6c6b5dc64d ON exam (aircraft_type_id)');
        $this->addSql('ALTER TABLE exam_question_option ALTER answer SET DEFAULT false');
        $this->addSql('ALTER TABLE bank_question DROP CONSTRAINT FK_87B753C9BEF4AED6');
        $this->addSql('DROP INDEX IDX_87B753C9BEF4AED6');
        $this->addSql('ALTER TABLE bank_question RENAME COLUMN type_rating_id TO aircraft_type_id');
        $this->addSql('ALTER TABLE bank_question ADD CONSTRAINT fk_87b753c9b5dc64d FOREIGN KEY (aircraft_type_id) REFERENCES aircraft_type (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX idx_87b753c9b5dc64d ON bank_question (aircraft_type_id)');
    }
}
