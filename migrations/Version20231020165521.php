<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20231020165521 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Exam bundle entities tree';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE SEQUENCE bank_question_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE bank_question_option_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE exam_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE exam_question_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE exam_question_option_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE bank_question (id INT NOT NULL, aircraft_type_id INT DEFAULT NULL, type VARCHAR(255) NOT NULL, question TEXT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_87B753C9B5DC64D ON bank_question (aircraft_type_id)');
        $this->addSql('CREATE TABLE bank_question_option (id INT NOT NULL, question_id INT DEFAULT NULL, text TEXT NOT NULL, is_correct BOOLEAN NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_A2431DB41E27F6BF ON bank_question_option (question_id)');
        $this->addSql('CREATE TABLE exam (id INT NOT NULL, pilot_id INT DEFAULT NULL, aircraft_type_id INT DEFAULT NULL, date TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, is_passed BOOLEAN NOT NULL, passed_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, result_percent INT DEFAULT NULL, type VARCHAR(255) NOT NULL, status VARCHAR(255) DEFAULT NULL, instructor_name VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_38BBA6C6CE55439B ON exam (pilot_id)');
        $this->addSql('CREATE INDEX IDX_38BBA6C6B5DC64D ON exam (aircraft_type_id)');
        $this->addSql('COMMENT ON COLUMN exam.date IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN exam.passed_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('CREATE TABLE exam_question (id INT NOT NULL, exam_id INT DEFAULT NULL, question TEXT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_F593067D578D5E91 ON exam_question (exam_id)');
        $this->addSql('CREATE TABLE exam_question_option (id INT NOT NULL, question_id INT DEFAULT NULL, description VARCHAR(255) NOT NULL, is_correct BOOLEAN NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_DF4D0E901E27F6BF ON exam_question_option (question_id)');
        $this->addSql('ALTER TABLE bank_question ADD CONSTRAINT FK_87B753C9B5DC64D FOREIGN KEY (aircraft_type_id) REFERENCES aircraft_type (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE bank_question_option ADD CONSTRAINT FK_A2431DB41E27F6BF FOREIGN KEY (question_id) REFERENCES bank_question (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE exam ADD CONSTRAINT FK_38BBA6C6CE55439B FOREIGN KEY (pilot_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE exam ADD CONSTRAINT FK_38BBA6C6B5DC64D FOREIGN KEY (aircraft_type_id) REFERENCES aircraft_type (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE exam_question ADD CONSTRAINT FK_F593067D578D5E91 FOREIGN KEY (exam_id) REFERENCES exam (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE exam_question_option ADD CONSTRAINT FK_DF4D0E901E27F6BF FOREIGN KEY (question_id) REFERENCES exam_question (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP SEQUENCE bank_question_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE bank_question_option_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE exam_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE exam_question_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE exam_question_option_id_seq CASCADE');
        $this->addSql('ALTER TABLE bank_question DROP CONSTRAINT FK_87B753C9B5DC64D');
        $this->addSql('ALTER TABLE bank_question_option DROP CONSTRAINT FK_A2431DB41E27F6BF');
        $this->addSql('ALTER TABLE exam DROP CONSTRAINT FK_38BBA6C6CE55439B');
        $this->addSql('ALTER TABLE exam DROP CONSTRAINT FK_38BBA6C6B5DC64D');
        $this->addSql('ALTER TABLE exam_question DROP CONSTRAINT FK_F593067D578D5E91');
        $this->addSql('ALTER TABLE exam_question_option DROP CONSTRAINT FK_DF4D0E901E27F6BF');
        $this->addSql('DROP TABLE bank_question');
        $this->addSql('DROP TABLE bank_question_option');
        $this->addSql('DROP TABLE exam');
        $this->addSql('DROP TABLE exam_question');
        $this->addSql('DROP TABLE exam_question_option');
    }
}
