<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240819214003 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Add initial flag for minimum and rank';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE minimum ADD initial BOOLEAN DEFAULT false');
        $this->addSql('ALTER TABLE rank ADD initial BOOLEAN DEFAULT false');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE minimum DROP initial');
        $this->addSql('ALTER TABLE rank DROP initial');
    }
}
