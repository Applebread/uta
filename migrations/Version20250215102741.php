<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20250215102741 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Moved airports to using icao code instead of numeric ID';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('DROP SEQUENCE airport_id_seq CASCADE');
        $this->addSql('CREATE SEQUENCE flight_alternate_id_seq INCREMENT BY 1 MINVALUE 1 START 1');

        $this->addSql('ALTER TABLE flight_alternates DROP CONSTRAINT fk_d211c97591f478c5');
        $this->addSql('ALTER TABLE flight_alternates DROP CONSTRAINT fk_d211c975289f53c8');
        $this->addSql('DROP TABLE flight_alternates');
        $this->addSql('ALTER TABLE aircraft DROP CONSTRAINT FK_13D9672964D218E');

        $this->addSql('ALTER TABLE flight DROP CONSTRAINT fk_c257e60e78ced90b');
        $this->addSql('ALTER TABLE flight DROP CONSTRAINT fk_c257e60e30354a65');
        $this->addSql('DROP INDEX idx_c257e60e30354a65');
        $this->addSql('DROP INDEX idx_c257e60e78ced90b');
        $this->addSql('ALTER TABLE flight DROP from_id');
        $this->addSql('ALTER TABLE flight DROP to_id');

        // drop unuseful
        $this->addSql('ALTER TABLE trip_leg DROP CONSTRAINT fk_f91979f530354a65');
        $this->addSql('ALTER TABLE trip_leg DROP CONSTRAINT FK_F91979F578CED90B');
        $this->addSql('DROP INDEX idx_f91979f530354a65');
        $this->addSql('ALTER TABLE "user" DROP CONSTRAINT FK_8D93D64964D218E');
        $this->addSql('ALTER TABLE airport DROP id');

        // change useful
        $this->addSql('ALTER TABLE aircraft ALTER location_id TYPE VARCHAR(4)');

        // add new columns
        $this->addSql('ALTER TABLE flight ADD "from" VARCHAR(4)');
        $this->addSql('ALTER TABLE flight ADD "to" VARCHAR(4)');
        $this->addSql('ALTER TABLE trip_leg ALTER from_id TYPE VARCHAR(4)');
        $this->addSql('ALTER TABLE trip_leg ALTER to_id TYPE VARCHAR(4)');
        $this->addSql('ALTER TABLE "user" ALTER location_id TYPE VARCHAR(4)');
        $this->addSql('ALTER TABLE airport ADD PRIMARY KEY (icao)');

        // constraints
        $this->addSql('ALTER TABLE aircraft ADD CONSTRAINT aircraft_location_constr FOREIGN KEY (location_id) REFERENCES airport (icao) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE flight ADD CONSTRAINT flight_from_constr FOREIGN KEY ("from") REFERENCES airport (icao) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE flight ADD CONSTRAINT flight_to_constr FOREIGN KEY ("to") REFERENCES airport (icao) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE trip_leg ADD CONSTRAINT trip_leg_from_constr FOREIGN KEY (from_id) REFERENCES airport (icao) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE trip_leg ADD CONSTRAINT trip_leg_to_constr FOREIGN KEY (to_id) REFERENCES airport (icao) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE "user" ADD CONSTRAINT user_location_constr FOREIGN KEY (location_id) REFERENCES airport (icao) NOT DEFERRABLE INITIALLY IMMEDIATE');

        // indexes
        $this->addSql('CREATE INDEX flight_from_idx ON flight ("from")');
        $this->addSql('CREATE INDEX flight_to_idx ON flight ("to")');

        // flight alternates many-to-many -> many-to-one with 3 entity
        $this->addSql('CREATE TABLE flight_alternate (id INT NOT NULL, alternate_icao VARCHAR(4) NOT NULL, flights_id INT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX flight_alternate_icao_idx ON flight_alternate (alternate_icao)');
        $this->addSql('CREATE INDEX flight_alternates_flight_id_idx ON flight_alternate (flights_id)');
        $this->addSql('ALTER TABLE flight_alternate ADD CONSTRAINT flight_alternate_alternate_icao_constr FOREIGN KEY (alternate_icao) REFERENCES airport (icao) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE flight_alternate ADD CONSTRAINT flight_alternate_flights_id_constr FOREIGN KEY (flights_id) REFERENCES flight (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP SEQUENCE flight_alternate_id_seq CASCADE');
        $this->addSql('CREATE SEQUENCE airport_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE flight_alternates (flight_id INT NOT NULL, airport_id INT NOT NULL, PRIMARY KEY(flight_id, airport_id))');
        $this->addSql('CREATE INDEX idx_d211c975289f53c8 ON flight_alternates (airport_id)');
        $this->addSql('CREATE INDEX idx_d211c97591f478c5 ON flight_alternates (flight_id)');
        $this->addSql('CREATE TABLE airports_dictionary (iata VARCHAR(3) NOT NULL, icao VARCHAR(4) NOT NULL)');
        $this->addSql('CREATE INDEX airports_dictionary_iata ON airports_dictionary (iata)');
        $this->addSql('CREATE INDEX airports_dictionary_icao ON airports_dictionary (icao)');
        $this->addSql('ALTER TABLE flight_alternates ADD CONSTRAINT fk_d211c97591f478c5 FOREIGN KEY (flight_id) REFERENCES flight (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE flight_alternates ADD CONSTRAINT fk_d211c975289f53c8 FOREIGN KEY (airport_id) REFERENCES airport (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE flight_alternate DROP CONSTRAINT FK_1326F8737FBA826E');
        $this->addSql('ALTER TABLE flight_alternate DROP CONSTRAINT FK_1326F873CEE7F62');
        $this->addSql('DROP TABLE flight_alternate');
        $this->addSql('ALTER TABLE aircraft_type ALTER average_speed SET DEFAULT 0.0');
        $this->addSql('ALTER TABLE rank ALTER initial SET DEFAULT false');
        $this->addSql('ALTER TABLE rank ALTER initial DROP NOT NULL');
        $this->addSql('ALTER TABLE minimum ALTER initial SET DEFAULT false');
        $this->addSql('ALTER TABLE minimum ALTER initial DROP NOT NULL');
        $this->addSql('ALTER TABLE exam_question_option ALTER answer SET DEFAULT false');
        $this->addSql('DROP INDEX airport_pkey');
        $this->addSql('ALTER TABLE airport ADD id INT NOT NULL');
        $this->addSql('ALTER TABLE airport ADD PRIMARY KEY (id)');
        $this->addSql('ALTER TABLE trip_leg DROP CONSTRAINT fk_f91979f578ced90b');
        $this->addSql('ALTER TABLE trip_leg ADD to_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE trip_leg ALTER from_id TYPE INT');
        $this->addSql('ALTER TABLE trip_leg ALTER from_id TYPE INT');
        $this->addSql('ALTER TABLE trip_leg ADD CONSTRAINT fk_f91979f530354a65 FOREIGN KEY (to_id) REFERENCES airport (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE trip_leg ADD CONSTRAINT fk_f91979f578ced90b FOREIGN KEY (from_id) REFERENCES airport (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX idx_f91979f530354a65 ON trip_leg (to_id)');
        $this->addSql('ALTER TABLE aircraft DROP CONSTRAINT fk_13d9672964d218e');
        $this->addSql('ALTER TABLE aircraft ALTER location_id TYPE INT');
        $this->addSql('ALTER TABLE aircraft ALTER location_id TYPE INT');
        $this->addSql('ALTER TABLE aircraft ADD CONSTRAINT fk_13d9672964d218e FOREIGN KEY (location_id) REFERENCES airport (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE "user" DROP CONSTRAINT fk_8d93d64964d218e');
        $this->addSql('ALTER TABLE "user" ALTER location_id TYPE INT');
        $this->addSql('ALTER TABLE "user" ALTER location_id TYPE INT');
        $this->addSql('ALTER TABLE "user" ADD CONSTRAINT fk_8d93d64964d218e FOREIGN KEY (location_id) REFERENCES airport (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE flight DROP CONSTRAINT FK_C257E60EB91AA170');
        $this->addSql('ALTER TABLE flight DROP CONSTRAINT FK_C257E60ED787D2C4');
        $this->addSql('DROP INDEX IDX_C257E60EB91AA170');
        $this->addSql('DROP INDEX IDX_C257E60ED787D2C4');
        $this->addSql('ALTER TABLE flight ADD from_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE flight ADD to_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE flight DROP "from"');
        $this->addSql('ALTER TABLE flight DROP "to"');
        $this->addSql('ALTER TABLE flight ADD CONSTRAINT fk_c257e60e78ced90b FOREIGN KEY (from_id) REFERENCES airport (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE flight ADD CONSTRAINT fk_c257e60e30354a65 FOREIGN KEY (to_id) REFERENCES airport (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX idx_c257e60e30354a65 ON flight (to_id)');
        $this->addSql('CREATE INDEX idx_c257e60e78ced90b ON flight (from_id)');
    }
}
