<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20231029105457 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Exam now related to user instead of pilot.';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE exam DROP CONSTRAINT fk_38bba6c6ce55439b');
        $this->addSql('DROP INDEX idx_38bba6c6ce55439b');
        $this->addSql('ALTER TABLE exam RENAME COLUMN pilot_id TO user_id');
        $this->addSql('ALTER TABLE exam ADD CONSTRAINT FK_38BBA6C6A76ED395 FOREIGN KEY (user_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_38BBA6C6A76ED395 ON exam (user_id)');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE exam DROP CONSTRAINT FK_38BBA6C6A76ED395');
        $this->addSql('DROP INDEX IDX_38BBA6C6A76ED395');
        $this->addSql('ALTER TABLE exam RENAME COLUMN user_id TO pilot_id');
        $this->addSql('ALTER TABLE exam ADD CONSTRAINT fk_38bba6c6ce55439b FOREIGN KEY (pilot_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX idx_38bba6c6ce55439b ON exam (pilot_id)');
    }
}
