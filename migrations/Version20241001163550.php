<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20241001163550 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE TABLE airports_dictionary (iata VARCHAR(3) NOT NULL, icao VARCHAR(4) NOT NULL)');
        $handler = fopen(__DIR__.DIRECTORY_SEPARATOR.'data'.DIRECTORY_SEPARATOR.'airports.csv', 'r');

        $key = 0;
        while ($row = fgetcsv($handler)) {
            $key++;
            if ($key === 1) {
                continue;
            }
            $this->addSql("INSERT INTO airports_dictionary (iata, icao) VALUES ('{$row[0]}', '{$row[1]}')");
        }

        fclose($handler);

        $this->addSql('CREATE INDEX airports_dictionary_icao ON "airports_dictionary" (icao)');
        $this->addSql('CREATE INDEX airports_dictionary_iata ON "airports_dictionary" (iata)');
    }

    public function down(Schema $schema): void
    {
        $this->addSql("DROP TABLE airports_dictionary");
    }
}
