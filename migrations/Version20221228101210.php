<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20221228101210 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Initial migration';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE SEQUENCE airac_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE aircraft_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE aircraft_stats_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE aircraft_type_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE airport_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE flight_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE logbook_item_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE minimum_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE pilot_minimum_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE pilot_rank_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE rank_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE "user_id_seq" INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE airac (id INT NOT NULL, cycle VARCHAR(255) DEFAULT NULL, archive_name VARCHAR(255) NOT NULL, updated TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, status VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('COMMENT ON COLUMN airac.updated IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('CREATE TABLE aircraft (id INT NOT NULL, type_id INT DEFAULT NULL, location_id INT DEFAULT NULL, aircraft_stats_id INT DEFAULT NULL, registration VARCHAR(9) NOT NULL, gate VARCHAR(255) NOT NULL, fob INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_13D9672962A8A7A7 ON aircraft (registration)');
        $this->addSql('CREATE INDEX IDX_13D96729C54C8C93 ON aircraft (type_id)');
        $this->addSql('CREATE INDEX IDX_13D9672964D218E ON aircraft (location_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_13D96729110FDA1B ON aircraft (aircraft_stats_id)');
        $this->addSql('CREATE TABLE aircraft_stats (id INT NOT NULL, last_flight_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, ac_total_fhminutes INT NOT NULL, ac_total_fc INT NOT NULL, latest_fifty_check_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, latest_two_hundred_check_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, latest_thousand_check_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('COMMENT ON COLUMN aircraft_stats.last_flight_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN aircraft_stats.latest_fifty_check_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN aircraft_stats.latest_two_hundred_check_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN aircraft_stats.latest_thousand_check_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('CREATE TABLE aircraft_type (id INT NOT NULL, icao VARCHAR(4) NOT NULL, name VARCHAR(255) NOT NULL, description TEXT NOT NULL, image VARCHAR(255) DEFAULT NULL, heavy BOOLEAN NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE airport (id INT NOT NULL, icao VARCHAR(4) NOT NULL, iata VARCHAR(3) NOT NULL, name VARCHAR(255) NOT NULL, latitude NUMERIC(20, 16) DEFAULT NULL, longitude NUMERIC(20, 16) DEFAULT NULL, elevation INT NOT NULL, city VARCHAR(255) NOT NULL, timezone VARCHAR(255) NOT NULL, source VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE flight (id INT NOT NULL, from_id INT DEFAULT NULL, to_id INT DEFAULT NULL, flight_number VARCHAR(7) NOT NULL, days_of_week TEXT NOT NULL, scheduled_from TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, scheduled_until TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, payload_percent INT NOT NULL, status VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_C257E60E78CED90B ON flight (from_id)');
        $this->addSql('CREATE INDEX IDX_C257E60E30354A65 ON flight (to_id)');
        $this->addSql('COMMENT ON COLUMN flight.days_of_week IS \'(DC2Type:simple_array)\'');
        $this->addSql('COMMENT ON COLUMN flight.scheduled_from IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN flight.scheduled_until IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('CREATE TABLE flight_alternates (flight_id INT NOT NULL, airport_id INT NOT NULL, PRIMARY KEY(flight_id, airport_id))');
        $this->addSql('CREATE INDEX IDX_D211C97591F478C5 ON flight_alternates (flight_id)');
        $this->addSql('CREATE INDEX IDX_D211C975289F53C8 ON flight_alternates (airport_id)');
        $this->addSql('CREATE TABLE logbook_item (id INT NOT NULL, pilot_id INT DEFAULT NULL, created TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, "from" VARCHAR(4) NOT NULL, "to" VARCHAR(4) NOT NULL, flight_time INT NOT NULL, landing_airport VARCHAR(4) NOT NULL, route TEXT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_4615E0A9CE55439B ON logbook_item (pilot_id)');
        $this->addSql('COMMENT ON COLUMN logbook_item.created IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('CREATE TABLE minimum (id INT NOT NULL, value VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE pilot_minimum (id INT NOT NULL, pilot_id INT DEFAULT NULL, minimum_id INT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_DFE28E19CE55439B ON pilot_minimum (pilot_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_DFE28E1939082B31 ON pilot_minimum (minimum_id)');
        $this->addSql('CREATE TABLE pilot_rank (id INT NOT NULL, pilot_id INT DEFAULT NULL, rank_id INT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_9C7473DDCE55439B ON pilot_rank (pilot_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_9C7473DD7616678F ON pilot_rank (rank_id)');
        $this->addSql('CREATE TABLE rank (id INT NOT NULL, name VARCHAR(255) NOT NULL, icon VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE "user" (id INT NOT NULL, rank_id INT DEFAULT NULL, minimum_id INT DEFAULT NULL, login VARCHAR(255) NOT NULL, email VARCHAR(180) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, type VARCHAR(255) NOT NULL, flight_time INT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8D93D6495550C4ED ON "user" (login)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8D93D649E7927C74 ON "user" (email)');
        $this->addSql('CREATE INDEX IDX_8D93D6497616678F ON "user" (rank_id)');
        $this->addSql('CREATE INDEX IDX_8D93D64939082B31 ON "user" (minimum_id)');
        $this->addSql('CREATE TABLE messenger_messages (id BIGSERIAL NOT NULL, body TEXT NOT NULL, headers TEXT NOT NULL, queue_name VARCHAR(190) NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, available_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, delivered_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_75EA56E0FB7336F0 ON messenger_messages (queue_name)');
        $this->addSql('CREATE INDEX IDX_75EA56E0E3BD61CE ON messenger_messages (available_at)');
        $this->addSql('CREATE INDEX IDX_75EA56E016BA31DB ON messenger_messages (delivered_at)');
        $this->addSql('CREATE OR REPLACE FUNCTION notify_messenger_messages() RETURNS TRIGGER AS $$
            BEGIN
                PERFORM pg_notify(\'messenger_messages\', NEW.queue_name::text);
                RETURN NEW;
            END;
        $$ LANGUAGE plpgsql;');
        $this->addSql('DROP TRIGGER IF EXISTS notify_trigger ON messenger_messages;');
        $this->addSql('CREATE TRIGGER notify_trigger AFTER INSERT OR UPDATE ON messenger_messages FOR EACH ROW EXECUTE PROCEDURE notify_messenger_messages();');
        $this->addSql('ALTER TABLE aircraft ADD CONSTRAINT FK_13D96729C54C8C93 FOREIGN KEY (type_id) REFERENCES aircraft_type (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE aircraft ADD CONSTRAINT FK_13D9672964D218E FOREIGN KEY (location_id) REFERENCES airport (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE aircraft ADD CONSTRAINT FK_13D96729110FDA1B FOREIGN KEY (aircraft_stats_id) REFERENCES aircraft_stats (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE flight ADD CONSTRAINT FK_C257E60E78CED90B FOREIGN KEY (from_id) REFERENCES airport (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE flight ADD CONSTRAINT FK_C257E60E30354A65 FOREIGN KEY (to_id) REFERENCES airport (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE flight_alternates ADD CONSTRAINT FK_D211C97591F478C5 FOREIGN KEY (flight_id) REFERENCES flight (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE flight_alternates ADD CONSTRAINT FK_D211C975289F53C8 FOREIGN KEY (airport_id) REFERENCES airport (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE logbook_item ADD CONSTRAINT FK_4615E0A9CE55439B FOREIGN KEY (pilot_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE pilot_minimum ADD CONSTRAINT FK_DFE28E19CE55439B FOREIGN KEY (pilot_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE pilot_minimum ADD CONSTRAINT FK_DFE28E1939082B31 FOREIGN KEY (minimum_id) REFERENCES minimum (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE pilot_rank ADD CONSTRAINT FK_9C7473DDCE55439B FOREIGN KEY (pilot_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE pilot_rank ADD CONSTRAINT FK_9C7473DD7616678F FOREIGN KEY (rank_id) REFERENCES rank (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE "user" ADD CONSTRAINT FK_8D93D6497616678F FOREIGN KEY (rank_id) REFERENCES pilot_rank (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE "user" ADD CONSTRAINT FK_8D93D64939082B31 FOREIGN KEY (minimum_id) REFERENCES pilot_minimum (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP SEQUENCE airac_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE aircraft_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE aircraft_stats_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE aircraft_type_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE airport_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE flight_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE logbook_item_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE minimum_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE pilot_minimum_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE pilot_rank_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE rank_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE "user_id_seq" CASCADE');
        $this->addSql('ALTER TABLE aircraft DROP CONSTRAINT FK_13D96729C54C8C93');
        $this->addSql('ALTER TABLE aircraft DROP CONSTRAINT FK_13D9672964D218E');
        $this->addSql('ALTER TABLE aircraft DROP CONSTRAINT FK_13D96729110FDA1B');
        $this->addSql('ALTER TABLE flight DROP CONSTRAINT FK_C257E60E78CED90B');
        $this->addSql('ALTER TABLE flight DROP CONSTRAINT FK_C257E60E30354A65');
        $this->addSql('ALTER TABLE flight_alternates DROP CONSTRAINT FK_D211C97591F478C5');
        $this->addSql('ALTER TABLE flight_alternates DROP CONSTRAINT FK_D211C975289F53C8');
        $this->addSql('ALTER TABLE logbook_item DROP CONSTRAINT FK_4615E0A9CE55439B');
        $this->addSql('ALTER TABLE pilot_minimum DROP CONSTRAINT FK_DFE28E19CE55439B');
        $this->addSql('ALTER TABLE pilot_minimum DROP CONSTRAINT FK_DFE28E1939082B31');
        $this->addSql('ALTER TABLE pilot_rank DROP CONSTRAINT FK_9C7473DDCE55439B');
        $this->addSql('ALTER TABLE pilot_rank DROP CONSTRAINT FK_9C7473DD7616678F');
        $this->addSql('ALTER TABLE "user" DROP CONSTRAINT FK_8D93D6497616678F');
        $this->addSql('ALTER TABLE "user" DROP CONSTRAINT FK_8D93D64939082B31');
        $this->addSql('DROP TABLE airac');
        $this->addSql('DROP TABLE aircraft');
        $this->addSql('DROP TABLE aircraft_stats');
        $this->addSql('DROP TABLE aircraft_type');
        $this->addSql('DROP TABLE airport');
        $this->addSql('DROP TABLE flight');
        $this->addSql('DROP TABLE flight_alternates');
        $this->addSql('DROP TABLE logbook_item');
        $this->addSql('DROP TABLE minimum');
        $this->addSql('DROP TABLE pilot_minimum');
        $this->addSql('DROP TABLE pilot_rank');
        $this->addSql('DROP TABLE rank');
        $this->addSql('DROP TABLE "user"');
        $this->addSql('DROP TABLE messenger_messages');
    }
}
