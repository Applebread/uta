<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240819172858 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Added is success field for exams';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE exam ADD is_success BOOLEAN DEFAULT null');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE exam DROP is_success');
    }
}
