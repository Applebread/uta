<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240825101628 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE aircraft_type DROP CONSTRAINT fk_586cead0b5dc64d');
        $this->addSql('DROP INDEX idx_586cead0b5dc64d');
        $this->addSql('ALTER TABLE aircraft_type RENAME COLUMN aircraft_type_id TO type_rating_id');
        $this->addSql('ALTER TABLE aircraft_type ADD CONSTRAINT FK_586CEAD0BEF4AED6 FOREIGN KEY (type_rating_id) REFERENCES type_rating (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_586CEAD0BEF4AED6 ON aircraft_type (type_rating_id)');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE aircraft_type DROP CONSTRAINT FK_586CEAD0BEF4AED6');
        $this->addSql('DROP INDEX IDX_586CEAD0BEF4AED6');
        $this->addSql('ALTER TABLE aircraft_type RENAME COLUMN type_rating_id TO aircraft_type_id');
        $this->addSql('ALTER TABLE aircraft_type ADD CONSTRAINT fk_586cead0b5dc64d FOREIGN KEY (aircraft_type_id) REFERENCES aircraft_type (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX idx_586cead0b5dc64d ON aircraft_type (aircraft_type_id)');
    }
}
