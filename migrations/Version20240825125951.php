<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240825125951 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE bank_question DROP CONSTRAINT FK_87B753C9BEF4AED6');
        $this->addSql('ALTER TABLE bank_question ADD CONSTRAINT FK_87B753C9BEF4AED6 FOREIGN KEY (type_rating_id) REFERENCES type_rating (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE bank_question DROP CONSTRAINT fk_87b753c9bef4aed6');
        $this->addSql('ALTER TABLE bank_question ADD CONSTRAINT fk_87b753c9bef4aed6 FOREIGN KEY (type_rating_id) REFERENCES aircraft_type (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }
}
