<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230710160153 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE aircraft ADD trip_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE aircraft ADD CONSTRAINT FK_13D96729A5BC2E0E FOREIGN KEY (trip_id) REFERENCES trip (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_13D96729A5BC2E0E ON aircraft (trip_id)');
        $this->addSql('ALTER TABLE trip DROP CONSTRAINT fk_7656f53bf2a9ff54');
        $this->addSql('DROP INDEX idx_7656f53bf2a9ff54');
        $this->addSql('DROP INDEX idx_7656f53b3346729b');
        $this->addSql('ALTER TABLE trip ADD aircraft_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE trip ADD etd TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL');
        $this->addSql('ALTER TABLE trip RENAME COLUMN first_officer_id TO first_officer');
        $this->addSql('COMMENT ON COLUMN trip.etd IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('ALTER TABLE trip ADD CONSTRAINT FK_7656F53B91FFA326 FOREIGN KEY (first_officer) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE trip ADD CONSTRAINT FK_7656F53B846E2F5C FOREIGN KEY (aircraft_id) REFERENCES aircraft (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_7656F53B3346729B ON trip (captain_id)');
        $this->addSql('CREATE INDEX IDX_7656F53B91FFA326 ON trip (first_officer)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_7656F53B846E2F5C ON trip (aircraft_id)');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE aircraft DROP CONSTRAINT FK_13D96729A5BC2E0E');
        $this->addSql('DROP INDEX UNIQ_13D96729A5BC2E0E');
        $this->addSql('ALTER TABLE aircraft DROP trip_id');
        $this->addSql('ALTER TABLE trip DROP CONSTRAINT FK_7656F53B91FFA326');
        $this->addSql('ALTER TABLE trip DROP CONSTRAINT FK_7656F53B846E2F5C');
        $this->addSql('DROP INDEX UNIQ_7656F53B3346729B');
        $this->addSql('DROP INDEX IDX_7656F53B91FFA326');
        $this->addSql('DROP INDEX UNIQ_7656F53B846E2F5C');
        $this->addSql('ALTER TABLE trip ADD first_officer_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE trip DROP first_officer');
        $this->addSql('ALTER TABLE trip DROP aircraft_id');
        $this->addSql('ALTER TABLE trip DROP etd');
        $this->addSql('ALTER TABLE trip ADD CONSTRAINT fk_7656f53bf2a9ff54 FOREIGN KEY (first_officer_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX idx_7656f53bf2a9ff54 ON trip (first_officer_id)');
        $this->addSql('CREATE INDEX idx_7656f53b3346729b ON trip (captain_id)');
    }
}
