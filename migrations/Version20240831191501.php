<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20240831191501 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE exam ADD instructor_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE exam DROP instructor_name');
        $this->addSql('ALTER TABLE exam ADD CONSTRAINT FK_38BBA6C68C4FC193 FOREIGN KEY (instructor_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_38BBA6C68C4FC193 ON exam (instructor_id)');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE exam DROP CONSTRAINT FK_38BBA6C68C4FC193');
        $this->addSql('DROP INDEX IDX_38BBA6C68C4FC193');
        $this->addSql('ALTER TABLE exam ADD instructor_name VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE exam DROP instructor_id');
    }
}
