<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20240825084316 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Added "type rating" with relations.';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE SEQUENCE type_rating_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE type_rating (id INT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('ALTER TABLE aircraft_type ADD aircraft_type_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE aircraft_type ADD CONSTRAINT FK_586CEAD0B5DC64D FOREIGN KEY (aircraft_type_id) REFERENCES aircraft_type (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_586CEAD0B5DC64D ON aircraft_type (aircraft_type_id)');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('DROP SEQUENCE type_rating_id_seq CASCADE');
        $this->addSql('DROP TABLE type_rating');
        $this->addSql('ALTER TABLE aircraft_type DROP CONSTRAINT FK_586CEAD0B5DC64D');
        $this->addSql('DROP INDEX IDX_586CEAD0B5DC64D');
        $this->addSql('ALTER TABLE aircraft_type DROP aircraft_type_id');
    }
}
